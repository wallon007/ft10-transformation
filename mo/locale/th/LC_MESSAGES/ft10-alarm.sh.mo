��          �      l      �  (   �  '   
  7   2  '   j     �  G   �     �     �                    ,     5     >     G     S     Z  $   z     �     �  p  �  (   !  '   J  �   r  '   &     N  G   a     �     �  $   �  $   �  '        :     S     m     �     �  d   �  s     -   �     �     	                                                                                         
        \t<b>$alarm_set_to_text $hr:$mn</b>      \t<b>$alarm_text $hr:$mn...</b> \n    Are you sure to set the alarm for that time tomorrow? $alarm_set_to_text $hr:$mn
$cancel_text $are_you_sure_text $cancel_alarm_text !bash -c 'cancel_quit_tray $XPID $target'!gtk-cancel $confirm_title Alarm Alarm set to Are you sure??? Cancel Alarm Duration Hour::CB Interval Minutes::CB Repeat To cancel, right-click for menu To cancel, right-click icon for menu infinite seconds Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Thai (https://www.transifex.com/antix-linux-community-contributions/teams/120110/th/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: th
Plural-Forms: nplurals=1; plural=0;
     \t<b>$alarm_set_to_text $hr:$mn</b>      \t<b>$alarm_text $hr:$mn...</b> \n    คุณแน่ใจหรือว่าตั้งนาฬิกาปลุกไว้สำหรับเวลานั้นในวันพรุ่งนี้ $alarm_set_to_text $hr:$mn
$cancel_text $are_you_sure_text $cancel_alarm_text !bash -c 'cancel_quit_tray $XPID $target'!gtk-cancel $confirm_title เตือน ตั้งปลุกเป็น คุณแน่ใจไหม??? ยกเลิกการปลุก ระยะเวลา ชั่วโมง::CB ช่วงเวลา นาที::CB ทำซ้ำ หากต้องการยกเลิก ให้คลิกขวาที่เมนู หากต้องการยกเลิก ให้คลิกขวาที่ไอคอนเมนู ไม่มีที่สิ้นสุด วินาที 