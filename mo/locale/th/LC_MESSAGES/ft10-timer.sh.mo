��          �   %   �      P     Q  "   f     �  1   �     �     �     �  	   �  !        %     =     B  '   H  !   p  #   �  #   �     �     �     �                1     R     g  
   n  p  y  3   �  c     Q   �  �   �  !   �     �  Q   �  4     ?   H  Q   �     �     �  �   �  f   �	  �   �	  ^   }
  -   �
  0   
     ;     N  Z   n  �   �  <   Q     �     �        	                                                                             
                                        Alarm sound filename Ask to begin each set (all timers) Ask to begin each timer Auto close progress bar display when all sets end Configuration Duration Each timer end Finished. Interface to Sysmonitor Indicator Multiple Timer settings Name Never Number of times to run set (all timers) Pop-up message when each set ends Pop-up message when each timer ends Progress Bar update every x seconds Ready to start SUSPEND COMPUTER Seconds Seconds!Minutes Sound alarm when each set ends Sound alarm when each timer ends Timer duration units Timers has ended. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Thai (https://www.transifex.com/antix-linux-community-contributions/teams/120110/th/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: th
Plural-Forms: nplurals=1; plural=0;
 ชื่อไฟล์เสียงปลุก ขอเริ่มแต่ละชุด (ตัวจับเวลาทั้งหมด) ขอให้เริ่มจับเวลาแต่ละครั้ง แถบแสดงความคืบหน้าปิดอัตโนมัติเมื่อสิ้นสุดการตั้งค่าทั้งหมด การกำหนดค่า ระยะเวลา สิ้นสุดตัวจับเวลาแต่ละครั้ง ที่เสร็จเรียบร้อย. อินเทอร์เฟซกับ Sysmonitor Indicator การตั้งค่าตัวจับเวลาหลายตัว ชื่อ ไม่เคย จำนวนครั้งในการทำงานที่ตั้งไว้ (ตัวจับเวลาทั้งหมด) ข้อความป๊อปอัปเมื่อสิ้นสุดแต่ละชุด ข้อความป๊อปอัปเมื่อสิ้นสุดการจับเวลาแต่ละครั้ง แถบความคืบหน้าอัปเดตทุก ๆ x วินาที พร้อมที่จะเริ่ม ระงับคอมพิวเตอร์ วินาที วินาที!นาที เสียงเตือนเมื่อแต่ละชุดสิ้นสุด ส่งเสียงเตือนเมื่อสิ้นสุดการจับเวลาแต่ละครั้ง หน่วยระยะเวลาจับเวลา ตัวจับเวลา จบแล้ว. 