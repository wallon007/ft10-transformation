��          �   %   �      P     Q  "   f     �  1   �     �     �     �  	   �  !        %     =     B  '   H  !   p  #   �  #   �     �     �     �                1     R     g  
   n  v  y     �  ,     )   3  >   ]     �     �      �     �  !   �     �            /   '  '   W  4     '   �     �     �            '     4   ;     p     �     �        	                                                                             
                                        Alarm sound filename Ask to begin each set (all timers) Ask to begin each timer Auto close progress bar display when all sets end Configuration Duration Each timer end Finished. Interface to Sysmonitor Indicator Multiple Timer settings Name Never Number of times to run set (all timers) Pop-up message when each set ends Pop-up message when each timer ends Progress Bar update every x seconds Ready to start SUSPEND COMPUTER Seconds Seconds!Minutes Sound alarm when each set ends Sound alarm when each timer ends Timer duration units Timers has ended. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Indonesian (https://www.transifex.com/antix-linux-community-contributions/teams/120110/id/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: id
Plural-Forms: nplurals=1; plural=0;
 Nama file suara alarm Minta untuk memulai setiap set (semua timer) Minta untuk memulai setiap pengatur waktu Tutup otomatis tampilan bilah kemajuan saat semua set berakhir Konfigurasi Durasi Setiap penghitung waktu berakhir Selesai. Antarmuka ke Indikator Sysmonitor Beberapa pengaturan Timer Nama Tidak pernah Berapa kali untuk menjalankan set (semua timer) Pesan pop-up ketika setiap set berakhir Pesan pop-up ketika setiap penghitung waktu berakhir Pembaruan Bilah Kemajuan setiap x detik Siap untuk mulai Tangguhkan KOMPUTER Detik Detik!Menit Bunyikan alarm saat setiap set berakhir Bunyikan alarm saat setiap penghitung waktu berakhir Satuan durasi pengatur waktu pengatur waktu telah berakhir. 