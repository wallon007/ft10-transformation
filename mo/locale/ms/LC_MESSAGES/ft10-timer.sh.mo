��          �   %   �      P     Q  "   f     �  1   �     �     �     �  	   �  !        %     =     B  '   H  !   p  #   �  #   �     �     �     �                1     R     g  
   n  q  y     �  /     #   4  7   X     �     �     �     �  &   �     �             8     )   K  ,   u  %   �     �     �     �     �  '     *   *     U     h     o        	                                                                             
                                        Alarm sound filename Ask to begin each set (all timers) Ask to begin each timer Auto close progress bar display when all sets end Configuration Duration Each timer end Finished. Interface to Sysmonitor Indicator Multiple Timer settings Name Never Number of times to run set (all timers) Pop-up message when each set ends Pop-up message when each timer ends Progress Bar update every x seconds Ready to start SUSPEND COMPUTER Seconds Seconds!Minutes Sound alarm when each set ends Sound alarm when each timer ends Timer duration units Timers has ended. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Malay (https://www.transifex.com/antix-linux-community-contributions/teams/120110/ms/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: ms
Plural-Forms: nplurals=1; plural=0;
 Nama fail bunyi penggera Minta untuk memulakan setiap set (semua pemasa) Minta untuk memulakan setiap pemasa Auto tutup paparan bar kemajuan apabila semua set tamat Konfigurasi Tempoh Setiap pemasa tamat Selesai. Antara muka kepada Penunjuk Sysmonitor Tetapan Berbilang Pemasa nama tidak pernah Bilangan kali untuk dijalankan ditetapkan (semua pemasa) Mesej pop timbul apabila setiap set tamat Mesej pop timbul apabila setiap pemasa tamat Kemas kini Bar Kemajuan setiap x saat Bersedia untuk bermula GANTUNG KOMPUTER Detik Detik!minit Bunyi penggera apabila setiap set tamat Bunyi penggera apabila setiap pemasa tamat Unit tempoh pemasa Pemasa telah tamat. 