��    (      \  5   �      p     q     }     �     �  
   �     �     �     �     �     �  ,   �     �                      F        f  
   k     v     �     �     �     �     �     �     �     �     �     �     �                    &     ,     3     ;     D  x  P     �     �  
   �     �     �               ;     J     j  |        �          #     0     ?  �   P     )	     2	  '   G	  
   o	  
   z	     �	     �	  5   �	     �	  +   �	     
     5
  %   D
     j
     {
     �
  +   �
  
   �
     �
     �
     �
                             
          	   $                                                     &   (       !          %          "                             #                    '        Accessories All Audio Browser Calculator Clock Control Centre Development E-mail Exit Favorite Apps: (click here to edit the list) File Manager Files Games Graphics Internet Left click- Add,move,remove Toolbar icons; Right click- Manage Toolbar Menu Multimedia Network manager - Connman News Office Pics Recent Search for programs and files Settings Show Desktop Stop App System Task switcher Terminal Text Text Editor Unplug USB drives Video Volume Weather Weather  Web Browser Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Tajik (https://www.transifex.com/antix-linux-community-contributions/teams/120110/tg/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: tg
Plural-Forms: nplurals=2; plural=(n != 1);
 Лавозимот Ҳама Аудио Браузер Ҳисобкунак Соат Маркази назорат Инкишоф Почтаи электронӣ Баромадгоҳ Барномаҳои дӯстдошта: (барои таҳрир кардани рӯйхат ин ҷо клик кунед) Менеҷери файл Файлҳо Бозиҳо Графика интернет Клики чап - Нишонаҳои панели асбобҳоро илова кардан, интиқол додан, нест кардан; Клики рост - Идоракунии панели асбобҳо Меню Мултимедиа Менеҷери шабака - Connman Ахбор Идора Суратҳо Ба наздикӣ Ҷустуҷӯи барномаҳо ва файлҳо Танзимот Мизи кориро нишон диҳед Қатъи барнома Система Ивазкунандаи вазифа Терминал Матн Муҳаррири матн Дискҳои USB-ро ҷудо кунед Видео Ҳаҷм Обу ҳаво Обу ҳаво Веб-браузер 