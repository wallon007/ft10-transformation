��          �   %   �      P     Q  "   f     �  1   �     �     �     �  	   �  !        %     =     B  '   H  !   p  #   �  #   �     �     �     �                1     R     g  
   n  y  y  8   �  �   ,  a   �  �        �     �  ;   �       6   %  2   \  	   �     �  t   �  f   $	  l   �	  �   �	  *   �
  4   �
     �
  %      h   &  n   �  2   �     1  &   J        	                                                                             
                                        Alarm sound filename Ask to begin each set (all timers) Ask to begin each timer Auto close progress bar display when all sets end Configuration Duration Each timer end Finished. Interface to Sysmonitor Indicator Multiple Timer settings Name Never Number of times to run set (all timers) Pop-up message when each set ends Pop-up message when each timer ends Progress Bar update every x seconds Ready to start SUSPEND COMPUTER Seconds Seconds!Minutes Sound alarm when each set ends Sound alarm when each timer ends Timer duration units Timers has ended. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Nepali (https://www.transifex.com/antix-linux-community-contributions/teams/120110/ne/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: ne
Plural-Forms: nplurals=2; plural=(n != 1);
 अलार्म ध्वनि फाइलनाम प्रत्येक सेट सुरु गर्न सोध्नुहोस् (सबै टाइमरहरू) प्रत्येक टाइमर सुरु गर्न सोध्नुहोस् सबै सेट समाप्त हुँदा स्वत: बन्द प्रगति पट्टी प्रदर्शन कन्फिगरेसन अवधि प्रत्येक टाइमर अन्त्य समाप्त भयो। Sysmonitor सूचकमा इन्टरफेस बहु टाइमर सेटिङहरू नाम कहिल्यै सेट चलाउनको लागि समयको संख्या (सबै टाइमरहरू) प्रत्येक सेट समाप्त हुँदा पप-अप सन्देश प्रत्येक टाइमर समाप्त हुँदा पप-अप सन्देश प्रत्येक x सेकेन्डमा प्रगति पट्टी अद्यावधिक गर्नुहोस् सुरु गर्न तयार छ सस्पेन्ड कम्प्युटर सेकेन्ड सेकेन्ड!मिनेट प्रत्येक सेट समाप्त हुँदा अलार्म ध्वनि प्रत्येक टाइमर समाप्त हुँदा अलार्म ध्वनि टाइमर अवधि एकाइहरू टाइमरहरू समाप्त भएको छ। 