��          �      l      �  (   �  '   
  7   2  '   j     �  G   �     �     �                    ,     5     >     G     S     Z  $   z     �     �  y  �  (   *  '   S  �   {  '   (     P  G   c     �     �  ,   �     �  ;        S     `     t     �  -   �  v   �  �   C     �     �     	                                                                                         
        \t<b>$alarm_set_to_text $hr:$mn</b>      \t<b>$alarm_text $hr:$mn...</b> \n    Are you sure to set the alarm for that time tomorrow? $alarm_set_to_text $hr:$mn
$cancel_text $are_you_sure_text $cancel_alarm_text !bash -c 'cancel_quit_tray $XPID $target'!gtk-cancel $confirm_title Alarm Alarm set to Are you sure??? Cancel Alarm Duration Hour::CB Interval Minutes::CB Repeat To cancel, right-click for menu To cancel, right-click icon for menu infinite seconds Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Nepali (https://www.transifex.com/antix-linux-community-contributions/teams/120110/ne/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: ne
Plural-Forms: nplurals=2; plural=(n != 1);
     \t<b>$alarm_set_to_text $hr:$mn</b>      \t<b>$alarm_text $hr:$mn...</b> \n    के तपाइँ भोलि त्यो समयको लागि अलार्म सेट गर्न निश्चित हुनुहुन्छ? $alarm_set_to_text $hr:$mn
$cancel_text $are_you_sure_text $cancel_alarm_text !bash -c 'cancel_quit_tray $XPID $target'!gtk-cancel $confirm_title अलार्म अलार्म सेट गरियो साच्चै हो??? अलार्म रद्द गर्नुहोस् अवधि घण्टा::CB अन्तराल मिनेट::CB दोहोर्याउनुहोस् रद्द गर्न, मेनुको लागि दायाँ क्लिक गर्नुहोस् रद्द गर्न, मेनुको लागि आइकनमा दायाँ क्लिक गर्नुहोस् अनन्त सेकेन्ड 