��    (      \  5   �      p     q     }     �     �  
   �     �     �     �     �     �  ,   �     �                      F        f  
   k     v     �     �     �     �     �     �     �     �     �     �     �                    &     ,     3     ;     D  y  P     �  	   �     �     �  $     	   8  1   B     t     �  1   �  �   �  %   V     |     �     �     �  K  �     &
  !   3
  8   U
     �
     �
     �
     �
  ]   �
     D  7   ]  %   �     �  "   �     �  	   
       N   4     �     �     �     �     �                       
          	   $                                                     &   (       !          %          "                             #                    '        Accessories All Audio Browser Calculator Clock Control Centre Development E-mail Exit Favorite Apps: (click here to edit the list) File Manager Files Games Graphics Internet Left click- Add,move,remove Toolbar icons; Right click- Manage Toolbar Menu Multimedia Network manager - Connman News Office Pics Recent Search for programs and files Settings Show Desktop Stop App System Task switcher Terminal Text Text Editor Unplug USB drives Video Volume Weather Weather  Web Browser Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Nepali (https://www.transifex.com/antix-linux-community-contributions/teams/120110/ne/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: ne
Plural-Forms: nplurals=2; plural=(n != 1);
 सामानहरू सबै अडियो ब्राउजर क्याल्कुलेटर घडी नियन्त्रण केन्द्र विकास इ-मेल बाहिर निस्कनुहोस् मनपर्ने एपहरू: (सूची सम्पादन गर्न यहाँ क्लिक गर्नुहोस्) फाइल प्रबन्धक फाइलहरू खेलहरू ग्राफिक्स इन्टरनेट बायाँ क्लिक - थप्नुहोस्, सार्नुहोस्, उपकरणपट्टी आइकनहरू हटाउनुहोस्; दायाँ क्लिक गर्नुहोस् - उपकरणपट्टी व्यवस्थापन गर्नुहोस् मेनु मल्टिमिडिया नेटवर्क प्रबन्धक - Connman समाचार कार्यालय तस्बिरहरू हालको कार्यक्रमहरू र फाइलहरू खोज्नुहोस् सेटिङहरू डेस्कटप देखाउनुहोस् एप रोक्नुहोस् प्रणाली टास्क स्विचर टर्मिनल पाठ पाठ सम्पादक USB ड्राइभहरू अनप्लग गर्नुहोस् भिडियो भोल्युम मौसम मौसम वेब ब्राउजर 