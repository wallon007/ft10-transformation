��          �   %   �      P     Q  "   f     �  1   �     �     �     �  	   �  !        %     =     B  '   H  !   p  #   �  #   �     �     �     �                1     R     g  
   n  �  y       +   5     a  _   �     �     �     �       #        :     S     X  3   ^  9   �  ?   �  1        >     N     i     q  2   �  8   �     �     	     
	        	                                                                             
                                        Alarm sound filename Ask to begin each set (all timers) Ask to begin each timer Auto close progress bar display when all sets end Configuration Duration Each timer end Finished. Interface to Sysmonitor Indicator Multiple Timer settings Name Never Number of times to run set (all timers) Pop-up message when each set ends Pop-up message when each timer ends Progress Bar update every x seconds Ready to start SUSPEND COMPUTER Seconds Seconds!Minutes Sound alarm when each set ends Sound alarm when each timer ends Timer duration units Timers has ended. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Irish (https://www.transifex.com/antix-linux-community-contributions/teams/120110/ga/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: ga
Plural-Forms: nplurals=5; plural=(n==1 ? 0 : n==2 ? 1 : n<7 ? 2 : n<11 ? 3 : 4);
 Ainm comhaid fuaime aláraim Iarr ar gach tacar a thosú (gach amadóir) Iarr ar gach lasc ama a thosú Taispeáin barra dul chun cinn a dhúnadh go huathoibríoch nuair a chríochnaíonn gach sraith Cumraíocht Fad Gach deireadh lasc ama Críochnaithe. Táscaire Comhéadain le Sysmonitor Socruithe Uaineadóir Il Ainm Riamh An líon uaireanta le socrú a rith (gach amadóir) Teachtaireacht aníos nuair a chríochnaíonn gach sraith Teachtaireacht aníos nuair a thagann deireadh le gach lasc ama Nuashonrú an Bharra Dul Chun Cinn gach x soicind Réidh le tosú AR FHIONRAÍODH RÍOMHAIRE Soicind Soicind!Nóiméad Aláram fuaime nuair a chríochnaíonn gach sraith Aláram fuaime nuair a thagann deireadh le gach lasc ama Aonaid ré lasc ama Timers tá deireadh leis. 