��    (      \  5   �      p     q     }     �     �  
   �     �     �     �     �     �  ,   �     �                      F        f  
   k     v     �     �     �     �     �     �     �     �     �     �     �                    &     ,     3     ;     D  �  P  
   �     �     �                    $     8     A     I  O   W     �     �     �  	   �  	   �  {   �  
   [  	   f     p     �     �     �     �  )   �  	   �     �     �     �     	     	     !	     (	     ;	     Y	     b	     h	     o	     v	                       
          	   $                                                     &   (       !          %          "                             #                    '        Accessories All Audio Browser Calculator Clock Control Centre Development E-mail Exit Favorite Apps: (click here to edit the list) File Manager Files Games Graphics Internet Left click- Add,move,remove Toolbar icons; Right click- Manage Toolbar Menu Multimedia Network manager - Connman News Office Pics Recent Search for programs and files Settings Show Desktop Stop App System Task switcher Terminal Text Text Editor Unplug USB drives Video Volume Weather Weather  Web Browser Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Irish (https://www.transifex.com/antix-linux-community-contributions/teams/120110/ga/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: ga
Plural-Forms: nplurals=5; plural=(n==1 ? 0 : n==2 ? 1 : n<7 ? 2 : n<11 ? 3 : 4);
 Gabhálais Gach Fuaime Brabhsálaí Áireamhán Clog Lárionad Rialaithe Forbairt R-phost An slí amach Na Feidhmchláir is Ansa leat: (cliceáil anseo chun an liosta a chur in eagar) Bainisteoir Comhad Comhaid Cluichí Grafaicí Idirlíon Cliceáil ar chlé - Cuir leis, bog, bain deilbhíní an Bharra Uirlisí; Cliceáil ar dheis - Bainistigh an Barra Uirlisí Roghchlár Ilmheáin Bainisteoir líonra - Connman Nuacht Oifig Pics le déanaí Cuardaigh le haghaidh cláir agus comhaid Socruithe Taispeáin Deasc Stop App Córas Malartóir tascanna Teirminéal Téacs Eagarthóir Téacs Dícheangail tiomántáin USB Físeán Toirt Aimsir Aimsir Brabhsálaí Gréasáin 