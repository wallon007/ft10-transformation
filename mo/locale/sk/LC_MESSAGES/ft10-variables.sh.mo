��    (      \  5   �      p     q     }     �     �  
   �     �     �     �     �     �  ,   �     �                      F        f  
   k     v     �     �     �     �     �     �     �     �     �     �     �                    &     ,     3     ;     D  �  P          %     -     2     >     J     Q     b     i     p  9   x     �     �     �     �     �  �   �     ~     �     �     �     �     �     �     �  
   �     �     	     +	     3	  	   D	     N	     S	     c	     x	     ~	     �	     �	     �	                       
          	   $                                                     &   (       !          %          "                             #                    '        Accessories All Audio Browser Calculator Clock Control Centre Development E-mail Exit Favorite Apps: (click here to edit the list) File Manager Files Games Graphics Internet Left click- Add,move,remove Toolbar icons; Right click- Manage Toolbar Menu Multimedia Network manager - Connman News Office Pics Recent Search for programs and files Settings Show Desktop Stop App System Task switcher Terminal Text Text Editor Unplug USB drives Video Volume Weather Weather  Web Browser Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Slovak (https://www.transifex.com/antix-linux-community-contributions/teams/120110/sk/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: sk
Plural-Forms: nplurals=4; plural=(n % 1 == 0 && n == 1 ? 0 : n % 1 == 0 && n >= 2 && n <= 4 ? 1 : n % 1 != 0 ? 2: 3);
 Príslušenstvo Všetky Zvuk Prehliadač Kalkulačka Hodiny Riadiace centrum rozvoj E-mail Východ Obľúbené aplikácie: (kliknutím sem upravíte zoznam) Správca súborov Súbory Hry Grafika internet Kliknutie ľavým tlačidlom - Pridať, presunúť, odstrániť ikony panela s nástrojmi; Kliknite pravým tlačidlom myši - Spravovať panel s nástrojmi Ponuka Multimédiá Správca siete - Connman Správy Kancelária obrázky Nedávne Vyhľadajte programy a súbory nastavenie Zobraziť pracovnú plochu Zastaviť aplikáciu systém Prepínač úloh Terminál Text Textový editor Odpojte jednotky USB Video Objem Počasie Počasie Webový prehliadač 