��          T      �       �      �      �      �   
   �   k        p  �  �     F     \     x     �  �   �     2                                        $add_tz_text!add:FBTN $delete_tz_text!remove:FBTN ADD Time zone FT10-clock Insert Time Zone to be added to the World Clock \n (Enter nothing to search trough all available timezones) REMOVE Time zone Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Slovak (https://www.transifex.com/antix-linux-community-contributions/teams/120110/sk/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: sk
Plural-Forms: nplurals=4; plural=(n % 1 == 0 && n == 1 ? 0 : n % 1 == 0 && n >= 2 && n <= 4 ? 1 : n % 1 != 0 ? 2: 3);
 $add_tz_text!add:FBTN $delete_tz_text!remove:FBTN PRIDAŤ časové pásmo Hodiny FT10 Vložte časové pásmo, ktoré sa má pridať do svetového času \n (Ak chcete prehľadávať všetky dostupné časové pásma, nezadávajte nič) ODSTRÁNIŤ Časové pásmo 