��          �   %   �      P     Q  "   f     �  1   �     �     �     �  	   �  !        %     =     B  '   H  !   p  #   �  #   �     �     �     �                1     R     g  
   n  �  y      >  =   _  ,   �  Y   �     $     2     :     T  #   a  #   �     �     �  0   �  2   �  9     6   T     �     �     �     �  +   �  1   �     ,	  
   H	  
   S	        	                                                                             
                                        Alarm sound filename Ask to begin each set (all timers) Ask to begin each timer Auto close progress bar display when all sets end Configuration Duration Each timer end Finished. Interface to Sysmonitor Indicator Multiple Timer settings Name Never Number of times to run set (all timers) Pop-up message when each set ends Pop-up message when each timer ends Progress Bar update every x seconds Ready to start SUSPEND COMPUTER Seconds Seconds!Minutes Sound alarm when each set ends Sound alarm when each timer ends Timer duration units Timers has ended. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Slovak (https://www.transifex.com/antix-linux-community-contributions/teams/120110/sk/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: sk
Plural-Forms: nplurals=4; plural=(n % 1 == 0 && n == 1 ? 0 : n % 1 == 0 && n >= 2 && n <= 4 ? 1 : n % 1 != 0 ? 2: 3);
 Názov súboru zvukového alarmu Požiadať o spustenie každého súboru (všetky časovače) Požiadajte o spustenie každého časovača Automatické zatvorenie zobrazenia indikátora priebehu po skončení všetkých súborov Konfigurácia Trvanie Každý koniec časovača Dokončené. Rozhranie pre indikátor Sysmonitor Viacnásobné nastavenia časovača názov Nikdy Počet spustení nastavený (všetky časovače) Vyskakovacia správa, keď sa každý set skončí Vyskakovacie hlásenie po skončení každého časovača Aktualizácia indikátora priebehu každých x sekúnd Pripravený začať POZASTAVIŤ POČÍTAČ sekúnd sekúnd!Minúty Zvukový alarm po skončení každého setu Zvukový alarm po skončení každého časovača Jednotky trvania časovača Časovače skončilo. 