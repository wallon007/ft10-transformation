��    
      l      �       �      �   (     .   ,  0   [     �     �  )   �     �     �  �       �  ?   �  E   #  F   i     �  %   �  B   �     -     K                         	                
    ADD ICON!add:FBTN Choose application to add to the Toolbar Double click any Application to move its icon: Double click any Application to remove its icon: Icon located! MOVE ICON!gtk-go-back-rtl:FBTN No icon located, using default Gears icon REMOVE ICON!remove:FBTN Tint2 icons Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:39+0000
Last-Translator: Robin, 2022
Language-Team: Slovak (https://www.transifex.com/antix-linux-community-contributions/teams/120110/sk/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: sk
Plural-Forms: nplurals=4; plural=(n % 1 == 0 && n == 1 ? 0 : n % 1 == 0 && n >= 2 && n <= 4 ? 1 : n % 1 != 0 ? 2: 3);
 PRIDAŤ IKONU!add:FBTN Vyberte aplikáciu, ktorú chcete pridať na Panel s nástrojmi Dvojitým kliknutím na ľubovoľnú aplikáciu presuniete jej ikonu: Dvojitým kliknutím na ľubovoľnú aplikáciu odstránite jej ikonu: Ikona sa nachádza! PRESUNÚŤ IKONU!gtk-go-back-rtl:FBTN Žiadna ikona sa nenachádza, používa sa predvolená ikona Gears ODSTRÁNIŤ IKONU!remove:FBTN Ikony Tint2 