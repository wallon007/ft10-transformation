��          T      �       �      �      �      �   
   �   k        p  x  �     �          ,  	   ?  t   I     �                                        $add_tz_text!add:FBTN $delete_tz_text!remove:FBTN ADD Time zone FT10-clock Insert Time Zone to be added to the World Clock \n (Enter nothing to search trough all available timezones) REMOVE Time zone Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Dutch (https://www.transifex.com/antix-linux-community-contributions/teams/120110/nl/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: nl
Plural-Forms: nplurals=2; plural=(n != 1);
 $add_tz_text!add:FBTN $delete_tz_text!remove:FBTN TOEVOEGEN Tijdzone FT10-klok Tijdzone invoegen om aan de wereldklok toe te voegen \n (Voer niets in om door alle beschikbare tijdzones te zoeken) VERWIJDER Tijdzone 