��          �   %   �      P     Q  "   f     �  1   �     �     �     �  	   �  !        %     =     B  '   H  !   p  #   �  #   �     �     �     �                1     R     g  
   n  �  y  3   >  H   r  B   �  r   �     q     �  $   �     �  ?   �  ,        9     @  G   M  J   �  P   �  N   1	  "   �	     �	     �	     �	  D   �	  J   3
  .   ~
     �
     �
        	                                                                             
                                        Alarm sound filename Ask to begin each set (all timers) Ask to begin each timer Auto close progress bar display when all sets end Configuration Duration Each timer end Finished. Interface to Sysmonitor Indicator Multiple Timer settings Name Never Number of times to run set (all timers) Pop-up message when each set ends Pop-up message when each timer ends Progress Bar update every x seconds Ready to start SUSPEND COMPUTER Seconds Seconds!Minutes Sound alarm when each set ends Sound alarm when each timer ends Timer duration units Timers has ended. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Serbian (https://www.transifex.com/antix-linux-community-contributions/teams/120110/sr/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: sr
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
 Назив датотеке звука аларма Тражи да започне сваки сет (сви тајмери) Затражите да започнете сваки тајмер Аутоматско затварање траке напретка када се сви сетови заврше Конфигурација Трајање Крај сваког тајмера Готов. Интерфејс за Сисмонитор индикатор Више подешавања тајмера Име Никада Број покретања је подешен (сви тајмери) Искачућа порука када се сваки сет заврши Искачућа порука када се сваки тајмер заврши Трака напретка се ажурира сваких к секунди Спремни за почетак СУСПЕНД ЦОМПУТЕР Секунде Секунде!Минута Звучни аларм када се сваки сет заврши Звучни аларм када се сваки тајмер заврши Јединице трајања тајмера Тајмери завршило се. 