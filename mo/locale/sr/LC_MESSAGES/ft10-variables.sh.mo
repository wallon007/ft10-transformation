��    (      \  5   �      p     q     }     �     �  
   �     �     �     �     �     �  ,   �     �                      F        f  
   k     v     �     �     �     �     �     �     �     �     �     �     �                    &     ,     3     ;     D  �  P          "  
   )     4     G     \     c     �     �  
   �  j   �          *     9     B     Q  �   b     0	     9	  ,   P	  
   }	     �	     �	     �	  7   �	     �	  *   
  %   1
     W
  %   d
     �
  
   �
     �
  (   �
  
   �
     �
  
   �
  
   
  '                          
          	   $                                                     &   (       !          %          "                             #                    '        Accessories All Audio Browser Calculator Clock Control Centre Development E-mail Exit Favorite Apps: (click here to edit the list) File Manager Files Games Graphics Internet Left click- Add,move,remove Toolbar icons; Right click- Manage Toolbar Menu Multimedia Network manager - Connman News Office Pics Recent Search for programs and files Settings Show Desktop Stop App System Task switcher Terminal Text Text Editor Unplug USB drives Video Volume Weather Weather  Web Browser Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Serbian (https://www.transifex.com/antix-linux-community-contributions/teams/120110/sr/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: sr
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
 Прибор Све Аудио Прегледач Калкулатор Сат Контролни центар Развој Е-маил Изађи Омиљене апликације: (кликните овде да бисте уредили листу) Филе Манагер Фајлови Игре Графика Интернет Кликните левим кликом - Додај, помери, уклони иконе на траци са алаткама; Десни клик - Управљај траком са алаткама Мени Мултимедија Мрежни менаџер - Цоннман Вести Канцеларија Пицс Скорашњи Потражите програме и датотеке Подешавања Прикажи радну површину Заустави апликацију Систем Пребацивач задатака Терминал Текст Текст едитор Искључите УСБ дискове Видео Волуме Време Време Интернет претраживач 