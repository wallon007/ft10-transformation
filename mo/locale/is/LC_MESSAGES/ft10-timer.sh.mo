��          �   %   �      P     Q  "   f     �  1   �     �     �     �  	   �  !        %     =     B  '   H  !   p  #   �  #   �     �     �     �                1     R     g  
   n  �  y  !     2   .  &   a  ?   �  
   �     �     �     �     �          -     2  8   9  1   r  7   �  /   �          $  	   3     =  ,   S  2   �     �     �  
   �        	                                                                             
                                        Alarm sound filename Ask to begin each set (all timers) Ask to begin each timer Auto close progress bar display when all sets end Configuration Duration Each timer end Finished. Interface to Sysmonitor Indicator Multiple Timer settings Name Never Number of times to run set (all timers) Pop-up message when each set ends Pop-up message when each timer ends Progress Bar update every x seconds Ready to start SUSPEND COMPUTER Seconds Seconds!Minutes Sound alarm when each set ends Sound alarm when each timer ends Timer duration units Timers has ended. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Icelandic (https://www.transifex.com/antix-linux-community-contributions/teams/120110/is/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: is
Plural-Forms: nplurals=2; plural=(n % 10 != 1 || n % 100 == 11);
 Skráarheiti viðvörunarhljóðs Biðja um að hefja hvert sett (allir tímamælir) Biðjið um að hefja hvern tímamæli Sjálfvirk lokun framvindustikunnar þegar öllum settum lýkur Stillingar Lengd Hver tímamælir lýkur Lokið. Tengi við Sysmonitor Vísir Margar tímastillingar Nafn Aldrei Fjöldi tíma til að keyra stilltur (allir tímamælir) Sprettigluggaskilaboð þegar hverju setti lýkur Sprettigluggaskilaboð þegar hverjum tímamæli lýkur Framvindustika uppfærsla á x sekúndna fresti Tilbúinn til að byrja FÆRÐU TÖLVU Sekúndur Sekúndur!Fundargerð Hljóðviðvörun þegar hverju setti lýkur Hljóðviðvörun þegar hverjum tímamæli lýkur Tímamælir lengdareiningar Tímamælir er lokið. 