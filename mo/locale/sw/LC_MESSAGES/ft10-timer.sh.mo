��          �   %   �      P     Q  "   f     �  1   �     �     �     �  	   �  !        %     =     B  '   H  !   p  #   �  #   �     �     �     �                1     R     g  
   n  z  y  !   �  *        A  C   ^     �     �     �     �  &   �     �          #  9   )  "   c  )   �  #   �     �     �     �     �  $   	  +   .     Z     y  
   �        	                                                                             
                                        Alarm sound filename Ask to begin each set (all timers) Ask to begin each timer Auto close progress bar display when all sets end Configuration Duration Each timer end Finished. Interface to Sysmonitor Indicator Multiple Timer settings Name Never Number of times to run set (all timers) Pop-up message when each set ends Pop-up message when each timer ends Progress Bar update every x seconds Ready to start SUSPEND COMPUTER Seconds Seconds!Minutes Sound alarm when each set ends Sound alarm when each timer ends Timer duration units Timers has ended. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Swahili (https://www.transifex.com/antix-linux-community-contributions/teams/120110/sw/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: sw
Plural-Forms: nplurals=2; plural=(n != 1);
 Jina la faili la sauti ya kengele Uliza kuanza kila seti (vipima muda vyote) Uliza kuanza kila kipima saa Funga onyesho la upau wa maendeleo kiotomatiki seti zote zinapoisha Usanidi Muda Kila mwisho wa kipima muda Imekamilika. Kiolesura cha Kiashiria cha Sysmonitor Mipangilio ya Kipima saa nyingi Jina Kamwe Idadi ya nyakati za kutekeleza kuweka (vipima muda vyote) Ujumbe ibukizi kila seti inapoisha Ujumbe ibukizi kila kipima saa kinapoisha Progress Bar sasisha kila sekunde x Tayari kuanza SIMAMA COMPUTER Sekunde Sekunde!Dakika Kengele ya sauti kila seti inapoisha Kengele ya sauti kila kipima saa kinapoisha Vipimo vya muda wa kipima muda Vipima muda imekwisha. 