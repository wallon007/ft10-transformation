��    (      \  5   �      p     q     }     �     �  
   �     �     �     �     �     �  ,   �     �                      F        f  
   k     v     �     �     �     �     �     �     �     �     �     �     �                    &     ,     3     ;     D  �  P               "  	   (     2     >     C  
   P     [  	   b  L   l     �     �  	   �  
   �     �  �   �     �     �     �     �     �     �     �  (   �     	     	     .	     @	     H	     [	     d	     j	     {	     �	     �	     �	     �	     �	                       
          	   $                                                     &   (       !          %          "                             #                    '        Accessories All Audio Browser Calculator Clock Control Centre Development E-mail Exit Favorite Apps: (click here to edit the list) File Manager Files Games Graphics Internet Left click- Add,move,remove Toolbar icons; Right click- Manage Toolbar Menu Multimedia Network manager - Connman News Office Pics Recent Search for programs and files Settings Show Desktop Stop App System Task switcher Terminal Text Text Editor Unplug USB drives Video Volume Weather Weather  Web Browser Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Gaelic, Scottish (https://www.transifex.com/antix-linux-community-contributions/teams/120110/gd/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: gd
Plural-Forms: nplurals=4; plural=(n==1 || n==11) ? 0 : (n==2 || n==12) ? 1 : (n > 2 && n < 20) ? 2 : 3;
 Accessories Uile Fuaim Brabhsair Àireamhair Cloc Ionad-smachd leasachadh Post-d Ar-a-mach Na h-aplacaidean as fheàrr leat: (cliog an seo gus an liosta a dheasachadh) Manaidsear faidhle Faidhlichean Geamannan Grafaigean Eadar-lìon Cliog air chlì - Cuir ris, gluais, thoir air falbh ìomhaighean a’ Chrann-inneal; Dèan briogadh deas - Stiùirich an Crann Inneal Clàr-taice Ioma-mheadhan Manaidsear lìonra - Connman Naidheachdan Oifis Dealbhan O chionn ghoirid Lorg airson prògraman agus faidhlichean Suidhichidhean Taisbeanadh Deasg Cuir stad air App Siostam Switcher gnìomhan Terminal Teacs Deasaiche teacsa Thoir air falbh draibhearan USB Bhideo Toirt Aimsir Aimsir Brabhsair-lìn 