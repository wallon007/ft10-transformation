��    (      \  5   �      p     q     }     �     �  
   �     �     �     �     �     �  ,   �     �                      F        f  
   k     v     �     �     �     �     �     �     �     �     �     �     �                    &     ,     3     ;     D  �  P  	   �     �     �  	   �  
   �     �       	             $  1   )     [     h     n     t  	   |  b   �     �  
   �     �                     '     -     L     Z     i     u     |     �     �     �     �     �     �     �     �  	   �                       
          	   $                                                     &   (       !          %          "                             #                    '        Accessories All Audio Browser Calculator Clock Control Centre Development E-mail Exit Favorite Apps: (click here to edit the list) File Manager Files Games Graphics Internet Left click- Add,move,remove Toolbar icons; Right click- Manage Toolbar Menu Multimedia Network manager - Connman News Office Pics Recent Search for programs and files Settings Show Desktop Stop App System Task switcher Terminal Text Text Editor Unplug USB drives Video Volume Weather Weather  Web Browser Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Norwegian Bokmål (https://www.transifex.com/antix-linux-community-contributions/teams/120110/nb/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: nb
Plural-Forms: nplurals=2; plural=(n != 1);
 Tilbehør Alle Lyd Nettleser Kalkulator Klokke Kontrollsenter Utvikling E-post Exit Favorittapper: (klikk her for å redigere listen) Filbehandler Filer Spill Grafikk Internett Venstre klikk- Legg til, flytt, fjern verktøylinjeikoner; Høyreklikk - Administrer verktøylinje Meny Multimedia Nettverkssjef - Connman Nyheter Kontor Bilder Nylig Søk etter programmer og filer Innstillinger Vis skrivebord Stopp appen System Oppgavebytter Terminal Tekst Tekstredigerer Koble fra USB-stasjoner Video Volum Vær Vær Nettleser 