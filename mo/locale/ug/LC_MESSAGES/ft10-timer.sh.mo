��          �   %   �      P     Q  "   f     �  1   �     �     �     �  	   �  !        %     =     B  '   H  !   p  #   �  #   �     �     �     �                1     R     g  
   n  y  y  S   �  �   G  _   �  �   5     �       D        S  S   s  .   �     �  *   	  Q   1	  n   �	  p   �	  {   c
  *   �
     
       "   .  p   Q  p   �  @   3     t     {        	                                                                             
                                        Alarm sound filename Ask to begin each set (all timers) Ask to begin each timer Auto close progress bar display when all sets end Configuration Duration Each timer end Finished. Interface to Sysmonitor Indicator Multiple Timer settings Name Never Number of times to run set (all timers) Pop-up message when each set ends Pop-up message when each timer ends Progress Bar update every x seconds Ready to start SUSPEND COMPUTER Seconds Seconds!Minutes Sound alarm when each set ends Sound alarm when each timer ends Timer duration units Timers has ended. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Uyghur (https://www.transifex.com/antix-linux-community-contributions/teams/120110/ug/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: ug
Plural-Forms: nplurals=2; plural=(n != 1);
 ﻰﻤﺴﯩﺋ ﯔﯩﻨﯨﺯﺍﯞﺎﺋ ﺵﯗﺭﯗﺪﻧﻼ﻿ﮬﺎﮔﺎﺋ (ﺭﻼ﻿ﺘﯩﻗﺍﯞ ﻖﯩﻟﺭﺎﺑ) ﯔﯩﻠﯩﻗ ﭖەﻟەﺗ ﻰﻨﺷﻼ﻿ﺷﺎﺑ ﻰﻨﺷﯛﺭﯜﻳ ﺮﯩﺑ ﺭەﮬ ﯔﯩﻠﯩﻗ ﭖەﻟەﺗ ﻰﻨﺷﻼ﻿ﺷﺎﺑ ﻰﻨﺘﯩﻗﺍﯞ ﺮﯩﺑ ﺭەﮬ ﯗﺪﯩﺘﯩﺳﺭﯚﻛ ﻰﻨﯩﻘﯨﺪﻟﺎﺑ ﺵﺎﻗﺎﺗ ﻚﯩﺗﺎﻣﻮﺘﭘﺎﺋ ﺍﺪﻧﺎﻘﺷﻻ﻿ﺮﯩﺧﺎﺋ ﺭەﻠﺷﯛﺭﯜﻳ ەﻤﻣەﮬ ﻰﺴﯩﻤﯩﻠﭘەﺳ Duration ﯗﺪﯩﺸﯩﻟﺮﯩﺧﺎﺋ ﺖﯩﻗﺍﯞ ﺮﯩﺑ ﺭەﮬ .ﻯﺪﻧﻼ﻿ﻣﺎﻣﺎﺗ Sysmonitor ﻯﺯﯜﻳ ەﻤﻧﯛﺭﯚﻛ ﯔﯩﻨﯩﭼﯜﻜﺗەﺳﺭﯚﻛ ﻰﻜﯩﺸﯕەﺗ ﺖﯩﻗﺍﯞ ﭖﯚﻛ ﻰﻤﺴﯩﺋ ﯗﺪﻳﺎﻤﻟﻮﺑ ﺰﯩﮔﺭەﮬ (ﺖﯩﻗﺍﯞ ﻖﯩﻟﺭﺎﺑ) ﻰﻧﺎﺳ ﻢﯩﺘﯧﻗ ﺵﯛﺭﯜﻳ ﺭﯘﭼﯘﺋ ەﻤﯨﺮﻛەﺳ ﺍﺪﻧﺎﻘﺷﻻ﻿ﺮﯩﺧﺎﺋ ﺵﯛﺭﯜﻳ ﺮﯩﺑ ﺭەﮬ ﻯﺭﯘﭼﯘﺋ ﺶﯩﻘﯕﺎﻗ ﺍﺪﻧﺎﻘﺷﻻ﻿ﺮﯩﺧﺎﺋ ﺖﯩﻗﺍﯞ ﺮﯩﺑ ﺭەﮬ ﺵﻼ﻿ﯩﯖﯧﻳ ﻰﻨﯩﻘﯨﺪﻟﺎﺑ ﺵەﻠﯨﺮﯩﮕﻠﯩﺋ ﺎﺘﺘﻧﯘﻜﯧﺳ ﺮﯩﺑ ﺭەﮬ ﺭﺎﻴﻳەﺗ ﺎﻘﺷﻼ﻿ﺷﺎﺑ SUSPEND COMPUTER ﺖﻧﯘﻜﯧﺳ ﺖﻧﯘﻜﯧﺳ!ﺕﯘﻨﯩﻣ ﻰﻟﺎﻨﮕﯩﺳ ﺯﺍﯞﺎﺋ ﺍﺪﻧﺎﻘﺷﻻ﻿ﺮﯩﺧﺎﺋ ﺵﯛﺭﯜﻳ ﺮﯩﺑ ﺭەﮬ ﻰﻟﺎﻨﮕﯩﺳ ﺯﺍﯞﺎﺋ ﺍﺪﻧﺎﻘﺷﻻ﻿ﺮﯩﺧﺎﺋ ﺖﯩﻗﺍﯞ ﺮﯩﺑ ﺭەﮬ ﻰﻜﯩﻟﺮﯩﺑ ﻰﺴﯩﻤﯩﻠﻛەﭼ ﺖﯩﻗﺍﯞ Timers .ﻰﺘﺷﻻ﻿ﺮﯩﺧﺎﺋ 