��          �   %   �      P     Q  "   f     �  1   �     �     �     �  	   �  !        %     =     B  '   H  !   p  #   �  #   �     �     �     �                1     R     g  
   n  �  y       *        D  7   c     �     �     �     �     �     �     �        (        1  !   Q  &   s     �     �     �     �     �     �               $        	                                                                             
                                        Alarm sound filename Ask to begin each set (all timers) Ask to begin each timer Auto close progress bar display when all sets end Configuration Duration Each timer end Finished. Interface to Sysmonitor Indicator Multiple Timer settings Name Never Number of times to run set (all timers) Pop-up message when each set ends Pop-up message when each timer ends Progress Bar update every x seconds Ready to start SUSPEND COMPUTER Seconds Seconds!Minutes Sound alarm when each set ends Sound alarm when each timer ends Timer duration units Timers has ended. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Haitian (Haitian Creole) (https://www.transifex.com/antix-linux-community-contributions/teams/120110/ht/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: ht
Plural-Forms: nplurals=2; plural=(n != 1);
 Non fichye son alam Mande pou kòmanse chak seri (tout revèy) Mande pou kòmanse chak revèy Auto fèmen pwogrè ba ekspozisyon lè tout ansanm fini Konfigirasyon Dire Chak fen revèy Fini. Entèfas Sysmonitor Endikatè Anviwònman revèy miltip Non Pa janm Kantite fwa pou kouri mete (tout timers) Mesaj pop-up lè chak seri fini Mesaj pop-up lè chak revèy fini Aktyalizasyon Ba Pwogrè chak x segonn Pare pou kòmanse SISPANN ÒDINATÈ Segond Segond!Minit Son alam lè chak seri fini Son alam lè chak revèy fini Inite dire revèy Timer te fini. 