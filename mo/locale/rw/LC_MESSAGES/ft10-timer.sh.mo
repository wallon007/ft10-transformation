��          �   %   �      P     Q  "   f     �  1   �     �     �     �  	   �  !        %     =     B  '   H  !   p  #   �  #   �     �     �     �                1     R     g  
   n  ~  y     �  &        7  7   P     �     �     �     �     �     �     �     �  )      *   *  +   U  '   �     �     �  
   �     �  &   �  '        :     H     N        	                                                                             
                                        Alarm sound filename Ask to begin each set (all timers) Ask to begin each timer Auto close progress bar display when all sets end Configuration Duration Each timer end Finished. Interface to Sysmonitor Indicator Multiple Timer settings Name Never Number of times to run set (all timers) Pop-up message when each set ends Pop-up message when each timer ends Progress Bar update every x seconds Ready to start SUSPEND COMPUTER Seconds Seconds!Minutes Sound alarm when each set ends Sound alarm when each timer ends Timer duration units Timers has ended. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Kinyarwanda (https://www.transifex.com/antix-linux-community-contributions/teams/120110/rw/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: rw
Plural-Forms: nplurals=2; plural=(n != 1);
 Imenyekanisha ryamazina Saba gutangira buri seti (igihe cyose) Saba gutangira buri gihe Auto funga amajyambere yerekana iyo seti zose zirangiye Iboneza Ikiringo Buri gihe kirangiye Byarangiye. Imigaragarire kuri Sysmonitor Igenamiterere ryinshi Izina Nta na rimwe Inshuro zo gukora gushiraho (igihe cyose) Ubutumwa bwa pop-up iyo buri seti irangiye Ubutumwa bwa pop-up iyo buri gihe kirangiye Iterambere ryumurongo buri x amasegonda Witegure gutangira SUSPEND COMPUTER Amasegonda Amasegonda!Iminota Ijwi ryumvikana iyo buri seti irangiye Ijwi ryumvikana iyo buri gihe kirangiye Ibihe byigihe Ibihe byarangiye. 