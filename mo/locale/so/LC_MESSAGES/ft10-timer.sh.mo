��          �   %   �      P     Q  "   f     �  1   �     �     �     �  	   �  !        %     =     B  '   H  !   p  #   �  #   �     �     �     �                1     R     g  
   n  y  y      �  ?     $   T  U   y     �     �     �  
   �           (     @     G  1   M  -     3   �  1   �          ,  	   @     J  1   ^  6   �     �     �     �        	                                                                             
                                        Alarm sound filename Ask to begin each set (all timers) Ask to begin each timer Auto close progress bar display when all sets end Configuration Duration Each timer end Finished. Interface to Sysmonitor Indicator Multiple Timer settings Name Never Number of times to run set (all timers) Pop-up message when each set ends Pop-up message when each timer ends Progress Bar update every x seconds Ready to start SUSPEND COMPUTER Seconds Seconds!Minutes Sound alarm when each set ends Sound alarm when each timer ends Timer duration units Timers has ended. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Somali (https://www.transifex.com/antix-linux-community-contributions/teams/120110/so/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: so
Plural-Forms: nplurals=2; plural=(n != 1);
 Magaca faylka dhawaaqa alaarmiga Weydiiso inaad bilowdo set kasta (dhammaan saacad-bixiyeyaasha) Weydiiso in aad bilowdo saacad kasta Si otomaatig ah u xidh bandhiga bar horumarka marka ay dhammaan xidhidhisku dhammaato Habaynta Muddada Waqti kasta wuu dhamaanayaa Dhammaatay Interface to Sysmonitor Tusiyaha Dejinta Saacadaha badan Magaca Marna Tirada wakhtiyada la dajiyay (dhammaan saacadaha) Farriinta soo booda marka set kastaa dhamaado Farriinta soo booda marka saacad kasta uu dhammaado Horumarka Bar wuxuu cusboonaysiiyaa x sekan kasta Diyaar u ah inuu bilaabo KUMBUUTARKA LA JIRO Ilbiriqsi Ilbiriqsi!Daqiiqado Dhawaaqa qaylo-dhaanta marka set kastaa dhammaado Dhawaaqa qaylo-dhaanta marka saacad kasta uu dhammaado Unugyada muddada saacadaha Saacadayaasha wuu dhamaaday. 