��          �   %   �      P     Q  "   f     �  1   �     �     �     �  	   �  !        %     =     B  '   H  !   p  #   �  #   �     �     �     �                1     R     g  
   n  �  y     =  5   Y  +   �  E   �                    +      3     T     k     o  1   v  &   �  1   �  +        -     A     V     ^  $   p  0   �     �     �     �        	                                                                             
                                        Alarm sound filename Ask to begin each set (all timers) Ask to begin each timer Auto close progress bar display when all sets end Configuration Duration Each timer end Finished. Interface to Sysmonitor Indicator Multiple Timer settings Name Never Number of times to run set (all timers) Pop-up message when each set ends Pop-up message when each timer ends Progress Bar update every x seconds Ready to start SUSPEND COMPUTER Seconds Seconds!Minutes Sound alarm when each set ends Sound alarm when each timer ends Timer duration units Timers has ended. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Croatian (https://www.transifex.com/antix-linux-community-contributions/teams/120110/hr/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: hr
Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
 Naziv datoteke zvuka alarma Zatraži da započne svaki set (svi mjerači vremena) Zatražite početak svakog mjerača vremena Automatsko zatvaranje prikaza trake napretka kada svi skupovi završe Konfiguracija Trajanje Kraj svakog timera Gotovo. Sučelje za Sysmonitor indikator Više postavki tajmera Ime Nikada Broj pokretanja postavljen (svi mjerači vremena) Skočna poruka kada svaki skup završi Skočna poruka kada završi svaki mjerač vremena Traka napretka ažurira se svakih x sekundi Spremni za početak OBUSTAVITI RAČUNALO Sekunde Sekunde!Zapisnici Zvučni alarm kada završi svaki set Zvučni alarm kada završi svaki mjerač vremena Jedinice trajanja tajmera Tajmeri je završio. 