��    (      \  5   �      p     q     }     �     �  
   �     �     �     �     �     �  ,   �     �                      F        f  
   k     v     �     �     �     �     �     �     �     �     �     �     �                    &     ,     3     ;     D  �  P  	   �     �     �            
   !     ,     <     H     P  K   V     �     �     �     �  	   �  �   �     Z  	   c     m     �     �     �     �     �     �     �     �     �      	     	     $	     +	     <	     T	     Z	     c	     r	     �	                       
          	   $                                                     &   (       !          %          "                             #                    '        Accessories All Audio Browser Calculator Clock Control Centre Development E-mail Exit Favorite Apps: (click here to edit the list) File Manager Files Games Graphics Internet Left click- Add,move,remove Toolbar icons; Right click- Manage Toolbar Menu Multimedia Network manager - Connman News Office Pics Recent Search for programs and files Settings Show Desktop Stop App System Task switcher Terminal Text Text Editor Unplug USB drives Video Volume Weather Weather  Web Browser Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Latvian (https://www.transifex.com/antix-linux-community-contributions/teams/120110/lv/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: lv
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n != 0 ? 1 : 2);
 Piederumi Visi Audio Pārlūkprogramma Kalkulators Pulkstenis Vadības centrs Attīstība E-pasts Izeja Iecienītākās lietotnes: (noklikšķiniet šeit, lai rediģētu sarakstu) Failu menedžeris Faili Spēles Grafika Internets Kreisais klikšķis — Pievienot, pārvietot, noņemt rīkjoslas ikonas; Ar peles labo pogu noklikšķiniet - Pārvaldīt rīkjoslu Izvēlne Multivide Tīkla pārvaldnieks - Connman Jaunumi Birojs Bildes Nesen Meklēt programmas un failus Iestatījumi Rādīt darbvirsmu Apturēt lietotni Sistēma Uzdevumu pārslēdzējs Terminālis Teksts Teksta redaktors Atvienojiet USB diskus Video Skaļums Laikapstākļi Laikapstākļi Interneta pārlūks 