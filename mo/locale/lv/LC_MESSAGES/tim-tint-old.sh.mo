��    
      l      �       �      �   (     .   ,  0   [     �     �  )   �     �     �  �       �  /   �  S   �  P   B     �  *   �  >   �          &                         	                
    ADD ICON!add:FBTN Choose application to add to the Toolbar Double click any Application to move its icon: Double click any Application to remove its icon: Icon located! MOVE ICON!gtk-go-back-rtl:FBTN No icon located, using default Gears icon REMOVE ICON!remove:FBTN Tint2 icons Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:39+0000
Last-Translator: Robin, 2022
Language-Team: Latvian (https://www.transifex.com/antix-linux-community-contributions/teams/120110/lv/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: lv
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n != 0 ? 1 : 2);
 PIEVIENOT IKONU!add:FBTN Izvēlieties programmu, ko pievienot rīkjoslai Veiciet dubultklikšķi uz jebkuras lietojumprogrammas, lai pārvietotu tās ikonu: Veiciet dubultklikšķi uz jebkuras lietojumprogrammas, lai noņemtu tās ikonu: Ikona atrodas! PĀRVIETOŠANAS IKONA!gtk-go-back-rtl:FBTN Nav atrasta neviena ikona, izmantojot noklusējuma Gears ikonu NOŅEMT IKONU!remove:FBTN Tint2 ikonas 