��          �   %   �      P     Q  "   f     �  1   �     �     �     �  	   �  !        %     =     B  '   H  !   p  #   �  #   �     �     �     �                1     R     g  
   n  y  y  &   �  .        I  U   e     �     �     �     �  0   �          ;     G      O  D   p  8   �  6   �     %  !   6     X     a  7   r  6   �     �     �     	        	                                                                             
                                        Alarm sound filename Ask to begin each set (all timers) Ask to begin each timer Auto close progress bar display when all sets end Configuration Duration Each timer end Finished. Interface to Sysmonitor Indicator Multiple Timer settings Name Never Number of times to run set (all timers) Pop-up message when each set ends Pop-up message when each timer ends Progress Bar update every x seconds Ready to start SUSPEND COMPUTER Seconds Seconds!Minutes Sound alarm when each set ends Sound alarm when each timer ends Timer duration units Timers has ended. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: German (https://www.transifex.com/antix-linux-community-contributions/teams/120110/de/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: de
Plural-Forms: nplurals=2; plural=(n != 1);
 Dateiname der Audiodatei des Alarmtons Jede Einstellung einzeln beginnen (alle Timer) Jeden Timer einzeln starten Fortschrittsbalken automatisch ausblenden, nachdem alle Einstellungen abgelaufen sind Konfiguration Dauer Alle Timer abgelaufen Beendet. Schnittstelle zur Anzeige der Systemüberwachung Mehrere Timer-Einstellungen Bezeichnung Niemals Anzahl der einzustellenden Timer Anzuzeigender Informationstext, wenn eine der Einstellungen abläuft Bildschirmnachricht, wenn einer der Timer abgelaufen ist Aktualisierung des Fortschrittsbalkens alle x Sekunden Bereit zum Start COMPUTER IN RUHEZUSTAND VERSETZEN Sekunden Sekunden!Minuten Alarmsignal geben, wenn eine der Einstellungen abläuft Akustischer Alarm, wenn einer der Timer abgelaufen ist Zeiteinheit für den Timer Timer ist beendet. 