��          �   %   �      P     Q  "   f     �  1   �     �     �     �  	   �  !        %     =     B  '   H  !   p  #   �  #   �     �     �     �                1     R     g  
   n  z  y  $   �  +        E  a   c     �     �     �     �  %   �          <     A  :   E  '   �  )   �  7   �     
          5     =  %   L  )   r     �     �  
   �        	                                                                             
                                        Alarm sound filename Ask to begin each set (all timers) Ask to begin each timer Auto close progress bar display when all sets end Configuration Duration Each timer end Finished. Interface to Sysmonitor Indicator Multiple Timer settings Name Never Number of times to run set (all timers) Pop-up message when each set ends Pop-up message when each timer ends Progress Bar update every x seconds Ready to start SUSPEND COMPUTER Seconds Seconds!Minutes Sound alarm when each set ends Sound alarm when each timer ends Timer duration units Timers has ended. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Italian (https://www.transifex.com/antix-linux-community-contributions/teams/120110/it/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: it
Plural-Forms: nplurals=2; plural=(n != 1);
 Nome del file del suono dell'allarme Chiedi di iniziare ogni set (tutti i timer) Chiedi di iniziare ogni timer Visualizzazione della barra di avanzamento della chiusura automatica al termine di tutte le serie Configurazione Durata Ogni timer finisce Finito. Interfaccia per indicatore Sysmonitor Impostazioni multiple del timer Nome Mai Numero di volte per l'esecuzione impostato (tutti i timer) Messaggio pop-up al termine di ogni set Messaggio pop-up al termine di ogni timer Aggiornamento della barra di avanzamento ogni x secondi Pronto per iniziare SOSPENDERE IL COMPUTER Secondi Secondi!Minuti Allarme sonoro al termine di ogni set Allarme sonoro allo scadere di ogni timer Unità di durata del timer Temporizzatori ha finito. 