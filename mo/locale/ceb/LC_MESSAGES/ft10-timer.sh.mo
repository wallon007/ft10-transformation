��          �   %   �      P     Q  "   f     �  1   �     �     �     �  	   �  !        %     =     B  '   H  !   p  #   �  #   �     �     �     �                1     R     g  
   n  |  y     �  6     #   B  K   f     �  	   �     �     �  !   �          *  
   1  ?   <  -   |  /   �  *   �               ,     4  *   G  -   r     �  	   �     �        	                                                                             
                                        Alarm sound filename Ask to begin each set (all timers) Ask to begin each timer Auto close progress bar display when all sets end Configuration Duration Each timer end Finished. Interface to Sysmonitor Indicator Multiple Timer settings Name Never Number of times to run set (all timers) Pop-up message when each set ends Pop-up message when each timer ends Progress Bar update every x seconds Ready to start SUSPEND COMPUTER Seconds Seconds!Minutes Sound alarm when each set ends Sound alarm when each timer ends Timer duration units Timers has ended. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Cebuano (https://www.transifex.com/antix-linux-community-contributions/teams/120110/ceb/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: ceb
Plural-Forms: nplurals=2; plural=(n != 1);
 Alarm sound filename Hangyoa nga sugdan ang matag set (tanan nga mga timer) Hangyoa ang pagsugod sa matag timer Awtomatikong pagsira sa progress bar display kung ang tanan nga set matapos Pag-configure Gidugayon Ang matag timer matapos Nahuman. Interface sa Sysmonitor Indicator Daghang mga setting sa Timer Ngalan Dili gayud Gidaghanon sa mga higayon sa pagdagan set (tanan nga mga timer) Pop-up nga mensahe kung ang matag set matapos Pop-up nga mensahe kung matapos ang matag timer Pag-update sa Progress Bar matag x segundo Andam sa pagsugod SUSPEND ANG KOMPUTER Segundo Segundo!Mga minuto Tunog ang alarm kung matapos ang matag set Tunog ang alarma kung matapos ang matag timer Mga yunit sa gidugayon sa timer Mga timer natapos na. 