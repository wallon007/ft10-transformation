��          T      �       �      �      �      �   
   �   k        p  w  �     �          +     K  X  c  A   �                                        $add_tz_text!add:FBTN $delete_tz_text!remove:FBTN ADD Time zone FT10-clock Insert Time Zone to be added to the World Clock \n (Enter nothing to search trough all available timezones) REMOVE Time zone Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Odia (https://www.transifex.com/antix-linux-community-contributions/teams/120110/or/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: or
Plural-Forms: nplurals=2; plural=(n != 1);
 $add_tz_text!add:FBTN $delete_tz_text!remove:FBTN ADD ସମୟ ମଣ୍ଡଳ | FT10- ଘଣ୍ଟା | ୱାର୍ଲ୍ଡ ଘଣ୍ଟାରେ ଯୋଡିବାକୁ ଟାଇମ୍ ଜୋନ୍ ସନ୍ନିବେଶ କରନ୍ତୁ | \n (ସମସ୍ତ ଉପଲବ୍ଧ ସମୟ ମଣ୍ଡଳ ମାଧ୍ୟମରେ ସନ୍ଧାନ କରିବାକୁ କିଛି ପ୍ରବେଶ କରନ୍ତୁ ନାହିଁ |) ସମୟ ମଣ୍ଡଳ ଅପସାରଣ କରନ୍ତୁ | 