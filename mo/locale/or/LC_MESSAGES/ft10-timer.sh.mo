��          �   %   �      P     Q  "   f     �  1   �     �     �     �  	   �  !        %     =     B  '   H  !   p  #   �  #   �     �     �     �                1     R     g  
   n  w  y  :   �  �   ,  i   �  �        �     �  7   �       L     @   g  	   �  !   �  w   �  t   L	  z   �	  k   <
  @   �
  3   �
       -   8  p   f  v   �  +   N     z  "   �        	                                                                             
                                        Alarm sound filename Ask to begin each set (all timers) Ask to begin each timer Auto close progress bar display when all sets end Configuration Duration Each timer end Finished. Interface to Sysmonitor Indicator Multiple Timer settings Name Never Number of times to run set (all timers) Pop-up message when each set ends Pop-up message when each timer ends Progress Bar update every x seconds Ready to start SUSPEND COMPUTER Seconds Seconds!Minutes Sound alarm when each set ends Sound alarm when each timer ends Timer duration units Timers has ended. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Odia (https://www.transifex.com/antix-linux-community-contributions/teams/120110/or/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: or
Plural-Forms: nplurals=2; plural=(n != 1);
 ଆଲାର୍ମ ଧ୍ୱନି ଫାଇଲନାମ | ପ୍ରତ୍ୟେକ ସେଟ୍ ଆରମ୍ଭ କରିବାକୁ କୁହନ୍ତୁ (ସମସ୍ତ ଟାଇମର୍) ପ୍ରତ୍ୟେକ ଟାଇମର୍ ଆରମ୍ଭ କରିବାକୁ କୁହନ୍ତୁ | ସମସ୍ତ ସେଟ୍ ସମାପ୍ତ ହେଲେ ଅଟୋ ବନ୍ଦ ପ୍ରଗତି ବାର୍ ପ୍ରଦର୍ଶନ | ବିନ୍ୟାସ ଅବଧି ପ୍ରତ୍ୟେକ ଟାଇମର୍ ଶେଷ | ସମାପ୍ତ ସିସମୋନିଟର ସୂଚକକୁ ଇଣ୍ଟରଫେସ୍ | ଏକାଧିକ ଟାଇମର୍ ସେଟିଙ୍ଗ୍ | ନାମ କଦାପି ନୁହେଁ | ସେଟ୍ ଚଲାଇବା ପାଇଁ ସଂଖ୍ୟା ସଂଖ୍ୟା (ସମସ୍ତ ଟାଇମର୍) ପ୍ରତ୍ୟେକ ସେଟ୍ ସମାପ୍ତ ହେଲେ ପପ୍-ଅପ୍ ବାର୍ତ୍ତା | ପ୍ରତ୍ୟେକ ଟାଇମର୍ ସମାପ୍ତ ହେଲେ ପପ୍-ଅପ୍ ବାର୍ତ୍ତା | ପ୍ରତ୍ୟେକ x ସେକେଣ୍ଡରେ ପ୍ରଗତି ବାର୍ ଅପଡେଟ୍ | ଆରମ୍ଭ କରିବାକୁ ପ୍ରସ୍ତୁତ | ସସପେଣ୍ଡ କମ୍ପ୍ୟୁଟର | ସେକେଣ୍ଡ୍ | ସେକେଣ୍ଡ୍ |!ମିନିଟ୍ ପ୍ରତ୍ୟେକ ସେଟ୍ ସମାପ୍ତ ହେଲେ ସାଉଣ୍ଡ୍ ଆଲାର୍ମ | ପ୍ରତ୍ୟେକ ଟାଇମର୍ ସମାପ୍ତ ହେଲେ ସାଉଣ୍ଡ୍ ଆଲାର୍ମ | ଟାଇମର୍ ଅବଧି ଏକକ | ଟାଇମର୍ସ ସମାପ୍ତ ହୋଇଛି 