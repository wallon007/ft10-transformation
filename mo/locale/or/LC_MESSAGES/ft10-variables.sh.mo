��    (      \  5   �      p     q     }     �     �  
   �     �     �     �     �     �  ,   �     �                      F        f  
   k     v     �     �     �     �     �     �     �     �     �     �     �                    &     ,     3     ;     D  w  P     �     �     �               6  3   H     |     �     �  �   �  -   J     x     �     �     �    �     �	  !   
  E   (
     n
     �
     �
     �
  m   �
     (  3   D  +   x     �  $   �     �  	   �  '     Y   /     �     �     �     �  %   �                       
          	   $                                                     &   (       !          %          "                             #                    '        Accessories All Audio Browser Calculator Clock Control Centre Development E-mail Exit Favorite Apps: (click here to edit the list) File Manager Files Games Graphics Internet Left click- Add,move,remove Toolbar icons; Right click- Manage Toolbar Menu Multimedia Network manager - Connman News Office Pics Recent Search for programs and files Settings Show Desktop Stop App System Task switcher Terminal Text Text Editor Unplug USB drives Video Volume Weather Weather  Web Browser Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Odia (https://www.transifex.com/antix-linux-community-contributions/teams/120110/or/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: or
Plural-Forms: nplurals=2; plural=(n != 1);
 ଆନୁଷଙ୍ଗିକ ସମସ୍ତ ଅଡିଓ ବ୍ରାଉଜର୍ କାଲକୁଲେଟର ଘଣ୍ଟା | ନିୟନ୍ତ୍ରଣ କେନ୍ଦ୍ର | ବିକାଶ ଇ-ମେଲ୍ | ପ୍ରସ୍ଥାନ ପ୍ରିୟ ଆପ୍ସ: (ତାଲିକା ସଂପାଦନ କରିବାକୁ ଏଠାରେ କ୍ଲିକ୍ କରନ୍ତୁ) ଫାଇଲ୍ ମ୍ୟାନେଜର୍ | ଫାଇଲଗୁଡିକ ଖେଳଗୁଡିକ ଗ୍ରାଫିକ୍ସ ଇଣ୍ଟରନେଟ୍ | ବାମ କ୍ଲିକ୍- ଟୁଲ୍ ବାର୍ ଆଇକନ୍ ଯୋଡନ୍ତୁ, ଘୁଞ୍ଚାନ୍ତୁ, ଅପସାରଣ କରନ୍ତୁ; ଡାହାଣ କ୍ଲିକ୍- ଟୁଲ୍ ବାର୍ ପରିଚାଳନା କରନ୍ତୁ | ମେନୁ ମଲ୍ଟିମିଡ଼ିଆ ନେଟୱର୍କ ପରିଚାଳକ - କନମ୍ୟାନ | ସମ୍ବାଦ ଅଫିସ୍ ଚିତ୍ର ସମ୍ପ୍ରତି ପ୍ରୋଗ୍ରାମ୍ ଏବଂ ଫାଇଲ୍ ପାଇଁ ସନ୍ଧାନ କରନ୍ତୁ | ସେଟିଂସମୂହ ଡେସ୍କଟପ୍ ଦେଖାନ୍ତୁ | ଆପ୍ ବନ୍ଦ କରନ୍ତୁ | ସିଷ୍ଟମ୍ | ଟାସ୍କ ସୁଇଚର୍ | ଟର୍ମିନାଲ୍ ପାଠ ପାଠ୍ୟ ସମ୍ପାଦକ | USB ଡ୍ରାଇଭଗୁଡ଼ିକୁ ଅନ୍ପ୍ଲଗ୍ କରନ୍ତୁ | ଭିଡିଓ ଭଲ୍ୟୁମ୍ ପାଣିପାଗ ପାଣିପାଗ ୱେବ୍ ବ୍ରାଉଜର୍ 