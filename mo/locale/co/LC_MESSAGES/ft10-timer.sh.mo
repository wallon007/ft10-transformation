��          �   %   �      P     Q  "   f     �  1   �     �     �     �  	   �  !        %     =     B  '   H  !   p  #   �  #   �     �     �     �                1     R     g  
   n  {  y     �  2        E  Q   e     �     �     �     �  &   �          *     /  .   3  (   b  *   �  4   �     �                  #   )  %   M     s     �     �        	                                                                             
                                        Alarm sound filename Ask to begin each set (all timers) Ask to begin each timer Auto close progress bar display when all sets end Configuration Duration Each timer end Finished. Interface to Sysmonitor Indicator Multiple Timer settings Name Never Number of times to run set (all timers) Pop-up message when each set ends Pop-up message when each timer ends Progress Bar update every x seconds Ready to start SUSPEND COMPUTER Seconds Seconds!Minutes Sound alarm when each set ends Sound alarm when each timer ends Timer duration units Timers has ended. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Corsican (https://www.transifex.com/antix-linux-community-contributions/teams/120110/co/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: co
Plural-Forms: nplurals=2; plural=(n != 1);
 U nome di u sonu di l'alarma Dumandate à inizià ogni set (tutti i cronometri) Dumandate à inizià ogni timer Visualizzazione automatica di a barra di prugressu quandu tutti i setti finiscinu Cunfigurazione Durata Ogni timer finisce Finitu. Interfaccia à l'indicatore Sysmonitor Multiple settings di Timer Nome Mai Numeru di volte per eseguisce (tutti i timers) Messaghju pop-up quandu ogni set finisci Missaghju pop-up quandu ogni timer finisci Aggiornamentu di a barra di prugressu ogni x seconde Pronta à principià SUSPENDU COMPUTER Sicondi Sicondi!Minuti Alarma sonu quandu ogni set finisci Alarma sonu quandu ogni timer finisce Unità di durata di u timer Timers hè finitu. 