��    (      \  5   �      p     q     }     �     �  
   �     �     �     �     �     �  ,   �     �                      F        f  
   k     v     �     �     �     �     �     �     �     �     �     �     �                    &     ,     3     ;     D  {  P     �     �     �     �     �     �     �                 4         U     a     h     p     y  v   �     �               $     ,     3     :     C     ]     d     s     �     �     �     �     �     �     �     �     �     �     �                       
          	   $                                                     &   (       !          %          "                             #                    '        Accessories All Audio Browser Calculator Clock Control Centre Development E-mail Exit Favorite Apps: (click here to edit the list) File Manager Files Games Graphics Internet Left click- Add,move,remove Toolbar icons; Right click- Manage Toolbar Menu Multimedia Network manager - Connman News Office Pics Recent Search for programs and files Settings Show Desktop Stop App System Task switcher Terminal Text Text Editor Unplug USB drives Video Volume Weather Weather  Web Browser Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Estonian (https://www.transifex.com/antix-linux-community-contributions/teams/120110/et/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: et
Plural-Forms: nplurals=2; plural=(n != 1);
 Aksessuaarid Kõik Heli Brauser Kalkulaator Kell Juhtimiskeskus Areng E-post Välju Lemmikrakendused: (loendi muutmiseks klõpsake siin) Failihaldur Failid Mängud Graafika Internet Vasak klõps – tööriistariba ikoonide lisamine, teisaldamine, eemaldamine; Paremklõps - Tööriistariba haldamine Menüü Multimeedia Võrguhaldur - Connman Uudised kontor Pildid Viimased Otsige programme ja faile Seaded Kuva töölaud Peata rakendus Süsteem Ülesande vahetaja Terminal Tekst Tekstiredaktor Ühendage USB-draivid lahti Video Helitugevus Ilm Ilm Veebibrauseris 