��    (      \  5   �      p     q     }     �     �  
   �     �     �     �     �     �  ,   �     �                      F        f  
   k     v     �     �     �     �     �     �     �     �     �     �     �                    &     ,     3     ;     D  |  P     �     �     �     �     �     �                    %  7   -     e     v  
   }     �     �  Q   �  	   �  
   �                !     )     0     8     U     a     o     x     �  	   �     �     �     �     �     �     �     �  
   �                       
          	   $                                                     &   (       !          %          "                             #                    '        Accessories All Audio Browser Calculator Clock Control Centre Development E-mail Exit Favorite Apps: (click here to edit the list) File Manager Files Games Graphics Internet Left click- Add,move,remove Toolbar icons; Right click- Manage Toolbar Menu Multimedia Network manager - Connman News Office Pics Recent Search for programs and files Settings Show Desktop Stop App System Task switcher Terminal Text Text Editor Unplug USB drives Video Volume Weather Weather  Web Browser Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Afrikaans (https://www.transifex.com/antix-linux-community-contributions/teams/120110/af/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: af
Plural-Forms: nplurals=2; plural=(n != 1);
 Bykomstighede Almal Oudio Blaaier Sakrekenaar Horlosie Beheersentrum Ontwikkeling E-pos Verlaat Gunsteling toepassings: (klik hier om die lys te wysig) Lêer bestuurder Lêers Speletjies Grafika Internet Linkerkliek- Voeg by, skuif, verwyder Toolbar-ikone; Regskliek - Bestuur nutsbalk Spyskaart Multimedia Netwerkbestuurder - Connman Nuus Kantoor Foto's Onlangs Soek vir programme en lêers Instellings Wys lessenaar Stop App Stelsel Taakwisselaar Terminale Teks Teksredakteur Ontkoppel USB-stasies Video Volume Weer Weer Webblaaier 