��          �   %   �      P     Q  "   f     �  1   �     �     �     �  	   �  !        %     =     B  '   H  !   p  #   �  #   �     �     �     �                1     R     g  
   n  �  y  3   �  m   2  Q   �  �   �     �     �  -   �     �  Q   �  ;   9  	   u       f   �  n   �  Y   l	  o   �	  4   6
  <   k
     �
     �
  S   �
  O   )  /   y     �  "   �        	                                                                             
                                        Alarm sound filename Ask to begin each set (all timers) Ask to begin each timer Auto close progress bar display when all sets end Configuration Duration Each timer end Finished. Interface to Sysmonitor Indicator Multiple Timer settings Name Never Number of times to run set (all timers) Pop-up message when each set ends Pop-up message when each timer ends Progress Bar update every x seconds Ready to start SUSPEND COMPUTER Seconds Seconds!Minutes Sound alarm when each set ends Sound alarm when each timer ends Timer duration units Timers has ended. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Panjabi (Punjabi) (https://www.transifex.com/antix-linux-community-contributions/teams/120110/pa/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: pa
Plural-Forms: nplurals=2; plural=(n != 1);
 ਅਲਾਰਮ ਧੁਨੀ ਫਾਈਲ ਨਾਮ ਹਰੇਕ ਸੈੱਟ ਨੂੰ ਸ਼ੁਰੂ ਕਰਨ ਲਈ ਕਹੋ (ਸਾਰੇ ਟਾਈਮਰ) ਹਰੇਕ ਟਾਈਮਰ ਨੂੰ ਸ਼ੁਰੂ ਕਰਨ ਲਈ ਕਹੋ ਸਾਰੇ ਸੈੱਟ ਖਤਮ ਹੋਣ 'ਤੇ ਪ੍ਰਗਤੀ ਪੱਟੀ ਡਿਸਪਲੇਅ ਨੂੰ ਆਟੋ ਬੰਦ ਕਰੋ ਸੰਰਚਨਾ ਮਿਆਦ ਹਰੇਕ ਟਾਈਮਰ ਦਾ ਅੰਤ ਸਮਾਪਤ। ਸਿਸਮੋਨੀਟਰ ਇੰਡੀਕੇਟਰ ਲਈ ਇੰਟਰਫੇਸ ਮਲਟੀਪਲ ਟਾਈਮਰ ਸੈਟਿੰਗਜ਼ ਨਾਮ ਕਦੇ ਨਹੀਂ ਸੈੱਟ ਚਲਾਉਣ ਲਈ ਸਮੇਂ ਦੀ ਗਿਣਤੀ (ਸਾਰੇ ਟਾਈਮਰ) ਪੌਪ-ਅੱਪ ਸੁਨੇਹਾ ਜਦੋਂ ਹਰੇਕ ਸੈੱਟ ਖਤਮ ਹੁੰਦਾ ਹੈ ਹਰ ਟਾਈਮਰ ਖਤਮ ਹੋਣ 'ਤੇ ਪੌਪ-ਅੱਪ ਸੁਨੇਹਾ ਪ੍ਰੋਗਰੈਸ ਬਾਰ ਹਰ x ਸਕਿੰਟ ਵਿੱਚ ਅੱਪਡੇਟ ਕਰਦਾ ਹੈ ਸ਼ੁਰੂ ਕਰਨ ਲਈ ਤਿਆਰ ਹੈ ਕੰਪਿਊਟਰ ਨੂੰ ਸਸਪੈਂਡ ਕਰੋ ਸਕਿੰਟ ਸਕਿੰਟ!ਮਿੰਟ ਹਰ ਸੈੱਟ ਦੇ ਖਤਮ ਹੋਣ 'ਤੇ ਅਲਾਰਮ ਵੱਜੋ ਹਰ ਟਾਈਮਰ ਖਤਮ ਹੋਣ 'ਤੇ ਅਲਾਰਮ ਵੱਜੋ ਟਾਈਮਰ ਮਿਆਦ ਇਕਾਈਆਂ ਟਾਈਮਰ ਖਤਮ ਹੋ ਗਿਆ ਹੈ. 