��          �      l      �  (   �  '   
  7   2  '   j     �  G   �     �     �                    ,     5     >     G     S     Z  $   z     �     �  �  �  (   5  '   ^  �   �  '        E  G   X     �     �  3   �  B   �  #   6     Z     g     x     �     �  V   �  k        r     �     	                                                                                         
        \t<b>$alarm_set_to_text $hr:$mn</b>      \t<b>$alarm_text $hr:$mn...</b> \n    Are you sure to set the alarm for that time tomorrow? $alarm_set_to_text $hr:$mn
$cancel_text $are_you_sure_text $cancel_alarm_text !bash -c 'cancel_quit_tray $XPID $target'!gtk-cancel $confirm_title Alarm Alarm set to Are you sure??? Cancel Alarm Duration Hour::CB Interval Minutes::CB Repeat To cancel, right-click for menu To cancel, right-click icon for menu infinite seconds Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Panjabi (Punjabi) (https://www.transifex.com/antix-linux-community-contributions/teams/120110/pa/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: pa
Plural-Forms: nplurals=2; plural=(n != 1);
     \t<b>$alarm_set_to_text $hr:$mn</b>      \t<b>$alarm_text $hr:$mn...</b> \n    ਕੀ ਤੁਸੀਂ ਭਲਕੇ ਉਸ ਸਮੇਂ ਲਈ ਅਲਾਰਮ ਸੈੱਟ ਕਰਨਾ ਯਕੀਨੀ ਬਣਾ ਰਹੇ ਹੋ? $alarm_set_to_text $hr:$mn
$cancel_text $are_you_sure_text $cancel_alarm_text !bash -c 'cancel_quit_tray $XPID $target'!gtk-cancel $confirm_title ਅਲਾਰਮ ਅਲਾਰਮ ਸੈੱਟ ਕੀਤਾ ਗਿਆ ਤੁਹਾਨੂੰ ਪੂਰਾ ਵਿਸ਼ਵਾਸ ਹੈ??? ਅਲਾਰਮ ਰੱਦ ਕਰੋ ਮਿਆਦ ਘੰਟਾ::CB ਅੰਤਰਾਲ ਮਿੰਟ::CB ਦੁਹਰਾਓ ਰੱਦ ਕਰਨ ਲਈ, ਮੀਨੂ ਲਈ ਸੱਜਾ-ਕਲਿੱਕ ਕਰੋ ਰੱਦ ਕਰਨ ਲਈ, ਮੀਨੂ ਲਈ ਆਈਕਨ 'ਤੇ ਸੱਜਾ-ਕਲਿੱਕ ਕਰੋ ਬੇਅੰਤ ਸਕਿੰਟ 