��          �   %   �      P     Q  "   f     �  1   �     �     �     �  	   �  !        %     =     B  '   H  !   p  #   �  #   �     �     �     �                1     R     g  
   n  z  y     �  7     %   H  F   n     �     �     �     �  $   �          .     2  ,   >  "   k  #   �  /   �     �  
   �     �       $     %   ;  &   a  
   �     �        	                                                                             
                                        Alarm sound filename Ask to begin each set (all timers) Ask to begin each timer Auto close progress bar display when all sets end Configuration Duration Each timer end Finished. Interface to Sysmonitor Indicator Multiple Timer settings Name Never Number of times to run set (all timers) Pop-up message when each set ends Pop-up message when each timer ends Progress Bar update every x seconds Ready to start SUSPEND COMPUTER Seconds Seconds!Minutes Sound alarm when each set ends Sound alarm when each timer ends Timer duration units Timers has ended. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Turkmen (https://www.transifex.com/antix-linux-community-contributions/teams/120110/tk/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: tk
Plural-Forms: nplurals=2; plural=(n != 1);
 Duýduryş ses faýlyň ady Her toplumy başlamagy haýyş ediň (ähli taýmerler) Her taýmeri başlamagy haýyş ediň Setshli toplumlar gutaranda öňe gidiş çyzgysyny awtomatiki ýapyň Sazlama Dowamlylygy Her taýmer gutarýar Tamamlandy. Sysmonitor görkezijisine interfeýs Birnäçe taýmer sazlamalary Ady Hiç haçan Toplumy işletmegiň sany (ähli taýmerler) Her toplum gutaranda açylan habar Her taýmer gutaranda açylan habar “Progress Bar” her x sekuntda täzelenýär Başlamaga taýyn KOMPUTUTER Sekunt Sekunt!Minutlar Her toplum gutaranda ses duýduryşy Her taýmer gutaranda ses duýduryşy Hasaplaýjynyň dowamlylygy birlikleri Taýmerler gutardy. 