��          �   %   �      P     Q  "   f     �  1   �     �     �     �  	   �  !        %     =     B  '   H  !   p  #   �  #   �     �     �     �                1     R     g  
   n  �  y  3   F  h   z  )   �  �        �  	   �  3   �  &   �  *      K   K  	   �     �  k   �  X   	  V   v	  R   �	      
  +   <
     h
  %   x
  R   �
  L   �
  ,   >     k     �        	                                                                             
                                        Alarm sound filename Ask to begin each set (all timers) Ask to begin each timer Auto close progress bar display when all sets end Configuration Duration Each timer end Finished. Interface to Sysmonitor Indicator Multiple Timer settings Name Never Number of times to run set (all timers) Pop-up message when each set ends Pop-up message when each timer ends Progress Bar update every x seconds Ready to start SUSPEND COMPUTER Seconds Seconds!Minutes Sound alarm when each set ends Sound alarm when each timer ends Timer duration units Timers has ended. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Arabic (https://www.transifex.com/antix-linux-community-contributions/teams/120110/ar/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: ar
Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 && n%100<=10 ? 3 : n%100>=11 && n%100<=99 ? 4 : 5;
 ﻪﻴﺒﻨﺘﻟﺍ ﺕﻮﺻ ﻒﻠﻣ ﻢﺳﺍ (ﺖﻗﻮﻟﺍ ﻂﺒﺿ ﺓﺰﻬﺟﺃ ﻊﻴﻤﺟ) ﺔﻋﻮﻤﺠﻣ ﻞﻛ ءﺪﺑ ﺐﻠﻃﺍ ﺩﺍﺪﻋ ﻞﻛ ءﺪﺑ ﺐﻠﻃﺍ ﺕﺎﻋﻮﻤﺠﻤﻟﺍ ﻊﻴﻤﺟ ءﺎﻬﺘﻧﺍ ﺪﻨﻋ ﻲﺋﺎﻘﻠﺘﻟﺍ ﻕﻼ﻿ﻏﻹ﻿ﺍ ﻡﺪﻘﺘﻟﺍ ﻂﻳﺮﺷ ﺽﺮﻋ ﺐﻴﺗﺮﺗ ﺓﺪﻣ ﺖﻗﻮﻟﺍ ﺩﺍﺪﻋ ﺔﻳﺎﻬﻧ ﻞﻛ .ﻦﻣ ءﺎﻬﺘﻧﻻ﻿ﺍ ﻢﺗ Sysmonitor ﺮﺷﺆﻤﻟ ﺔﻬﺟﺍﻭ ﺓﺩﺪﻌﺘﻤﻟﺍ ﺖﻗﻮﻟﺍ ﺩﺍﺪﻋ ﺕﺍﺩﺍﺪﻋﺇ ﻢﺳﺍ ﺎﻘﻠﻄﻣ (ﺕﺎﺘﻗﺆﻤﻟﺍ ﻊﻴﻤﺟ) ﻞﻴﻐﺸﺘﻟﺍ ﺕﺍﺮﻣ ﺩﺪﻋ ﻦﻴﻴﻌﺗ ﻢﺗ ﺔﻋﻮﻤﺠﻣ ﻞﻛ ءﺎﻬﺘﻧﺍ ﺪﻨﻋ ﺔﻘﺜﺒﻨﻣ ﺔﻟﺎﺳﺭ ﺩﺍﺪﻋ ﻞﻛ ﻲﻬﺘﻨﻳ ﺎﻣﺪﻨﻋ ﺔﻘﺜﺒﻨﻣ ﺔﻟﺎﺳﺭ ﺔﻴﻧﺎﺛ x ﻞﻛ ﻡﺪﻘﺘﻟﺍ ﻂﻳﺮﺷ ﺚﻳﺪﺤﺗ ﻢﺘﻳ ءﺪﺒﻠﻟ ﺰﻫﺎﺟ ﺮﺗﻮﻴﺒﻤﻜﻟﺍ ﻖﻴﻠﻌﺗ ﻲﻧﺍﻮﺛ ﻲﻧﺍﻮﺛ!ﻖﺋﺎﻗﺪﻟﺍ ﺔﻋﻮﻤﺠﻣ ﻞﻛ ءﺎﻬﺘﻧﺍ ﺪﻨﻋ ﻲﺗﻮﺻ ﺭﺍﺬﻧﺇ ﺖﻗﺆﻣ ﻞﻛ ءﺎﻬﺘﻧﺍ ﺪﻨﻋ ﻲﺗﻮﺻ ﺭﺍﺬﻧﺇ ﺖﻗﻮﻤﻟﺍ ﺓﺪﻣ ﺕﺍﺪﺣﻭ ﺕﺎﺘﻗﻮﻤﻟﺍ .ﻰﻬﺘﻧﺍ 