��          �   %   �      P     Q  "   f     �  1   �     �     �     �  	   �  !        %     =     B  '   H  !   p  #   �  #   �     �     �     �                1     R     g  
   n  y  y     �  4   	  )   >  G   h     �     �     �     �      �          #     )  ;   .  #   j  1   �  2   �     �               $     2  )   R     |     �  
   �        	                                                                             
                                        Alarm sound filename Ask to begin each set (all timers) Ask to begin each timer Auto close progress bar display when all sets end Configuration Duration Each timer end Finished. Interface to Sysmonitor Indicator Multiple Timer settings Name Never Number of times to run set (all timers) Pop-up message when each set ends Pop-up message when each timer ends Progress Bar update every x seconds Ready to start SUSPEND COMPUTER Seconds Seconds!Minutes Sound alarm when each set ends Sound alarm when each timer ends Timer duration units Timers has ended. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Turkish (https://www.transifex.com/antix-linux-community-contributions/teams/120110/tr/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: tr
Plural-Forms: nplurals=2; plural=(n > 1);
 Alarm sesi dosya adı Her sete başlamayı isteyin (tüm zamanlayıcılar) Her zamanlayıcıyı başlatmayı isteyin Tüm setler sona erdiğinde ilerleme çubuğu ekranını otomatik kapat Yapılandırma Süre Her zamanlayıcı sonu Bitti. Sysmonitor Göstergesi Arayüzü Çoklu Zamanlayıcı ayarları İsim Asla Ayarlanacak çalıştırma sayısı (tüm zamanlayıcılar) Her set bittiğinde açılır mesaj Her zamanlayıcı sona erdiğinde açılır mesaj İlerleme Çubuğu her x saniyede bir güncellenir Başlamaya hazır BİLGİSAYARI ASKIYA AL saniye saniye!dakika Her set bittiğinde sesli alarm Her zamanlayıcı bittiğinde sesli alarm Zamanlayıcı süre birimleri zamanlayıcılar sona erdi. 