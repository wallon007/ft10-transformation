��          �   %   �      P     Q  "   f     �  1   �     �     �     �  	   �  !        %     =     B  '   H  !   p  #   �  #   �     �     �     �                1     R     g  
   n  w  y  =   �  m   /  F   �  �   �     s     �  0   �  	   �  H   �  <   )  	   f     p  p   �  O   �  U   K	  t   �	  C   
  8   Z
     �
     �
  S   �
  Y     B   q     �  "   �        	                                                                             
                                        Alarm sound filename Ask to begin each set (all timers) Ask to begin each timer Auto close progress bar display when all sets end Configuration Duration Each timer end Finished. Interface to Sysmonitor Indicator Multiple Timer settings Name Never Number of times to run set (all timers) Pop-up message when each set ends Pop-up message when each timer ends Progress Bar update every x seconds Ready to start SUSPEND COMPUTER Seconds Seconds!Minutes Sound alarm when each set ends Sound alarm when each timer ends Timer duration units Timers has ended. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Urdu (https://www.transifex.com/antix-linux-community-contributions/teams/120110/ur/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: ur
Plural-Forms: nplurals=2; plural=(n != 1);
 ﻡﺎﻧ ﺎﮐ ﻞﺋﺎﻓ ﮉﻧﺅﺎﺳ ﻡﺭﻻ﻿ﺍ (ﺮﻤﺋﺎﭨ ﻡﺎﻤﺗ) ﮟﯿﮭﭼﻮﭘ ﮯﯿﻟ ﮯﮐ ﮯﻧﺮﮐ ﻉﻭﺮﺷ ﭧﯿﺳ ﺮﮨ ۔ﮟﯿﮩﮐ ﻮﮐ ﮯﻧﺮﮐ ﻉﻭﺮﺷ ﺮﻤﺋﺎﭨ ﺮﮨ ۔ﮟﯾﺮﮐ ﺪﻨﺑ ﻮﭨﺁ ﻮﮐ ﮯﻠﭙﺳﮈ ﺭﺎﺑ ﺲﯾﺮﮔﻭﺮﭘ ﺮﭘ ﮯﻧﻮﮨ ﻢﺘﺧ ﭧﯿﺳ ﻡﺎﻤﺗ ﻦﺸﯾﺮﮕﯿﻔﻨﮐ ﮧﯿﻧﺍﺭﻭﺩ ﻡﺎﺘﺘﺧﺍ ﺎﮐ ﺮﻤﺋﺎﭨ ﺮﮨ ﻢﺘﺧ ﺲﯿﻓﺮﭩﻧﺍ ﺎﮐ ﮮﺭﺎﺷﺍ ﺮﭩﯿﻧﺎﻤﺴﯿﺳ ﺕﺎﺒﯿﺗﺮﺗ ﯽﮐ ﺮﻤﺋﺎﭨ ﺩﺪﻌﺘﻣ ﻡﺎﻧ ﮟﯿﮩﻧ ﯽﮭﺒﮐ (ﺮﻤﺋﺎﭨ ﻡﺎﻤﺗ) ﺩﺍﺪﻌﺗ ﯽﮐ ﺕﺎﻗﻭﺍ ﮯﯿﻟ ﮯﮐ ﮯﻧﻼ﻿ﭼ ﭧﯿﺳ ﻡﺎﻐﯿﭘ ﭖﺍ ﭖﺎﭘ ﺮﭘ ﮯﻧﻮﮨ ﻢﺘﺧ ﭧﯿﺳ ﺮﮨ ﻡﺎﻐﯿﭘ ﭖﺍ ﭖﺎﭘ ﺮﭘ ﮯﻧﻮﮨ ﻢﺘﺧ ﺮﻤﺋﺎﭨ ﺮﮨ ۔ﮯﮨ ﺎﺗﺮﮐ ﭧﯾﮈ ﭖﺍ ﮟﯿﻣ ﮉﻨﮑﯿﺳ ﺲﮑﯾﺍ ﺮﮨ ﺭﺎﺑ ﺲﯾﺮﮔﻭﺮﭘ ۔ﮟﯿﮨ ﺭﺎﯿﺗ ﮯﯿﻟ ﮯﮐ ﮯﻧﺮﮐ ﻉﻭﺮﺷ ۔ﮟﯾﺮﮐ ﻞﻄﻌﻣ ﻮﮐ ﺮﭨﻮﯿﭙﻤﮐ ﺯﮉﻨﮑﯿﺳ ﺯﮉﻨﮑﯿﺳ!ﭧﻨﻣ ۔ﮟﯿﺋﺎﮕﻟ ﻡﺭﻻ﻿ﺍ ﺮﭘ ﮯﻧﻮﮨ ﻢﺘﺧ ﭧﯿﺳ ﺮﮨ ۔ﮟﯿﺋﺎﮕﻟ ﻡﺭﻻ﻿ﺍ ﺮﭘ ﮯﻧﻮﮨ ﻢﺘﺧ ﺮﻤﺋﺎﭨ ﺮﮨ ﮞﺎﯿﺋﺎﮐﺍ ﯽﮐ ﮧﯿﻧﺍﺭﻭﺩ ﺮﻤﺋﺎﭨ ﺯﺮﻤﺋﺎﭨ .ﮯﮨ ﺎﯿﮔ ﻮﮨ ﻢﺘﺧ 