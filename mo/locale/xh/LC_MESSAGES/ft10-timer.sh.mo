��          �   %   �      P     Q  "   f     �  1   �     �     �     �  	   �  !        %     =     B  '   H  !   p  #   �  #   �     �     �     �                1     R     g  
   n  x  y     �  2     %   D  H   j  
   �     �     �     �  !   �  !        <     B  5   I  +     6   �  -   �          %  
   6     A  &   T  1   {     �     �  	   �        	                                                                             
                                        Alarm sound filename Ask to begin each set (all timers) Ask to begin each timer Auto close progress bar display when all sets end Configuration Duration Each timer end Finished. Interface to Sysmonitor Indicator Multiple Timer settings Name Never Number of times to run set (all timers) Pop-up message when each set ends Pop-up message when each timer ends Progress Bar update every x seconds Ready to start SUSPEND COMPUTER Seconds Seconds!Minutes Sound alarm when each set ends Sound alarm when each timer ends Timer duration units Timers has ended. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Xhosa (https://www.transifex.com/antix-linux-community-contributions/teams/120110/xh/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: xh
Plural-Forms: nplurals=2; plural=(n != 1);
 Igama lefayile yesandi se-alam Cela ukuqalisa isethi nganye (zonke izibali-xesha) Cela ukuqalisa isibali-xesha ngasinye Vala ngokuzenzekelayo isibonisi sebha yenkqubela xa zonke iiseti ziphela Uqwalaselo Ubude bexesha Isibali-xesha ngasinye siphela Igqitywe tu. Ujongano kwiSibonisi seSysmonitor Iisetingi zesibali-xesha ezininzi Igama Ungaze Inani lamaxesha okuqhuba isethi (zonke izibali-xesha) Umyalezo ozivelelayo xa iseti nganye iphela Umyalezo ozivelelayo xa isibali-xesha ngasinye siphela Uhlaziyo lweBha yenkqubela rhoqo x imizuzwana Ukulungele ukuqalisa ZUZA IKHOMPYUTHA Imizuzwana Imizuzwana!Imizuzu Isivusi sesandi xa iseti nganye iphela Isivusi sesandi xa isibali-xesha ngasinye siphela Iiyunithi zobude bexesha Izibali-xesha iphelile. 