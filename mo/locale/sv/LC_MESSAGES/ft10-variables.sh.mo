��    (      \  5   �      p     q     }     �     �  
   �     �     �     �     �     �  ,   �     �                      F        f  
   k     v     �     �     �     �     �     �     �     �     �     �     �                    &     ,     3     ;     D  z  P  
   �     �     �     �  
   �     �     �  
                4   (     ]     j     p     u     |  e   �     �  
   �     �               &     -     5     R     a  
   p     {     �     �     �     �     �     �     �     �     �     �                       
          	   $                                                     &   (       !          %          "                             #                    '        Accessories All Audio Browser Calculator Clock Control Centre Development E-mail Exit Favorite Apps: (click here to edit the list) File Manager Files Games Graphics Internet Left click- Add,move,remove Toolbar icons; Right click- Manage Toolbar Menu Multimedia Network manager - Connman News Office Pics Recent Search for programs and files Settings Show Desktop Stop App System Task switcher Terminal Text Text Editor Unplug USB drives Video Volume Weather Weather  Web Browser Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Swedish (https://www.transifex.com/antix-linux-community-contributions/teams/120110/sv/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: sv
Plural-Forms: nplurals=2; plural=(n != 1);
 Tillbehör Allt Audio Webbläsare Kalkylator Klocka Kontrollcenter Utveckling E-post Utgång Favoritappar: (klicka här för att redigera listan) Filhanterare Filer Spel Grafik Internet Vänsterklick- Lägg till, flytta, ta bort verktygsfältsikoner; Högerklicka - Hantera verktygsfält Meny Multimedia Nätverksansvarig - Connman Nyheter Kontor Bilder Nyligen Sök efter program och filer inställningar Visa skrivbord Stoppa app Systemet Uppgiftsväxlare Terminal Text Textredigerare Koppla ur USB-enheter Video Volym Väder Väder Webbläsare 