��    
      l      �       �      �   (     .   ,  0   [     �     �  )   �     �     �  z       �  3   �  <   �  =        J      X  ,   y     �     �                         	                
    ADD ICON!add:FBTN Choose application to add to the Toolbar Double click any Application to move its icon: Double click any Application to remove its icon: Icon located! MOVE ICON!gtk-go-back-rtl:FBTN No icon located, using default Gears icon REMOVE ICON!remove:FBTN Tint2 icons Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:39+0000
Last-Translator: Robin, 2022
Language-Team: Swedish (https://www.transifex.com/antix-linux-community-contributions/teams/120110/sv/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: sv
Plural-Forms: nplurals=2; plural=(n != 1);
 LÄGG TILL IKON!add:FBTN Välj applikation att lägga till i verktygsfältet Dubbelklicka på valfritt program för att flytta dess ikon: Dubbelklicka på valfritt program för att ta bort dess ikon: Ikonen finns! FLYTTA IKON!gtk-go-back-rtl:FBTN Ingen ikon finns med standardkugghjulsikonen BORT ICON!remove:FBTN Tint2 ikoner 