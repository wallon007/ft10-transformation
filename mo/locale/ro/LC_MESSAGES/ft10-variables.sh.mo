��    (      \  5   �      p     q     }     �     �  
   �     �     �     �     �     �  ,   �     �                      F        f  
   k     v     �     �     �     �     �     �     �     �     �     �     �                    &     ,     3     ;     D  �  P  	   �     �            
             #  
   5     @     G  7   O     �     �     �     �     �  �   �     H  
   O     Z     v     }     �     �     �     �     �     �     �     �     �     �     	     	     /	     5	     ;	     A	     G	                       
          	   $                                                     &   (       !          %          "                             #                    '        Accessories All Audio Browser Calculator Clock Control Centre Development E-mail Exit Favorite Apps: (click here to edit the list) File Manager Files Games Graphics Internet Left click- Add,move,remove Toolbar icons; Right click- Manage Toolbar Menu Multimedia Network manager - Connman News Office Pics Recent Search for programs and files Settings Show Desktop Stop App System Task switcher Terminal Text Text Editor Unplug USB drives Video Volume Weather Weather  Web Browser Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Romanian (https://www.transifex.com/antix-linux-community-contributions/teams/120110/ro/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: ro
Plural-Forms: nplurals=3; plural=(n==1?0:(((n%100>19)||((n%100==0)&&(n!=0)))?2:1));
 Accesorii Toate Audio Browser Calculator Ceas Centru de control Dezvoltare E-mail Ieșire Aplicații preferate: (click aici pentru a edita lista) Manager de fișiere Fișiere Jocuri Grafică Internet Clic stânga- Adăugați, mutați, eliminați pictogramele din Bara de instrumente; Faceți clic dreapta - Gestionați bara de instrumente Meniul Multimedia Manager de rețea - Connman Știri Birou Poze Recent Căutați programe și fișiere Setări Arată desktop Opriți aplicația Sistem Comutator de sarcini Terminal Text Editor de text Deconectați unitățile USB Video Volum Vreme Vreme Browser web 