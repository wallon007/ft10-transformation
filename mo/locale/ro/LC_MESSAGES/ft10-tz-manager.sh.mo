��          T      �       �      �      �      �   
   �   k        p  �  �     &     <     X  	   i  �   s                                             $add_tz_text!add:FBTN $delete_tz_text!remove:FBTN ADD Time zone FT10-clock Insert Time Zone to be added to the World Clock \n (Enter nothing to search trough all available timezones) REMOVE Time zone Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Romanian (https://www.transifex.com/antix-linux-community-contributions/teams/120110/ro/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: ro
Plural-Forms: nplurals=3; plural=(n==1?0:(((n%100>19)||((n%100==0)&&(n!=0)))?2:1));
 $add_tz_text!add:FBTN $delete_tz_text!remove:FBTN ADAUGĂ Fus orar FT10-ceas Introduceți fusul orar pentru a fi adăugat la ceasul mondial \n (Nu introduceți nimic pentru a căuta prin toate fusurile orare disponibile) Elimină fusul orar 