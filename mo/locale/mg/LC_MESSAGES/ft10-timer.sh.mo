��          �   %   �      P     Q  "   f     �  1   �     �     �     �  	   �  !        %     =     B  '   H  !   p  #   �  #   �     �     �     �                1     R     g  
   n  z  y     �  D     2   W  H   �  
   �  
   �  "   �       &        9     Q     Y  6   ]  3   �  5   �  /   �     .     ?     N     W  4   h  ;   �  "   �     �     	        	                                                                             
                                        Alarm sound filename Ask to begin each set (all timers) Ask to begin each timer Auto close progress bar display when all sets end Configuration Duration Each timer end Finished. Interface to Sysmonitor Indicator Multiple Timer settings Name Never Number of times to run set (all timers) Pop-up message when each set ends Pop-up message when each timer ends Progress Bar update every x seconds Ready to start SUSPEND COMPUTER Seconds Seconds!Minutes Sound alarm when each set ends Sound alarm when each timer ends Timer duration units Timers has ended. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Malagasy (https://www.transifex.com/antix-linux-community-contributions/teams/120110/mg/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: mg
Plural-Forms: nplurals=2; plural=(n > 1);
 Anaran-drakitra feo fanairana Angataho ny hanomboka ny andiany tsirairay (famantaranandro rehetra) Angataho ny hanomboka ny fameram-potoana tsirairay Asehoy ny bara fandrosoana automatique rehefa tapitra ny andiany rehetra fanahafana Faharetana Ny famerana tsirairay dia mifarana vita. Interface amin'ny Sysmonitor Indicator Fikirana Timer maromaro Anarana tsy Isan'ny hazakazaka napetraka (famantaranandro rehetra) Hafatra pop-up rehefa mifarana ny andiany tsirairay Hafatra mipoitra rehefa tapitra ny famerana tsirairay Fanavaozana ny Bar Progress isaky ny x segondra Vonona hanomboka AMPIO ORDINIKA segondra segondra!minitra Manao fanairana rehefa mifarana ny andiany tsirairay Manao fanairana rehefa tapitra ny fameram-potoana tsirairay Units faharetan'ny fameram-potoana Timers efa tapitra. 