��          �      l      �  (   �  '   
  7   2  '   j     �  G   �     �     �                    ,     5     >     G     S     Z  $   z     �     �  z  �  (   +  '   T  E   |  '   �     �  G   �     E  	   T     ^     r     �  
   �     �  	   �     �     �  /   �  7        9     I     	                                                                                         
        \t<b>$alarm_set_to_text $hr:$mn</b>      \t<b>$alarm_text $hr:$mn...</b> \n    Are you sure to set the alarm for that time tomorrow? $alarm_set_to_text $hr:$mn
$cancel_text $are_you_sure_text $cancel_alarm_text !bash -c 'cancel_quit_tray $XPID $target'!gtk-cancel $confirm_title Alarm Alarm set to Are you sure??? Cancel Alarm Duration Hour::CB Interval Minutes::CB Repeat To cancel, right-click for menu To cancel, right-click icon for menu infinite seconds Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Malagasy (https://www.transifex.com/antix-linux-community-contributions/teams/120110/mg/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: mg
Plural-Forms: nplurals=2; plural=(n > 1);
     \t<b>$alarm_set_to_text $hr:$mn</b>      \t<b>$alarm_text $hr:$mn...</b> \n    Azo antoka ve fa hametraka ny fanairana ho an'io ora io rahampitso? $alarm_set_to_text $hr:$mn
$cancel_text $are_you_sure_text $cancel_alarm_text !bash -c 'cancel_quit_tray $XPID $target'!gtk-cancel $confirm_title Fanairana Nalefa ny fanairana Azonao antoka ve izany??? Fanairana fanairana Faharetana Ora::CB Elanelana minitra::CB Avereno Raha hanafoana, tsindrio havanana amin'ny menio Raha hanafoana, tsindrio havanana kisary ho an'ny menio tsy manam-petra segondra 