��          �   %   �      P     Q  "   f     �  1   �     �     �     �  	   �  !        %     =     B  '   H  !   p  #   �  #   �     �     �     �                1     R     g  
   n  �  y     �  '        :  0   V     �     �     �     �     �     �     �     �  '   �     "  $   >     c     �     �     �  
   �     �  $   �     �  	                	                                                                             
                                        Alarm sound filename Ask to begin each set (all timers) Ask to begin each timer Auto close progress bar display when all sets end Configuration Duration Each timer end Finished. Interface to Sysmonitor Indicator Multiple Timer settings Name Never Number of times to run set (all timers) Pop-up message when each set ends Pop-up message when each timer ends Progress Bar update every x seconds Ready to start SUSPEND COMPUTER Seconds Seconds!Minutes Sound alarm when each set ends Sound alarm when each timer ends Timer duration units Timers has ended. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Chinese (Taiwan) (https://www.transifex.com/antix-linux-community-contributions/teams/120110/zh_TW/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: zh_TW
Plural-Forms: nplurals=1; plural=0;
 報警聲音文件名 要求開始每組（所有計時器） 要求開始每個計時器 所有集合結束時自動關閉進度條顯示 配置 期間 每個定時器結束 完成的。 Sysmonitor 指示器的接口 多個定時器設置 名稱 絕不 運行次數設置（所有計時器） 每組結束時彈出消息 每個計時器結束時彈出消息 進度條每 x 秒更新一次 準備開始 暫停電腦 秒 秒!分鐘 每組結束時發出警報 每個計時器結束時發出警報 計時器持續時間單位 計時器 結束了。 