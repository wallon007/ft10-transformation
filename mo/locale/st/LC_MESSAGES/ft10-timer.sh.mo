��          �   %   �      P     Q  "   f     �  1   �     �     �     �  	   �  !        %     =     B  '   H  !   p  #   �  #   �     �     �     �                1     R     g  
   n  �  y  #   �  4      &   U  H   |  	   �     �     �  	   �      �          8     @  5   L  *   �  7   �  :   �           2     A     M  '   b  4   �     �     �  	   �        	                                                                             
                                        Alarm sound filename Ask to begin each set (all timers) Ask to begin each timer Auto close progress bar display when all sets end Configuration Duration Each timer end Finished. Interface to Sysmonitor Indicator Multiple Timer settings Name Never Number of times to run set (all timers) Pop-up message when each set ends Pop-up message when each timer ends Progress Bar update every x seconds Ready to start SUSPEND COMPUTER Seconds Seconds!Minutes Sound alarm when each set ends Sound alarm when each timer ends Timer duration units Timers has ended. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Sotho, Southern (https://www.transifex.com/antix-linux-community-contributions/teams/120110/st/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: st
Plural-Forms: nplurals=2; plural=(n != 1);
 Lebitso la faele ea molumo oa alamo Kopa ho qala sete e 'ngoe le e 'ngoe (linako tsohle) Kopa ho qala sebali se seng le se seng Koala ka bohona ponts'o ea bara ea tsoelo-pele ha li-sete tsohle li fela Tlhophiso Nako Qetello ea nako ka 'ngoe E felile. Sehokelo ho Sysmonitor Indicator Litlhophiso tse ngata tsa Timer Lebitso Le ka mohla Nomoro ea linako tsa ho tsamaisa sete (linako tsohle) Molaetsa oa pop-up ha sete ka 'ngoe e fela Molaetsa oa pop-up ha sebali se seng le se seng se fela Progress Bar e ntlafatsa metsotsoana e meng le e meng ea x E loketse ho qala EKETSANG KOMPI Metsotsoana Metsotsoana!Metsotso Alamo ya modumo ha sete ka nngwe e fela Alamo ea molumo ha sebali se seng le se seng se fela Liyuniti tsa nako ea nako Lisebelisoa tsa nako e felile. 