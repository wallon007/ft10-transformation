��    
      l      �       �      �           .  	   ;     E     Z     l     x     �  �  �  *   6  2   a     �     �     �     �     �     �  &                      	                      
    CPU and RAM indicator's Font CPU and RAM usage indicator:btn Clock's Font Clock:btn Configure FT10 fonts Date in clock:btn Date's Font Menu and toolbar:btn Toolbar and Menu's Font Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:39+0000
Last-Translator: Wallon Wallon, 2022
Language-Team: French (Belgium) (https://www.transifex.com/antix-linux-community-contributions/teams/120110/fr_BE/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: fr_BE
Plural-Forms: nplurals=2; plural=(n > 1);
 Police des indicateurs du CPU et de la RAM Indicateur d'utilisation du CPU et de la RAM : btn Police de l'horloge Horloge : btn Configurer les polices FT10 Date de l'horloge : btn Police de la date Menu et barre d'outils : btn Police de la barre d'outils et du menu 