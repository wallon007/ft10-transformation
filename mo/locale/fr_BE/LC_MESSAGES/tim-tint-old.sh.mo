��    
      l      �       �      �   (     .   ,  0   [     �     �  )   �     �     �  �       �  8   �  >   �  >   ,     k  )     C   �      �                              	                
    ADD ICON!add:FBTN Choose application to add to the Toolbar Double click any Application to move its icon: Double click any Application to remove its icon: Icon located! MOVE ICON!gtk-go-back-rtl:FBTN No icon located, using default Gears icon REMOVE ICON!remove:FBTN Tint2 icons Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:39+0000
Last-Translator: Wallon Wallon, 2022
Language-Team: French (Belgium) (https://www.transifex.com/antix-linux-community-contributions/teams/120110/fr_BE/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: fr_BE
Plural-Forms: nplurals=2; plural=(n > 1);
 Ajouter une icône!add:FBTN Choisissez l'application à ajouter à la barre d'outils Double-cliquez sur une application pour déplacer son icône : Double-cliquez sur une application pour supprimer son icône : Icône localisée ! Déplacer une icône!gtk-go-back-rtl:FBTN Aucune icône localisée, utilisation de l'icône par défaut Gears Supprimer une icône!remove:FBTN Tint2 icônes 