��          �      l      �  (   �  '   
  7   2  '   j     �  G   �     �     �                    ,     5     >     G     S     Z  $   z     �     �  x  �  (   )  '   R  �   z  '        >  G   Q     �     �  =   �  8   �  4   2     g     ~     �  "   �  4   �  u   �  �   p     �     	     	                                                                                         
        \t<b>$alarm_set_to_text $hr:$mn</b>      \t<b>$alarm_text $hr:$mn...</b> \n    Are you sure to set the alarm for that time tomorrow? $alarm_set_to_text $hr:$mn
$cancel_text $are_you_sure_text $cancel_alarm_text !bash -c 'cancel_quit_tray $XPID $target'!gtk-cancel $confirm_title Alarm Alarm set to Are you sure??? Cancel Alarm Duration Hour::CB Interval Minutes::CB Repeat To cancel, right-click for menu To cancel, right-click icon for menu infinite seconds Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Tamil (https://www.transifex.com/antix-linux-community-contributions/teams/120110/ta/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: ta
Plural-Forms: nplurals=2; plural=(n != 1);
     \t<b>$alarm_set_to_text $hr:$mn</b>      \t<b>$alarm_text $hr:$mn...</b> \n    நாளை அந்த நேரத்திற்கான அலாரத்தை நிச்சயமாக அமைப்பீர்களா? $alarm_set_to_text $hr:$mn
$cancel_text $are_you_sure_text $cancel_alarm_text !bash -c 'cancel_quit_tray $XPID $target'!gtk-cancel $confirm_title அலாரம் அலாரம் அமைக்கப்பட்டது நீ சொல்வது உறுதியா??? அலாரத்தை ரத்துசெய் கால அளவு மணி::CB இடைவெளி நிமிடங்கள்::CB மீண்டும் செய்யவும் ரத்து செய்ய, மெனுவில் வலது கிளிக் செய்யவும் ரத்துசெய்ய, மெனுவிற்கான ஐகானை வலது கிளிக் செய்யவும் எல்லையற்ற வினாடிகள் 