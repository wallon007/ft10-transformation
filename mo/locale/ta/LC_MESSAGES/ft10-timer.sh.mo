��          �   %   �      P     Q  "   f     �  1   �     �     �     �  	   �  !        %     =     B  '   H  !   p  #   �  #   �     �     �     �                1     R     g  
   n  x  y  ?   �  �   2  l   �  �   F     	     (  >   ?     ~  b   �  5   �     4	  +   D	  �   p	  �   
  z   �
  �     (   �  @   �       4   4  |   i     �  /   f     �  .   �        	                                                                             
                                        Alarm sound filename Ask to begin each set (all timers) Ask to begin each timer Auto close progress bar display when all sets end Configuration Duration Each timer end Finished. Interface to Sysmonitor Indicator Multiple Timer settings Name Never Number of times to run set (all timers) Pop-up message when each set ends Pop-up message when each timer ends Progress Bar update every x seconds Ready to start SUSPEND COMPUTER Seconds Seconds!Minutes Sound alarm when each set ends Sound alarm when each timer ends Timer duration units Timers has ended. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Tamil (https://www.transifex.com/antix-linux-community-contributions/teams/120110/ta/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: ta
Plural-Forms: nplurals=2; plural=(n != 1);
 அலாரம் ஒலி கோப்பு பெயர் ஒவ்வொரு தொகுப்பையும் தொடங்கச் சொல்லுங்கள் (அனைத்து டைமர்கள்) ஒவ்வொரு டைமரையும் தொடங்கச் சொல்லுங்கள் அனைத்து செட்களும் முடிவடையும் போது தானாக மூடு முன்னேற்றப் பட்டி காட்சி கட்டமைப்பு கால அளவு ஒவ்வொரு டைமர் முடிவும் முடிந்தது. சிஸ்மோனிட்டர் காட்டிக்கான இடைமுகம் பல டைமர் அமைப்புகள் பெயர் ஒருபோதும் இல்லை இயக்க வேண்டிய நேரங்களின் எண்ணிக்கை (அனைத்து டைமர்கள்) ஒவ்வொரு தொகுப்பும் முடிவடையும் போது பாப்-அப் செய்தி ஒவ்வொரு டைமர் முடியும்போதும் பாப்-அப் செய்தி ஒவ்வொரு x வினாடிக்கும் முன்னேற்றப் பட்டியைப் புதுப்பிக்கவும் தொடங்கத் தயார் கணினியை இடைநிறுத்தவும் நொடிகள் நொடிகள்!நிமிடங்கள் ஒவ்வொரு செட் முடியும்போதும் அலாரம் ஒலிக்கும் ஒவ்வொரு டைமர் முடியும்போதும் அலாரம் ஒலிக்கும் டைமர் கால அலகுகள் டைமர்கள் முடிந்துவிட்டது. 