��          �   %   �      P     Q  "   f     �  1   �     �     �     �  	   �  !        %     =     B  '   H  !   p  #   �  #   �     �     �     �                1     R     g  
   n  Z  y  7   �  O     7   \  �   �          4  0   I     z  7   �  2   �     �     �  R   	  d   [	  f   �	  O   '
     w
  $   �
     �
     �
  V   �
  X   <  2   �     �     �        	                                                                             
                                        Alarm sound filename Ask to begin each set (all timers) Ask to begin each timer Auto close progress bar display when all sets end Configuration Duration Each timer end Finished. Interface to Sysmonitor Indicator Multiple Timer settings Name Never Number of times to run set (all timers) Pop-up message when each set ends Pop-up message when each timer ends Progress Bar update every x seconds Ready to start SUSPEND COMPUTER Seconds Seconds!Minutes Sound alarm when each set ends Sound alarm when each timer ends Timer duration units Timers has ended. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Ukrainian (https://www.transifex.com/antix-linux-community-contributions/teams/120110/uk/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: uk
Plural-Forms: nplurals=4; plural=(n % 1 == 0 && n % 10 == 1 && n % 100 != 11 ? 0 : n % 1 == 0 && n % 10 >= 2 && n % 10 <= 4 && (n % 100 < 12 || n % 100 > 14) ? 1 : n % 1 == 0 && (n % 10 ==0 || (n % 10 >=5 && n % 10 <=9) || (n % 100 >=11 && n % 100 <=14 )) ? 2: 3);
 Назва файлу звукового сигналу Попросити розпочати кожен сет (усі таймери) Попросіть почати кожен таймер Автоматично закривати індикатор виконання, коли всі набори закінчуються Конфігурація Тривалість Кожен таймер закінчується Готово. Інтерфейс до індикатора Sysmonitor Кілька налаштувань таймера Ім'я Ніколи Кількість запусків встановлено (усі таймери) Спливаюче повідомлення, коли кожен набір закінчується Спливаюче повідомлення, коли кожен таймер закінчується Панель прогресу оновлюється кожні х секунд Готовий почати ПРИВИНЯТИ КОМП'ЮТЕР Секунди Секунди!Хвилини Звуковий сигнал, коли закінчується кожен набір Звуковий сигнал, коли закінчується кожен таймер Одиниці тривалості таймера Таймери закінчився. 