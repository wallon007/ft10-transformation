��          �   %   �      P     Q  "   f     �  1   �     �     �     �  	   �  !        %     =     B  '   H  !   p  #   �  #   �     �     �     �                1     R     g  
   n  {  y  )   �  :     %   Z  Y   �     �     �     �       "     +   3     _     d  O   k  &   �  /   �  5        H     ^     s     {  )   �  -   �  +   �     	     	        	                                                                             
                                        Alarm sound filename Ask to begin each set (all timers) Ask to begin each timer Auto close progress bar display when all sets end Configuration Duration Each timer end Finished. Interface to Sysmonitor Indicator Multiple Timer settings Name Never Number of times to run set (all timers) Pop-up message when each set ends Pop-up message when each timer ends Progress Bar update every x seconds Ready to start SUSPEND COMPUTER Seconds Seconds!Minutes Sound alarm when each set ends Sound alarm when each timer ends Timer duration units Timers has ended. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Albanian (https://www.transifex.com/antix-linux-community-contributions/teams/120110/sq/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: sq
Plural-Forms: nplurals=2; plural=(n != 1);
 Emri i skedarit të tingullit të alarmit Kërkoni të filloni çdo grup (të gjithë kohëmatësit) Kërkoni të filloni çdo kohëmatës Mbyllja automatike e shiritit të përparimit shfaqet kur të gjitha grupet përfundojnë Konfigurimi Kohëzgjatja Çdo fund timer Përfunduar. Ndërfaqja me treguesin Sysmonitor Cilësimet e shumëfishta të kohëmatësit Emri kurrë Numri i herëve për të ekzekutuar është caktuar (të gjithë kohëmatësit) Mesazh pop-up kur çdo grup përfundon Mesazh kërcyes kur përfundon çdo kohëmatës Përditësimi i Shiritit të Progresit çdo x sekonda Gati për të filluar PEZULLOJ KOMPJUTERIN Sekonda Sekonda!Minutat Tingëllon alarm kur çdo grup përfundon Alarmi me zë kur çdo kohëmatës përfundon Njësitë e kohëzgjatjes së kohëmatësit Timers ka përfunduar. 