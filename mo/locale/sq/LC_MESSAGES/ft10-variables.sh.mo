��    (      \  5   �      p     q     }     �     �  
   �     �     �     �     �     �  ,   �     �                      F        f  
   k     v     �     �     �     �     �     �     �     �     �     �     �                    &     ,     3     ;     D  {  P  	   �  
   �     �  
   �     �     �       	        !     (  D   .     s  	   �     �     �     �  b   �       
              >     D     J     W  #   `  
   �     �     �     �     �     �     �     �     �     	     	     	     	     	                       
          	   $                                                     &   (       !          %          "                             #                    '        Accessories All Audio Browser Calculator Clock Control Centre Development E-mail Exit Favorite Apps: (click here to edit the list) File Manager Files Games Graphics Internet Left click- Add,move,remove Toolbar icons; Right click- Manage Toolbar Menu Multimedia Network manager - Connman News Office Pics Recent Search for programs and files Settings Show Desktop Stop App System Task switcher Terminal Text Text Editor Unplug USB drives Video Volume Weather Weather  Web Browser Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Albanian (https://www.transifex.com/antix-linux-community-contributions/teams/120110/sq/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: sq
Plural-Forms: nplurals=2; plural=(n != 1);
 Aksesorë Të gjitha Audio Shfletuesi Llogaritësi Ora Qendra e Kontrollit Zhvillimi E-mail Dilni Aplikacionet e preferuara: (kliko këtu për të modifikuar listën) Menaxheri i skedarëve Skedarët Lojëra Grafika internet Klikoni majtas- Shto, zhvendos, hiqni ikonat e Toolbar; Klikoni me të djathtën - Menaxho Toolbar Menu Multimedia Menaxheri i rrjetit - Connman Lajme Zyrë Fotografitë E fundit Kërkoni për programe dhe skedarë Cilësimet Shfaq Desktopin Ndalo aplikacionin Sistemi Ndërruesi i detyrave Terminal Teksti Redaktori i tekstit Hiqni disqet USB Video Vëllimi Moti Moti Shfletues uebi 