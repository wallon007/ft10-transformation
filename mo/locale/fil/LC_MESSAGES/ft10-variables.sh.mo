��    (      \  5   �      p     q     }     �     �  
   �     �     �     �     �     �  ,   �     �                      F        f  
   k     v     �     �     �     �     �     �     �     �     �     �     �                    &     ,     3     ;     D  �  P               #     )  
   1     <     C  	   R     \     c  ;   k     �     �     �     �     �  l   �     P  
   U  !   `     �     �     �  
   �     �     �     �     �     �     �     	     	     !	     -	     H	     N	     S	     [	     c	                       
          	   $                                                     &   (       !          %          "                             #                    '        Accessories All Audio Browser Calculator Clock Control Centre Development E-mail Exit Favorite Apps: (click here to edit the list) File Manager Files Games Graphics Internet Left click- Add,move,remove Toolbar icons; Right click- Manage Toolbar Menu Multimedia Network manager - Connman News Office Pics Recent Search for programs and files Settings Show Desktop Stop App System Task switcher Terminal Text Text Editor Unplug USB drives Video Volume Weather Weather  Web Browser Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Filipino (https://www.transifex.com/antix-linux-community-contributions/teams/120110/fil/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: fil
Plural-Forms: nplurals=2; plural=(n == 1 || n==2 || n==3) || (n % 10 != 4 || n % 10 != 6 || n % 10 != 9);
 Mga accessories Lahat Audio Browser Calculator orasan Control Center Pag-unlad E-mail Lumabas Mga Paboritong App: (i-click dito para i-edit ang listahan) Tagapamahala ng File Mga file Mga laro Mga graphic Internet Kaliwang pag-click- Magdagdag, ilipat, alisin ang mga icon ng Toolbar; I-right click- Pamahalaan ang Toolbar Menu Multimedia Tagapamahala ng network - Connman Balita Opisina Mga litrato Kamakailan Maghanap ng mga program at file Mga setting Ipakita ang Desktop Ihinto ang App Sistema Tagalipat ng gawain Terminal Text Text Editor I-unplug ang mga USB drive Video Dami Panahon Panahon Web Browser 