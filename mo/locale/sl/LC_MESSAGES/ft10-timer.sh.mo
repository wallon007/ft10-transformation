��          �   %   �      P     Q  "   f     �  1   �     �     �     �  	   �  !        %     =     B  '   H  !   p  #   �  #   �     �     �     �                1     R     g  
   n  �  y     *  6   D  &   {  E   �     �     �     �          $     D     _     c  2   j  +   �  /   �  .   �     (     @     U     \  &   j  *   �     �  
   �     �        	                                                                             
                                        Alarm sound filename Ask to begin each set (all timers) Ask to begin each timer Auto close progress bar display when all sets end Configuration Duration Each timer end Finished. Interface to Sysmonitor Indicator Multiple Timer settings Name Never Number of times to run set (all timers) Pop-up message when each set ends Pop-up message when each timer ends Progress Bar update every x seconds Ready to start SUSPEND COMPUTER Seconds Seconds!Minutes Sound alarm when each set ends Sound alarm when each timer ends Timer duration units Timers has ended. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Slovenian (https://www.transifex.com/antix-linux-community-contributions/teams/120110/sl/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: sl
Plural-Forms: nplurals=4; plural=(n%100==1 ? 0 : n%100==2 ? 1 : n%100==3 || n%100==4 ? 2 : 3);
 Ime datoteke zvoka alarma Prosite za začetek vsakega niza (vsi merilniki časa) Prosite za začetek vsakega časovnika Samodejno zapiranje prikaza vrstice napredka, ko se končajo vsi nizi Konfiguracija Trajanje Konec vsakega časovnika Dokončano. Vmesnik za indikator Sysmonitor Več nastavitev časovnika ime Nikoli Nastavljeno število zagonov (vsi merilniki časa) Pojavno sporočilo, ko se vsak sklop konča Pojavno sporočilo, ko se vsak časovnik konča Vrstica napredka se posodablja vsakih x sekund Pripravljen za začetek USTAVITE RAČUNALNIK Sekund Sekund!Minute Zvočni alarm, ko se vsak sklop konča Zvočni alarm, ko se vsak časovnik konča Enote trajanja časovnika Časovniki se je končalo. 