��          �      l      �  (   �  '   
  7   2  '   j     �  G   �     �     �                    ,     5     >     G     S     Z  $   z     �     �  t  �  (   %  '   N  C   v  '   �     �  G   �     =     L     R     g     }     �     �     �  	   �     �  )   �  1   �               	                                                                                         
        \t<b>$alarm_set_to_text $hr:$mn</b>      \t<b>$alarm_text $hr:$mn...</b> \n    Are you sure to set the alarm for that time tomorrow? $alarm_set_to_text $hr:$mn
$cancel_text $are_you_sure_text $cancel_alarm_text !bash -c 'cancel_quit_tray $XPID $target'!gtk-cancel $confirm_title Alarm Alarm set to Are you sure??? Cancel Alarm Duration Hour::CB Interval Minutes::CB Repeat To cancel, right-click for menu To cancel, right-click icon for menu infinite seconds Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Javanese (https://www.transifex.com/antix-linux-community-contributions/teams/120110/jv/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: jv
Plural-Forms: nplurals=1; plural=0;
     \t<b>$alarm_set_to_text $hr:$mn</b>      \t<b>$alarm_text $hr:$mn...</b> \n    Apa sampeyan yakin bakal nyetel weker kanggo wektu kasebut sesuk? $alarm_set_to_text $hr:$mn
$cancel_text $are_you_sure_text $cancel_alarm_text !bash -c 'cancel_quit_tray $XPID $target'!gtk-cancel $confirm_title Weker Weker disetel kanggo Apa sampeyan yakin??? Batal Weker Duration jam::CB Interval menit::CB mbaleni Kanggo mbatalake, klik-tengen kanggo menu Kanggo mbatalake, klik-tengen lambang kanggo menu tanpa wates detik 