��    
      l      �       �      �   (     .   ,  0   [     �     �  )   �     �     �  �       �  ;   �  A   
  C   L     �  !   �  G   �       4   .                         	                
    ADD ICON!add:FBTN Choose application to add to the Toolbar Double click any Application to move its icon: Double click any Application to remove its icon: Icon located! MOVE ICON!gtk-go-back-rtl:FBTN No icon located, using default Gears icon REMOVE ICON!remove:FBTN Tint2 icons Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:39+0000
Last-Translator: marcelo cripe <marcelocripe@gmail.com>, 2022
Language-Team: Portuguese (Brazil) (https://www.transifex.com/antix-linux-community-contributions/teams/120110/pt_BR/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: pt_BR
Plural-Forms: nplurals=2; plural=(n > 1);
 ADICIONAR ÍCONE!add:FBTN Escolha o aplicativo para adicionar à barra de ferramentas Clique duas vezes em qualquer aplicativo para mover o seu ícone: Clique duas vezes em qualquer aplicativo para remover o seu ícone: O Ícone foi localizado! MOVER ÍCONE!gtk-go-back-rtl:FBTN Nenhum ícone foi localizado, utilizando o ícone padrão da engrenagem REMOVER ÍCONE!remove:FBTN Gerenciador de Ícones da Barra de Ferramentas Tint2 