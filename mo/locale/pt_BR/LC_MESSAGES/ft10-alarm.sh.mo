��          �      l      �  (   �  '   
  7   2  '   j     �  G   �     �     �                    ,     5     >     G     S     Z  $   z     �     �  �  �  (   ]  '   �  L   �  '   �     #  G   6     ~     �     �     �     �  	   �     �  	   �     �       J     J   `  	   �     �     	                                                                                         
        \t<b>$alarm_set_to_text $hr:$mn</b>      \t<b>$alarm_text $hr:$mn...</b> \n    Are you sure to set the alarm for that time tomorrow? $alarm_set_to_text $hr:$mn
$cancel_text $are_you_sure_text $cancel_alarm_text !bash -c 'cancel_quit_tray $XPID $target'!gtk-cancel $confirm_title Alarm Alarm set to Are you sure??? Cancel Alarm Duration Hour::CB Interval Minutes::CB Repeat To cancel, right-click for menu To cancel, right-click icon for menu infinite seconds Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: marcelo cripe <marcelocripe@gmail.com>, 2022
Language-Team: Portuguese (Brazil) (https://www.transifex.com/antix-linux-community-contributions/teams/120110/pt_BR/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: pt_BR
Plural-Forms: nplurals=2; plural=(n > 1);
     \t<b>$alarm_set_to_text $hr:$mn</b>      \t<b>$alarm_text $hr:$mn...</b> \n    Você tem certeza que quer definir o alarme para este horário de amanhã? $alarm_set_to_text $hr:$mn
$cancel_text $are_you_sure_text $cancel_alarm_text !bash -c 'cancel_quit_tray $XPID $target'!gtk-cancel $confirm_title Alarme O alarme está definido para Você tem a certeza??? Cancelar o Alarme Duração Hora(s)::CB Intervalo Minuto(s)::CB Repetições Para cancelar, clique com o botão direito do rato/mouse no ícone do menu Para cancelar, clique com o botão direito do rato/mouse no ícone do menu infinitas segundos 