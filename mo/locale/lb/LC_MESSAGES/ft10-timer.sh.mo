��          �   %   �      P     Q  "   f     �  1   �     �     �     �  	   �  !        %     =     B  '   H  !   p  #   �  #   �     �     �     �                1     R     g  
   n  �  y     �  $        5  @   P     �     �     �  	   �  "   �     �     �        /     '   3  %   [  "   �     �     �     �     �      �  "        (     ?     E        	                                                                             
                                        Alarm sound filename Ask to begin each set (all timers) Ask to begin each timer Auto close progress bar display when all sets end Configuration Duration Each timer end Finished. Interface to Sysmonitor Indicator Multiple Timer settings Name Never Number of times to run set (all timers) Pop-up message when each set ends Pop-up message when each timer ends Progress Bar update every x seconds Ready to start SUSPEND COMPUTER Seconds Seconds!Minutes Sound alarm when each set ends Sound alarm when each timer ends Timer duration units Timers has ended. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Luxembourgish (https://www.transifex.com/antix-linux-community-contributions/teams/120110/lb/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: lb
Plural-Forms: nplurals=2; plural=(n != 1);
 Alarm Sound Dateinumm Frot all Set unzefänken (all Timer) Frot all Timer unzefänken Auto zoumaachen Fortschrëtter Bar Display wann all Sets ophalen Configuratioun Dauer All Timer Enn Fäerdeg. Interface fir Sysmonitor Indikator Multiple Timer Astellunge Numm Ni Zuel vun Zäiten fir ze lafen gesat (all Timer) Pop-up Message wann all Set eriwwer ass Pop-up Message wann all Timer eriwwer Progress Bar update all x Sekonnen Prett fir unzefänken SUSPEND COMPUTER Sekonnen Sekonnen!Minutt Sound Alarm wann all Set eriwwer Sound Alarm wann all Timer eriwwer Timer Dauer Unitéiten Timer eriwwer ass. 