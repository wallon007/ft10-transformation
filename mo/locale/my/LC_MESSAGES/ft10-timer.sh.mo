��          �   %   �      P     Q  "   f     �  1   �     �     �     �  	   �  !        %     =     B  '   H  !   p  #   �  #   �     �     �     �                1     R     g  
   n  s  y  :   �  y   (  _   �  �     !   �     �  R        b  @   {  <   �     �     		  �   %	  �   �	  �   l
  k     K   �  F   �       1   3  w   e  �   �  :   v  !   �  *   �        	                                                                             
                                        Alarm sound filename Ask to begin each set (all timers) Ask to begin each timer Auto close progress bar display when all sets end Configuration Duration Each timer end Finished. Interface to Sysmonitor Indicator Multiple Timer settings Name Never Number of times to run set (all timers) Pop-up message when each set ends Pop-up message when each timer ends Progress Bar update every x seconds Ready to start SUSPEND COMPUTER Seconds Seconds!Minutes Sound alarm when each set ends Sound alarm when each timer ends Timer duration units Timers has ended. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Burmese (https://www.transifex.com/antix-linux-community-contributions/teams/120110/my/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: my
Plural-Forms: nplurals=1; plural=0;
 အချက်ပေးသံ ဖိုင်အမည် အစုံတစ်ခုစီကို စတင်ရန် (တိုင်မာများအားလုံး) timer တစ်ခုစီကို စတင်ရန် တောင်းဆိုပါ။ အစုံအားလုံးပြီးဆုံးသောအခါ အလိုအလျောက်ပိတ်သည့် တိုးတက်မှုဘားကိုပြသသည်။ ဖွဲ့စည်းမှု သင်တန်းကာလ timer တစ်ခုစီ ပြီးဆုံးသွားပါပြီ။ ပြီးပြီ။ Sysmonitor Indicator သို့ အင်တာဖေ့စ် တိုင်မာဆက်တင်များစွာ နာမည် ဘယ်တော့မှ သတ်မှတ်လုပ်ဆောင်ရန် အကြိမ်အရေအတွက် (တိုင်မာများအားလုံး) အစုံလိုက်တိုင်း ပြီးဆုံးသွားသောအခါတွင် ပေါ်လာသော မက်ဆေ့ချ် အချိန်တိုင်းကိရိယာတစ်ခုစီ ပြီးဆုံးသည့်အခါ ပေါ်လာသော မက်ဆေ့ချ် Progress Bar ကို x စက္ကန့်တိုင်း အပ်ဒိတ်လုပ်ပါ။ စတင်ရန်အဆင်သင့်ဖြစ်ပါပြီ။ ကွန်ပျူတာကို ရပ်လိုက်ပါ။ စက္ကန့် စက္ကန့်!မိနစ်များ အစုံလိုက်တိုင်း ပြီးဆုံးသောအခါ အချက်ပေးသံ အချိန်တိုင်းကိရိယာတစ်ခုစီ ပြီးဆုံးသည့်အခါ အချက်ပေးသံ Timer ကြာချိန် ယူနစ်များ တိုင်မာများ ပြီးသွားပါပြီ။ 