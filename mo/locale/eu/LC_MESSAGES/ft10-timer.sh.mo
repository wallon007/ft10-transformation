��          �   %   �      P     Q  "   f     �  1   �     �     �     �  	   �  !        %     =     B  '   H  !   p  #   �  #   �     �     �     �                1     R     g  
   n  y  y      �  6     %   K  O   q     �     �  "   �     �  #        '     G     M  @   V  ,   �  3   �  +   �     $     2  	   D     N  ,   _  4   �  #   �     �  
   �        	                                                                             
                                        Alarm sound filename Ask to begin each set (all timers) Ask to begin each timer Auto close progress bar display when all sets end Configuration Duration Each timer end Finished. Interface to Sysmonitor Indicator Multiple Timer settings Name Never Number of times to run set (all timers) Pop-up message when each set ends Pop-up message when each timer ends Progress Bar update every x seconds Ready to start SUSPEND COMPUTER Seconds Seconds!Minutes Sound alarm when each set ends Sound alarm when each timer ends Timer duration units Timers has ended. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Basque (https://www.transifex.com/antix-linux-community-contributions/teams/120110/eu/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: eu
Plural-Forms: nplurals=2; plural=(n != 1);
 Alarma soinu fitxategiaren izena Eskatu multzo bakoitza hasteko (tenporizadore guztiak) Eskatu tenporizadore bakoitza hasteko Itxi automatikoki aurrerapen-barra bistaratzea multzo guztiak amaitzen direnean Konfigurazioa Iraupena Tenporizadore bakoitza amaitzen da Amaitu. Sysmonitor adierazlerako interfazea Tenporizadorearen ezarpen anitz Izena Inoiz ez Exekutatu beharreko aldiz kopurua ezarri (tenporizadore guztiak) Pop-up mezua multzo bakoitza amaitzen denean Pop-up mezua tenporizadore bakoitza amaitzen denean Aurrerapen-barra eguneratu x segundoz behin Hasteko prest ORDENAGAILUA ETEN Segundoak Segundoak!Minutu Soinu alarma multzo bakoitza amaitzen denean Alarma soinua tenporizadore bakoitza amaitzen denean Tenporizadorearen iraupen-unitateak Tenporizadoreak amaitu da. 