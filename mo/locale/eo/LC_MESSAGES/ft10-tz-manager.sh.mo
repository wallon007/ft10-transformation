��          T      �       �      �      �      �   
   �   k        p  |  �     �          0     ?  r   M     �                                        $add_tz_text!add:FBTN $delete_tz_text!remove:FBTN ADD Time zone FT10-clock Insert Time Zone to be added to the World Clock \n (Enter nothing to search trough all available timezones) REMOVE Time zone Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Esperanto (https://www.transifex.com/antix-linux-community-contributions/teams/120110/eo/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: eo
Plural-Forms: nplurals=2; plural=(n != 1);
 $add_tz_text!add:FBTN $delete_tz_text!remove:FBTN ALDONU Horzono FT10-horloĝo Enmetu Horzonon por esti aldonita al la Monda Horloĝo \n (Enigu nenion por serĉi tra ĉiuj disponeblaj horzonoj) FORIGI Horzonon 