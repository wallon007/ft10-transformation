��    (      \  5   �      p     q     }     �     �  
   �     �     �     �     �     �  ,   �     �                      F        f  
   k     v     �     �     �     �     �     �     �     �     �     �     �                    &     ,     3     ;     D  }  P     �     �     �  	   �     �     �               (     /  8   6     o     �     �  	   �     �  �   �  	   2     <     H  	   b     l     x     ~     �     �     �     �     �     �     �     	     	     	     4	     ;	     B	     H	     N	                       
          	   $                                                     &   (       !          %          "                             #                    '        Accessories All Audio Browser Calculator Clock Control Centre Development E-mail Exit Favorite Apps: (click here to edit the list) File Manager Files Games Graphics Internet Left click- Add,move,remove Toolbar icons; Right click- Manage Toolbar Menu Multimedia Network manager - Connman News Office Pics Recent Search for programs and files Settings Show Desktop Stop App System Task switcher Terminal Text Text Editor Unplug USB drives Video Volume Weather Weather  Web Browser Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Portuguese (https://www.transifex.com/antix-linux-community-contributions/teams/120110/pt/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: pt
Plural-Forms: nplurals=2; plural=(n != 1);
 Acessórios Tudo Áudio Navegador Calculadora Relógio Centro de Controle Desenvolvimento E-mail Saída Aplicativos favoritos: (clique aqui para editar a lista) Gerenciador de arquivos arquivos Jogos Gráficos Internet Clique esquerdo- Adicionar, mover, remover ícones da barra de ferramentas; Clique com o botão direito - Gerenciar barra de ferramentas Cardápio Multimídia Gerente de rede - Connman Notícias Escritório Fotos Recente Pesquisar programas e arquivos Configurações Mostrar area de trabalho Parar aplicativo Sistema Alternador de tarefas terminal Texto Editor de texto Desconecte unidades USB Vídeo Volume Clima Clima Navegador da Web 