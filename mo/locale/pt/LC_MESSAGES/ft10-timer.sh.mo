��          �   %   �      P     Q  "   f     �  1   �     �     �     �  	   �  !        %     =     B  '   H  !   p  #   �  #   �     �     �     �                1     R     g  
   n  }  y      �  :     #   S  ]   w     �  	   �     �       &     '   :     b     g  C   m  ,   �  )   �  5        >     S     h     q  *   �  '   �  %   �     �  	   
	        	                                                                             
                                        Alarm sound filename Ask to begin each set (all timers) Ask to begin each timer Auto close progress bar display when all sets end Configuration Duration Each timer end Finished. Interface to Sysmonitor Indicator Multiple Timer settings Name Never Number of times to run set (all timers) Pop-up message when each set ends Pop-up message when each timer ends Progress Bar update every x seconds Ready to start SUSPEND COMPUTER Seconds Seconds!Minutes Sound alarm when each set ends Sound alarm when each timer ends Timer duration units Timers has ended. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Portuguese (https://www.transifex.com/antix-linux-community-contributions/teams/120110/pt/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: pt
Plural-Forms: nplurals=2; plural=(n != 1);
 Nome do arquivo de som de alarme Peça para iniciar cada conjunto (todos os temporizadores) Peça para iniciar cada cronômetro Exibição da barra de progresso de fechamento automático quando todos os conjuntos terminam Configuração Duração Cada fim do temporizador Finalizado. Interface para Indicador do Sysmonitor Várias configurações de temporizador Nome Nunca Número de vezes para executar o conjunto (todos os temporizadores) Mensagem pop-up quando cada conjunto termina Mensagem pop-up quando cada timer termina Atualização da barra de progresso a cada x segundos Pronto para começar SUSPENDER COMPUTADOR Segundos Segundos!Minutos Alarme sonoro quando cada conjunto termina Alarme sonoro quando cada timer termina Unidades de duração do temporizador Temporizadores terminou. 