��          �   %   �      P     Q  "   f     �  1   �     �     �     �  	   �  !        %     =     B  '   H  !   p  #   �  #   �     �     �     �                1     R     g  
   n  y  y     �  6     1   B  /   t     �     �     �     �  !   �     �            0     +   J  )   v  5   �     �     �  	   �       !     #   7     [     w     �        	                                                                             
                                        Alarm sound filename Ask to begin each set (all timers) Ask to begin each timer Auto close progress bar display when all sets end Configuration Duration Each timer end Finished. Interface to Sysmonitor Indicator Multiple Timer settings Name Never Number of times to run set (all timers) Pop-up message when each set ends Pop-up message when each timer ends Progress Bar update every x seconds Ready to start SUSPEND COMPUTER Seconds Seconds!Minutes Sound alarm when each set ends Sound alarm when each timer ends Timer duration units Timers has ended. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Nyanja (https://www.transifex.com/antix-linux-community-contributions/teams/120110/ny/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: ny
Plural-Forms: nplurals=2; plural=(n != 1);
 Dzina lafayilo la Alamu Funsani kuti muyambe seti iliyonse (zowerengera zonse) Funsani kuti muyambe chowerengera nthawi iliyonse Tsekani zowonetseratu zowonetsera zonse zikatha Kusintha Kutalika Nthawi iliyonse kumapeto Zatha. Interface to Sysmonitor Indicator Makonda angapo a Timer Dzina Ayi Nambala yanthawi zoyitanitsa (zowerengera zonse) Tumikira uthenga pamene aliyense seti kutha Uthenga wotulukira nthawi iliyonse ikatha Kupititsa patsogolo Bar sinthani masekondi x aliwonse Okonzeka kuyamba AYIMITSA KOMPYUTA Masekondi Masekondi!Mphindi Alamu yamawu ikatha seti iliyonse Alamu yamawu ikatha nthawi iliyonse Mayunitsi owerengera nthawi Zowerengera nthawi zatha. 