��          �      l      �  (   �  '   
  7   2  '   j     �  G   �     �     �                    ,     5     >     G     S     Z  $   z     �     �  v  �  (   '  '   P  X   x  '   �     �  G        T     c  "   o     �     �     �  	   �     �  	   �  
   �  ,     <   5  	   r     |     	                                                                                         
        \t<b>$alarm_set_to_text $hr:$mn</b>      \t<b>$alarm_text $hr:$mn...</b> \n    Are you sure to set the alarm for that time tomorrow? $alarm_set_to_text $hr:$mn
$cancel_text $are_you_sure_text $cancel_alarm_text !bash -c 'cancel_quit_tray $XPID $target'!gtk-cancel $confirm_title Alarm Alarm set to Are you sure??? Cancel Alarm Duration Hour::CB Interval Minutes::CB Repeat To cancel, right-click for menu To cancel, right-click icon for menu infinite seconds Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Vietnamese (https://www.transifex.com/antix-linux-community-contributions/teams/120110/vi/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: vi
Plural-Forms: nplurals=1; plural=0;
     \t<b>$alarm_set_to_text $hr:$mn</b>      \t<b>$alarm_text $hr:$mn...</b> \n    Bạn có chắc chắn đặt báo thức cho thời gian đó vào ngày mai không? $alarm_set_to_text $hr:$mn
$cancel_text $are_you_sure_text $cancel_alarm_text !bash -c 'cancel_quit_tray $XPID $target'!gtk-cancel $confirm_title Báo thức Báo thức được đặt thành Bạn có chắc không??? Hủy báo thức Khoảng thời gian Giờ::CB Khoảng thời gian Phút::CB Nói lại Để hủy, nhấp chuột phải vào menu Để hủy, nhấp chuột phải vào biểu tượng menu vô hạn giây 