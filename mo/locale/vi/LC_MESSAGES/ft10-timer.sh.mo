��          �   %   �      P     Q  "   f     �  1   �     �     �     �  	   �  !        %     =     B  '   H  !   p  #   �  #   �     �     �     �                1     R     g  
   n  v  y      �  E     .   W  Y   �     �     �            '   -     U     u     z  4   �  2   �  =   �  2   1     d     �     �     �  8   �  D   �  &   ,	     S	     _	        	                                                                             
                                        Alarm sound filename Ask to begin each set (all timers) Ask to begin each timer Auto close progress bar display when all sets end Configuration Duration Each timer end Finished. Interface to Sysmonitor Indicator Multiple Timer settings Name Never Number of times to run set (all timers) Pop-up message when each set ends Pop-up message when each timer ends Progress Bar update every x seconds Ready to start SUSPEND COMPUTER Seconds Seconds!Minutes Sound alarm when each set ends Sound alarm when each timer ends Timer duration units Timers has ended. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Vietnamese (https://www.transifex.com/antix-linux-community-contributions/teams/120110/vi/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: vi
Plural-Forms: nplurals=1; plural=0;
 Tên tệp âm thanh cảnh báo Yêu cầu bắt đầu mỗi bộ (tất cả các bộ hẹn giờ) Yêu cầu bắt đầu mỗi bộ hẹn giờ Hiển thị thanh tiến trình tự động đóng khi tất cả các bộ kết thúc Cấu hình Khoảng thời gian Mỗi kết thúc hẹn giờ Hoàn thành. Giao diện với Chỉ báo Sysmonitor Nhiều cài đặt Hẹn giờ Tên Không bao giờ Số lần chạy bộ (tất cả bộ hẹn giờ) Thông báo bật lên khi mỗi tập kết thúc Thông báo bật lên khi mỗi bộ hẹn giờ kết thúc Cập nhật thanh tiến trình sau mỗi x giây Sẵn sàng để bắt đầu TẠM NGỪNG MÁY TÍNH Giây Giây!Phút Báo động bằng âm thanh khi mỗi bộ kết thúc Báo động bằng âm thanh khi mỗi bộ hẹn giờ kết thúc Đơn vị thời lượng hẹn giờ Hẹn giờ đã kết thúc. 