��          �   %   �      P     Q  "   f     �  1   �     �     �     �  	   �  !        %     =     B  '   H  !   p  #   �  #   �     �     �     �                1     R     g  
   n     y     z  <   �  *   �  A        D     S     [     x  #   �     �     �     �  8   �  :     ;   B  '   ~     �     �  	   �     �  0   �  1   	     P	     m	  	   y	        	                                                                             
                                        Alarm sound filename Ask to begin each set (all timers) Ask to begin each timer Auto close progress bar display when all sets end Configuration Duration Each timer end Finished. Interface to Sysmonitor Indicator Multiple Timer settings Name Never Number of times to run set (all timers) Pop-up message when each set ends Pop-up message when each timer ends Progress Bar update every x seconds Ready to start SUSPEND COMPUTER Seconds Seconds!Minutes Sound alarm when each set ends Sound alarm when each timer ends Timer duration units Timers has ended. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Lithuanian (https://www.transifex.com/antix-linux-community-contributions/teams/120110/lt/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: lt
Plural-Forms: nplurals=4; plural=(n % 10 == 1 && (n % 100 > 19 || n % 100 < 11) ? 0 : (n % 10 >= 2 && n % 10 <=9) && (n % 100 > 19 || n % 100 < 11) ? 1 : n % 1 != 0 ? 2: 3);
 Signalo garso failo pavadinimas Paprašykite pradėti kiekvieną rinkinį (visi laikmačiai) Paprašykite pradėti kiekvieną laikmatį Automatiškai uždaroma eigos juosta, kai baigiasi visi rinkiniai Konfigūracija Trukmė Kiekviena laikmačio pabaiga Baigta. Sąsaja su Sysmonitor indikatoriumi Keli laikmačio nustatymai vardas Niekada Nustatytas paleidimo kartų skaičius (visi laikmačiai) Iššokantis pranešimas, kai baigiasi kiekvienas rinkinys Iššokantis pranešimas, kai baigiasi kiekvienas laikmatis Eigos juosta atnaujinama kas x sekundes Paruošta pradėti PASTABDYTI KOMPIUTERĮ sekundės sekundės!Minutės Garso signalas, kai baigiasi kiekvienas rinkinys Garso signalas, kai baigiasi kiekvienas laikmatis Laikmačio trukmės vienetai Laikmačiai baigėsi. 