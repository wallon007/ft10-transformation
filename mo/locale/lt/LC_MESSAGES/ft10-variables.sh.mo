��    (      \  5   �      p     q     }     �     �  
   �     �     �     �     �     �  ,   �     �                      F        f  
   k     v     �     �     �     �     �     �     �     �     �     �     �                    &     ,     3     ;     D     P     Q     Y     ^  
   e     p  	   ~     �     �     �     �  M   �     �       	        "  
   *  �   5     �     �     �     	     	  
   	     *	     3	  
   P	     [	     n	     �	     �	  
   �	     �	     �	     �	     �	     �	     �	     �	     
                       
          	   $                                                     &   (       !          %          "                             #                    '        Accessories All Audio Browser Calculator Clock Control Centre Development E-mail Exit Favorite Apps: (click here to edit the list) File Manager Files Games Graphics Internet Left click- Add,move,remove Toolbar icons; Right click- Manage Toolbar Menu Multimedia Network manager - Connman News Office Pics Recent Search for programs and files Settings Show Desktop Stop App System Task switcher Terminal Text Text Editor Unplug USB drives Video Volume Weather Weather  Web Browser Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Lithuanian (https://www.transifex.com/antix-linux-community-contributions/teams/120110/lt/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: lt
Plural-Forms: nplurals=4; plural=(n % 10 == 1 && (n % 100 > 19 || n % 100 < 11) ? 0 : (n % 10 >= 2 && n % 10 <=9) && (n % 100 > 19 || n % 100 < 11) ? 1 : n % 1 != 0 ? 2: 3);
 Priedai Visi Garsas Naršyklė Skaičiuoklė Laikrodis Valdymo centras Vystymas paštas Išeiti Mėgstamiausios programos: (spustelėkite čia norėdami redaguoti sąrašą) Failų tvarkyklė Failai Žaidimai Grafika internetas Kairiuoju pelės mygtuku spustelėkite - Pridėti, perkelti, pašalinti įrankių juostos piktogramas; Dešiniuoju pelės mygtuku spustelėkite - Tvarkyti įrankių juostą Meniu Multimedija Tinklo valdytojas - Connman žinios Biuras Nuotraukos Neseniai Ieškoti programų ir failų Nustatymai Rodyti darbalaukį Sustabdyti programą Sistema Užduočių perjungiklis Terminalas Tekstas Teksto redaktorius Atjunkite USB diskus Vaizdo įrašas Apimtis Orai Orai Interneto naršyklė 