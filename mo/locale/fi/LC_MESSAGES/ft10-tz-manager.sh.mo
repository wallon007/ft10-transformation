��          T      �       �      �      �      �   
   �   k        p  z  �     �          .  
   C  �   N     �                                        $add_tz_text!add:FBTN $delete_tz_text!remove:FBTN ADD Time zone FT10-clock Insert Time Zone to be added to the World Clock \n (Enter nothing to search trough all available timezones) REMOVE Time zone Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Finnish (https://www.transifex.com/antix-linux-community-contributions/teams/120110/fi/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: fi
Plural-Forms: nplurals=2; plural=(n != 1);
 $add_tz_text!add:FBTN $delete_tz_text!remove:FBTN LISÄÄ Aikavyöhyke FT10 kello Lisää maailmankelloon lisättävä aikavyöhyke \n (Älä kirjoita mitään hakeaksesi kaikki käytettävissä olevat aikavyöhykkeet) POISTA Aikavyöhyke 