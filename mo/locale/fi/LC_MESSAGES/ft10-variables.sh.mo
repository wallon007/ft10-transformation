��    (      \  5   �      p     q     }     �     �  
   �     �     �     �     �     �  ,   �     �                      F        f  
   k     v     �     �     �     �     �     �     �     �     �     �     �                    &     ,     3     ;     D  z  P     �     �     �     �     �     �     �     	            >   %     d  	   v     �  	   �     �  �   �       
   "     -     F     N     W     ]     k  	   �     �     �     �     �  
   �     �     �     �     	     	     $	     *	     0	                       
          	   $                                                     &   (       !          %          "                             #                    '        Accessories All Audio Browser Calculator Clock Control Centre Development E-mail Exit Favorite Apps: (click here to edit the list) File Manager Files Games Graphics Internet Left click- Add,move,remove Toolbar icons; Right click- Manage Toolbar Menu Multimedia Network manager - Connman News Office Pics Recent Search for programs and files Settings Show Desktop Stop App System Task switcher Terminal Text Text Editor Unplug USB drives Video Volume Weather Weather  Web Browser Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Finnish (https://www.transifex.com/antix-linux-community-contributions/teams/120110/fi/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: fi
Plural-Forms: nplurals=2; plural=(n != 1);
 Lisätarvikkeet Kaikki Audio Selain Laskin Kello Ohjauskeskus Kehitys Sähköposti Poistu Suosikkisovellukset: (muokkaa luetteloa napsauttamalla tätä) Tiedostonhallinta Tiedostot Pelit Grafiikka Internet Vasen painike - Lisää, siirrä, poista työkalupalkin kuvakkeet; Napsauta hiiren kakkospainikkeella - Hallitse työkalupalkkia Valikko Multimedia Verkkovastaava - Connman Uutiset Toimisto Kuvia Viimeaikaiset Etsi ohjelmia ja tiedostoja asetukset Näytä työpöytä Pysäytä sovellus Järjestelmä Tehtävän vaihtaja Terminaali Teksti Tekstieditori Irrota USB-asemat Video Äänenvoimakkuus Sää Sää Nettiselain 