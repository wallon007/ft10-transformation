��          �      l      �  (   �  '   
  7   2  '   j     �  G   �     �     �                    ,     5     >     G     S     Z  $   z     �     �  z  �  (   +  '   T  o   |  '   �       G   '     o  "   ~  #   �  )   �  2   �     "     <     J     ]  )   z  v   �  �        �     �     	                                                                                         
        \t<b>$alarm_set_to_text $hr:$mn</b>      \t<b>$alarm_text $hr:$mn...</b> \n    Are you sure to set the alarm for that time tomorrow? $alarm_set_to_text $hr:$mn
$cancel_text $are_you_sure_text $cancel_alarm_text !bash -c 'cancel_quit_tray $XPID $target'!gtk-cancel $confirm_title Alarm Alarm set to Are you sure??? Cancel Alarm Duration Hour::CB Interval Minutes::CB Repeat To cancel, right-click for menu To cancel, right-click icon for menu infinite seconds Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Sinhala (https://www.transifex.com/antix-linux-community-contributions/teams/120110/si/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: si
Plural-Forms: nplurals=2; plural=(n != 1);
     \t<b>$alarm_set_to_text $hr:$mn</b>      \t<b>$alarm_text $hr:$mn...</b> \n    හෙට එම වේලාවට සීනුව සැකසීමට ඔබට විශ්වාසද? $alarm_set_to_text $hr:$mn
$cancel_text $are_you_sure_text $cancel_alarm_text !bash -c 'cancel_quit_tray $XPID $target'!gtk-cancel $confirm_title අනතුරු ඇඟවීම එලාමය සකසා ඇත ඔයාට විශ්වාස ද??? එලාමය අවලංගු කරන්න කාල සීමාව පැය::CB අන්තරය මිනිත්තු::CB නැවත නැවත කරන්න අවලංගු කිරීමට, මෙනුව සඳහා දකුණු-ක්ලික් කරන්න අවලංගු කිරීමට, මෙනුව සඳහා අයිකනය දකුණු-ක්ලික් කරන්න අනන්තය තත්පර 