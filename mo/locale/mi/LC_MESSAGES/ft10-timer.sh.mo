��          �   %   �      P     Q  "   f     �  1   �     �     �     �  	   �  !        %     =     B  '   H  !   p  #   �  #   �     �     �     �                1     R     g  
   n  w  y     �  ,        :  J   T     �     �     �     �     �     �       	     4     "   J  #   m  !   �     �     �     �     �  ,   �  -   !  
   O  	   Z  	   d        	                                                                             
                                        Alarm sound filename Ask to begin each set (all timers) Ask to begin each timer Auto close progress bar display when all sets end Configuration Duration Each timer end Finished. Interface to Sysmonitor Indicator Multiple Timer settings Name Never Number of times to run set (all timers) Pop-up message when each set ends Pop-up message when each timer ends Progress Bar update every x seconds Ready to start SUSPEND COMPUTER Seconds Seconds!Minutes Sound alarm when each set ends Sound alarm when each timer ends Timer duration units Timers has ended. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Maori (https://www.transifex.com/antix-linux-community-contributions/teams/120110/mi/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: mi
Plural-Forms: nplurals=2; plural=(n > 1);
 Ingoa kōnae tangi whakaoho Tonoa kia timata ia huinga (nga taima katoa) Tonoa kia timata ia taima Katia aunoa te whakaaturanga pae ahunga whakamua ina mutu nga huinga katoa Whirihoranga Roanga Ka mutu ia matawā Kua oti. Atanga ki Sysmonitor Indicator Tautuhinga Matama Maha Ingoa Kaua rawa Te maha o nga wa e whakahaerea ana (nga taima katoa) Karere pahū-ake ka mutu ia huinga Karere pahū-ake ka mutu ia matawā Whakahou Pae Kaunuku ia x hēkona Kua reri ki te timata WHAKATARI ROROHIA Hekona Hekona!Nga meneti Whakatangihia te whakaoho ina mutu ia huinga Whakatangihia te whakaoho ina mutu ia matawā Wae wa roa Nga taima kua mutu. 