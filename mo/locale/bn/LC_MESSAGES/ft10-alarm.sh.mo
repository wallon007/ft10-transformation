��          �      l      �  (   �  '   
  7   2  '   j     �  G   �     �     �                    ,     5     >     G     S     Z  $   z     �     �  z  �  (   +  '   T  �   |  '   2     Z  G   m     �     �  9   �  ,     8   >     w     �     �     �  .   �  a   �  q   V     �     �     	                                                                                         
        \t<b>$alarm_set_to_text $hr:$mn</b>      \t<b>$alarm_text $hr:$mn...</b> \n    Are you sure to set the alarm for that time tomorrow? $alarm_set_to_text $hr:$mn
$cancel_text $are_you_sure_text $cancel_alarm_text !bash -c 'cancel_quit_tray $XPID $target'!gtk-cancel $confirm_title Alarm Alarm set to Are you sure??? Cancel Alarm Duration Hour::CB Interval Minutes::CB Repeat To cancel, right-click for menu To cancel, right-click icon for menu infinite seconds Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Bengali (https://www.transifex.com/antix-linux-community-contributions/teams/120110/bn/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: bn
Plural-Forms: nplurals=2; plural=(n != 1);
     \t<b>$alarm_set_to_text $hr:$mn</b>      \t<b>$alarm_text $hr:$mn...</b> \n    আপনি কি আগামীকাল সেই সময়ের জন্য অ্যালার্ম সেট করার বিষয়ে নিশ্চিত? $alarm_set_to_text $hr:$mn
$cancel_text $are_you_sure_text $cancel_alarm_text !bash -c 'cancel_quit_tray $XPID $target'!gtk-cancel $confirm_title এলার্ম এলার্ম সেট করা হয়েছে তুমি কি নিশ্চিত??? অ্যালার্ম বাতিল করুন সময়কাল ঘন্টা::CB অন্তর মিনিট::CB পুনরাবৃত্তি করুন বাতিল করতে, মেনুর জন্য ডান-ক্লিক করুন বাতিল করতে, মেনুর জন্য আইকনে ডান-ক্লিক করুন অসীম সেকেন্ড 