��          �   %   �      P     Q  "   f     �  1   �     �     �     �  	   �  !        %     =     B  '   H  !   p  #   �  #   �     �     �     �                1     R     g  
   n  w  y  1   �  Y   #  ;   }  �   �     I     b  *   s     �  6   �  4   �          #  L   2  O     S   �  Y   #	     }	  %   �	     �	     �	  M   �	  Q   >
  2   �
     �
     �
        	                                                                             
                                        Alarm sound filename Ask to begin each set (all timers) Ask to begin each timer Auto close progress bar display when all sets end Configuration Duration Each timer end Finished. Interface to Sysmonitor Indicator Multiple Timer settings Name Never Number of times to run set (all timers) Pop-up message when each set ends Pop-up message when each timer ends Progress Bar update every x seconds Ready to start SUSPEND COMPUTER Seconds Seconds!Minutes Sound alarm when each set ends Sound alarm when each timer ends Timer duration units Timers has ended. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Kazakh (https://www.transifex.com/antix-linux-community-contributions/teams/120110/kk/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: kk
Plural-Forms: nplurals=2; plural=(n!=1);
 Дабыл дыбысының файл атауы Әрбір жиынды бастауды сұраңыз (барлық таймерлер) Әрбір таймерді бастауды сұраңыз Барлық жиындар аяқталған кезде автоматты түрде жабылу барысы жолағын көрсету Конфигурация Ұзақтығы Әрбір таймер аяқталады Аяқталды. Sysmonitor индикаторына интерфейс Бірнеше таймер параметрлері Аты Ешқашан Орындалатын уақыт саны (барлық таймерлер) Әрбір жиын аяқталған кезде қалқымалы хабар Әрбір таймер аяқталған кезде қалқымалы хабар Прогресс жолағы әрбір x секунд сайын жаңартылады Бастауға дайын КОМПЬЮТЕРДІ ТОҚТАТУ Секундтар Секундтар!Минут Әрбір жиын аяқталған кезде дыбыстық дабыл Әрбір таймер аяқталған кезде дыбыстық дабыл Таймер ұзақтығы бірліктері Таймерлер аяқталды. 