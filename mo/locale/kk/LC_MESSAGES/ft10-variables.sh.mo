��    (      \  5   �      p     q     }     �     �  
   �     �     �     �     �     �  ,   �     �                      F        f  
   k     v     �     �     �     �     �     �     �     �     �     �     �                    &     ,     3     ;     D  w  P     �     �  
   �     �       
        *     J  !   S     u  n   ~     �     	          '     6  
  G  
   R	     ]	  %   t	     �	  
   �	     �	  
   �	  C   �	     
  (   1
  #   Z
     ~
  %   �
     �
  
   �
  !   �
  -   �
  
        $     1     A     Q                       
          	   $                                                     &   (       !          %          "                             #                    '        Accessories All Audio Browser Calculator Clock Control Centre Development E-mail Exit Favorite Apps: (click here to edit the list) File Manager Files Games Graphics Internet Left click- Add,move,remove Toolbar icons; Right click- Manage Toolbar Menu Multimedia Network manager - Connman News Office Pics Recent Search for programs and files Settings Show Desktop Stop App System Task switcher Terminal Text Text Editor Unplug USB drives Video Volume Weather Weather  Web Browser Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Kazakh (https://www.transifex.com/antix-linux-community-contributions/teams/120110/kk/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: kk
Plural-Forms: nplurals=2; plural=(n!=1);
 Аксессуарлар Барлық Аудио Браузер Калькулятор Сағат Басқару орталығы Даму Электрондық пошта Шығу Таңдаулы қолданбалар: (тізімді өңдеу үшін осы жерді басыңыз) Файл менеджері Файлдар Ойындар Графика ғаламтор Сол жақ түймені басыңыз - Құралдар тақтасының белгішелерін қосу, жылжыту, жою; Тінтуірдің оң жақ түймешігімен басыңыз - Құралдар тақтасын басқару Мәзір Мультимедиа Желі менеджері - Connman Жаңалықтар кеңсе Суреттер Соңғы Бағдарламалар мен файлдарды іздеңіз Параметрлер Жұмыс үстелін көрсету Қолданбаны тоқтату Жүйе Тапсырма ауыстырғыш Терминал Мәтін Мәтіндік редактор USB дискілерін ажыратыңыз Бейне Көлемі Ауа райы Ауа райы Веб-шолғыш 