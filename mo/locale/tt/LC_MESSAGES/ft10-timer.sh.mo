��          �   %   �      P     Q  "   f     �  1   �     �     �     �  	   �  !        %     =     B  '   H  !   p  #   �  #   �     �     �     �                1     R     g  
   n  q  y  ?   �  d   +  1   �  k   �     .     G     X     w  @   �  0   �     �       [      P   |  F   �  H   	     ]	  %   w	     �	  #   �	  S   �	  A   (
  <   j
     �
     �
        	                                                                             
                                        Alarm sound filename Ask to begin each set (all timers) Ask to begin each timer Auto close progress bar display when all sets end Configuration Duration Each timer end Finished. Interface to Sysmonitor Indicator Multiple Timer settings Name Never Number of times to run set (all timers) Pop-up message when each set ends Pop-up message when each timer ends Progress Bar update every x seconds Ready to start SUSPEND COMPUTER Seconds Seconds!Minutes Sound alarm when each set ends Sound alarm when each timer ends Timer duration units Timers has ended. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Tatar (https://www.transifex.com/antix-linux-community-contributions/teams/120110/tt/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: tt
Plural-Forms: nplurals=1; plural=0;
 Сигнализация тавыш файлының исеме Setәрбер комплектны башларга кушыгыз (барлык таймерлар) Eachәр таймерны башларга куш Барлык комплектлар беткәч, прогресс сызыгын автоматик ябу Конфигурация Озынлыгы Eachәр таймер бетә Тәмамланды. Сисмонитор күрсәткеченә интерфейс Берничә Таймер көйләүләре Исем Беркайчан да Комплектны эшләтеп җибәрү саны (барлык таймерлар) Setәрбер комплект беткәч калкып чыккан хәбәр Timәр таймер беткәч калкып чыккан хәбәр Прогресс сызыгы һәр x секундта яңартыла Башларга әзер Компьютерны туктату Секундлар Секундлар!Минутлар Setәрбер комплект беткәч тавыш сигнализациясе Таймер беткәч тавыш сигнализациясе Таймерның озынлыгы берәмлекләре Таймерлар бетте. 