��          �      l      �  (   �  '   
  7   2  '   j     �  G   �     �     �                    ,     5     >     G     S     Z  $   z     �     �  |  �  (   -  '   V  �   ~  '        <  G   O     �     �  .   �  =   �  .   #     R     h     �     �  !   �  w   �  �   L     �  !   �     	                                                                                         
        \t<b>$alarm_set_to_text $hr:$mn</b>      \t<b>$alarm_text $hr:$mn...</b> \n    Are you sure to set the alarm for that time tomorrow? $alarm_set_to_text $hr:$mn
$cancel_text $are_you_sure_text $cancel_alarm_text !bash -c 'cancel_quit_tray $XPID $target'!gtk-cancel $confirm_title Alarm Alarm set to Are you sure??? Cancel Alarm Duration Hour::CB Interval Minutes::CB Repeat To cancel, right-click for menu To cancel, right-click icon for menu infinite seconds Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Malayalam (https://www.transifex.com/antix-linux-community-contributions/teams/120110/ml/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: ml
Plural-Forms: nplurals=2; plural=(n != 1);
     \t<b>$alarm_set_to_text $hr:$mn</b>      \t<b>$alarm_text $hr:$mn...</b> \n    നാളെ ആ സമയത്തേക്ക് അലാറം സജ്ജീകരിക്കുമെന്ന് ഉറപ്പാണോ? $alarm_set_to_text $hr:$mn
$cancel_text $are_you_sure_text $cancel_alarm_text !bash -c 'cancel_quit_tray $XPID $target'!gtk-cancel $confirm_title അലാറം അലാറം സജ്ജമാക്കി നിങ്ങൾക്ക് ഉറപ്പാണോ??? അലാറം റദ്ദാക്കുക ദൈർഘ്യം മണിക്കൂർ::CB ഇടവേള മിനിറ്റ്::CB ആവർത്തിച്ച് റദ്ദാക്കാൻ, മെനുവിൽ റൈറ്റ് ക്ലിക്ക് ചെയ്യുക റദ്ദാക്കാൻ, മെനുവിനുള്ള ഐക്കണിൽ വലത് ക്ലിക്ക് ചെയ്യുക അനന്തമായ സെക്കന്റുകൾ 