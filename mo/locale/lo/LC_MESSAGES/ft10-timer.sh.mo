��          �   %   �      P     Q  "   f     �  1   �     �     �     �  	   �  !        %     =     B  '   H  !   p  #   �  #   �     �     �     �                1     R     g  
   n  o  y  '   �  x     N   �  �   �     �     �  0   �     �  J     Q   Y  	   �     �  �   �  o   �	  �   
  Z   �
  6     $   H     m     �  ]   �  ~   �  T   }  '   �  (   �        	                                                                             
                                        Alarm sound filename Ask to begin each set (all timers) Ask to begin each timer Auto close progress bar display when all sets end Configuration Duration Each timer end Finished. Interface to Sysmonitor Indicator Multiple Timer settings Name Never Number of times to run set (all timers) Pop-up message when each set ends Pop-up message when each timer ends Progress Bar update every x seconds Ready to start SUSPEND COMPUTER Seconds Seconds!Minutes Sound alarm when each set ends Sound alarm when each timer ends Timer duration units Timers has ended. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Lao (https://www.transifex.com/antix-linux-community-contributions/teams/120110/lo/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: lo
Plural-Forms: nplurals=1; plural=0;
 ຊື່ໄຟລ໌ສຽງປຸກ ຂໍໃຫ້ເລີ່ມຕົ້ນແຕ່ລະຊຸດ (ຕົວຈັບເວລາທັງຫມົດ) ຂໍໃຫ້ເລີ່ມຈັບເວລາແຕ່ລະຄັ້ງ ປິດອັດຕະໂນມັດສະແດງແຖບຄວາມຄືບຫນ້າເມື່ອຊຸດທັງຫມົດສິ້ນສຸດລົງ ການຕັ້ງຄ່າ ໄລຍະເວລາ ແຕ່ລະເວລາສິ້ນສຸດ ສຳເລັດແລ້ວ. ການໂຕ້ຕອບກັບຕົວຊີ້ວັດ Sysmonitor ການຕັ້ງຄ່າໂມງຈັບເວລາຫຼາຍອັນ ຊື່ ບໍ່ເຄີຍ ຈໍາu200bນວນu200bເວu200bລາu200bທີ່u200bຈະu200bດໍາu200bເນີນu200bການu200bກໍາu200bນົດ (ຕົວu200bຈັບu200bເວu200bລາu200bທັງu200bຫມົດu200b) ຂໍ້ຄວາມປັອບອັບເມື່ອແຕ່ລະຊຸດສິ້ນສຸດລົງ ຂໍ້ຄວາມປັອບອັບເມື່ອເຄື່ອງຈັບເວລາແຕ່ລະອັນສິ້ນສຸດລົງ ແຖບຄວາມຄືບໜ້າອັບເດດທຸກໆ x ວິນາທີ ພ້ອມທີ່ຈະເລີ່ມຕົ້ນ ຢຸດຄອມພິວເຕີ ວິນາທີ ວິນາທີ!ນາທີ ສຽງເຕືອນເມື່ອແຕ່ລະຊຸດສິ້ນສຸດລົງ ສຽງປຸກເມື່ອເຄື່ອງຈັບເວລາແຕ່ລະອັນສິ້ນສຸດລົງ ຫົວໜ່ວຍໄລຍະເວລາຂອງໂມງຈັບເວລາ ເຄື່ອງຈັບເວລາ ໄດ້ສິ້ນສຸດລົງ. 