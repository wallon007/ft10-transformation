��    (      \  5   �      p     q     }     �     �  
   �     �     �     �     �     �  ,   �     �                      F        f  
   k     v     �     �     �     �     �     �     �     �     �     �     �                    &     ,     3     ;     D  |  P     �     �     �     �     �            
        $     *  A   3     u     �  	   �     �     �  �   �     *     0     <     W     ^     f     m     s     �     �     �     �     �  	   �     �     �     �     	     	     (	     4	     @	                       
          	   $                                                     &   (       !          %          "                             #                    '        Accessories All Audio Browser Calculator Clock Control Centre Development E-mail Exit Favorite Apps: (click here to edit the list) File Manager Files Games Graphics Internet Left click- Add,move,remove Toolbar icons; Right click- Manage Toolbar Menu Multimedia Network manager - Connman News Office Pics Recent Search for programs and files Settings Show Desktop Stop App System Task switcher Terminal Text Text Editor Unplug USB drives Video Volume Weather Weather  Web Browser Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Hungarian (https://www.transifex.com/antix-linux-community-contributions/teams/120110/hu/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: hu
Plural-Forms: nplurals=2; plural=(n != 1);
 kiegészítők Minden Hang Böngésző Számológép Óra Vezérlőközpont Fejlődés Email Kijárat Kedvenc alkalmazások: (a lista szerkesztéséhez kattintson ide) Fájl kezelő Fájlok Játékok Grafika Internet Bal kattintás – Eszköztár ikonok hozzáadása, áthelyezése, eltávolítása; Kattintson jobb gombbal - Eszköztár kezelése Menü Multimédia Hálózatkezelő - Connman hírek Hivatal Képek Friss Programok és fájlok keresése Beállítások Asztal mutatása Stop App Rendszer Feladatváltó Terminál Szöveg Szöveg szerkesztő Húzza ki az USB-meghajtókat Videó Hangerő Időjárás Időjárás Böngésző 