��          T      �       �      �      �      �   
   �   k        p  �  �     F     \     x     �  x   �                                             $add_tz_text!add:FBTN $delete_tz_text!remove:FBTN ADD Time zone FT10-clock Insert Time Zone to be added to the World Clock \n (Enter nothing to search trough all available timezones) REMOVE Time zone Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Bosnian (https://www.transifex.com/antix-linux-community-contributions/teams/120110/bs/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: bs
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
 $add_tz_text!add:FBTN $delete_tz_text!remove:FBTN DODAJ vremensku zonu FT10-sat Unesite vremensku zonu koju želite dodati Svjetskom satu \n (Unesite ništa da pretražite sve dostupne vremenske zone) UKLONI vremensku zonu 