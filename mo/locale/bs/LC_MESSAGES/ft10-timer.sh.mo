��          �   %   �      P     Q  "   f     �  1   �     �     �     �  	   �  !        %     =     B  '   H  !   p  #   �  #   �     �     �     �                1     R     g  
   n  �  y     >  7   Z  "   �  G   �     �            
   (  !   3     U     l     p  4   v  +   �  .   �  +        2     F     W     _  '   n  *   �     �     �     �        	                                                                             
                                        Alarm sound filename Ask to begin each set (all timers) Ask to begin each timer Auto close progress bar display when all sets end Configuration Duration Each timer end Finished. Interface to Sysmonitor Indicator Multiple Timer settings Name Never Number of times to run set (all timers) Pop-up message when each set ends Pop-up message when each timer ends Progress Bar update every x seconds Ready to start SUSPEND COMPUTER Seconds Seconds!Minutes Sound alarm when each set ends Sound alarm when each timer ends Timer duration units Timers has ended. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Bosnian (https://www.transifex.com/antix-linux-community-contributions/teams/120110/bs/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: bs
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
 Naziv datoteke zvuka alarma Zamolite da započnete svaki set (svi mjerači vremena) Zatražite početak svakog tajmera Automatsko zatvaranje prikaza trake napretka kada se svi setovi završe Konfiguracija Trajanje Kraj svakog tajmera Završeno. Interfejs za Sysmonitor indikator Više postavki tajmera Ime Nikad Broj pokretanja je postavljen (svi mjerači vremena) Iskačuća poruka kada se svaki set završi Iskačuća poruka kada se svaki tajmer završi Traka napretka se ažurira svakih x sekundi Spremni za početak SUSPEND COMPUTER Sekunde Sekunde!Minute Zvučni alarm kada se završi svaki set Zvučni alarm kada se svaki tajmer završi Jedinice trajanja tajmera Tajmeri je završio. 