��          �   %   �      P     Q  "   f     �  1   �     �     �     �  	   �  !        %     =     B  '   H  !   p  #   �  #   �     �     �     �                1     R     g  
   n    y      �  <   �  &   �  J   
     U     b     o     �  "   �     �     �     �  1   �  7     6   D  +   {     �     �     �     �  2   �  1   	     E	     d	     k	        	                                                                             
                                        Alarm sound filename Ask to begin each set (all timers) Ask to begin each timer Auto close progress bar display when all sets end Configuration Duration Each timer end Finished. Interface to Sysmonitor Indicator Multiple Timer settings Name Never Number of times to run set (all timers) Pop-up message when each set ends Pop-up message when each timer ends Progress Bar update every x seconds Ready to start SUSPEND COMPUTER Seconds Seconds!Minutes Sound alarm when each set ends Sound alarm when each timer ends Timer duration units Timers has ended. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Polish (https://www.transifex.com/antix-linux-community-contributions/teams/120110/pl/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: pl
Plural-Forms: nplurals=4; plural=(n==1 ? 0 : (n%10>=2 && n%10<=4) && (n%100<12 || n%100>14) ? 1 : n!=1 && (n%10>=0 && n%10<=1) || (n%10>=5 && n%10<=9) || (n%100>=12 && n%100<=14) ? 2 : 3);
 Nazwa pliku dźwiękowego alarmu Poproś o rozpoczęcie każdego zestawu (wszystkie liczniki) Poproś o rozpoczęcie każdego timera Automatyczne zamykanie paska postępu po zakończeniu wszystkich zestawów Konfiguracja Czas trwania Każdy czas się kończy Skończone. Interfejs do wskaźnika Sysmonitor Wiele ustawień timera Nazwa Nigdy Ustawiona liczba uruchomień (wszystkie liczniki) Komunikat wyskakujący po zakończeniu każdego zestawu Komunikat wyskakujący po zakończeniu każdego timera Pasek postępu aktualizuje się co x sekund Gotowy do startu ZAWIESZ KOMPUTER sekundy sekundy!Minuty Alarm dźwiękowy po zakończeniu każdego zestawu Alarm dźwiękowy po zakończeniu każdego timera Jednostki czasu trwania timera Zegary dobiegło końca. 