��    (      \  5   �      p     q     }     �     �  
   �     �     �     �     �     �  ,   �     �                      F        f  
   k     v     �     �     �     �     �     �     �     �     �     �     �                    &     ,     3     ;     D    P  	   \  	   f     p     v  
   �     �     �     �     �     �  9   �     �                      �   '     �  
   �     �     �     �     �     �     �  
   	     #	     1	     F	     M	     b	     k	     q	     	     �	     �	     �	     �	     �	                       
          	   $                                                     &   (       !          %          "                             #                    '        Accessories All Audio Browser Calculator Clock Control Centre Development E-mail Exit Favorite Apps: (click here to edit the list) File Manager Files Games Graphics Internet Left click- Add,move,remove Toolbar icons; Right click- Manage Toolbar Menu Multimedia Network manager - Connman News Office Pics Recent Search for programs and files Settings Show Desktop Stop App System Task switcher Terminal Text Text Editor Unplug USB drives Video Volume Weather Weather  Web Browser Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Polish (https://www.transifex.com/antix-linux-community-contributions/teams/120110/pl/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: pl
Plural-Forms: nplurals=4; plural=(n==1 ? 0 : (n%10>=2 && n%10<=4) && (n%100<12 || n%100>14) ? 1 : n!=1 && (n%10>=0 && n%10<=1) || (n%10>=5 && n%10<=9) || (n%100>=12 && n%100<=14) ? 2 : 3);
 Akcesoria Wszystkie Audio Przeglądarka Kalkulator Zegar Centrum sterowania Rozwój E-mail Wyjście Ulubione aplikacje: (kliknij tutaj, aby edytować listę) Menedżer plików Pliki Gry Grafika Internet Lewy przycisk myszy - Dodaj, przenieś, usuń ikony paska narzędzi; Kliknij prawym przyciskiem myszy — Zarządzaj paskiem narzędzi Menu Multimedia Menedżer sieci - Connman Aktualności Gabinet Zdjęcia Ostatni Wyszukaj programy i pliki Ustawienia Pokaż pulpit Zatrzymaj aplikację System Przełącznik zadań Terminal Tekst Edytor tekstu Odłącz dyski USB Wideo Tom Pogoda Pogoda Przeglądarka internetowa 