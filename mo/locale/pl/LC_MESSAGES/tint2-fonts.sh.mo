��    
      l      �       �      �           .  	   ;     E     Z     l     x     �    �  ,   �  4   �       	   #     -     G     Z     h     �                   	                      
    CPU and RAM indicator's Font CPU and RAM usage indicator:btn Clock's Font Clock:btn Configure FT10 fonts Date in clock:btn Date's Font Menu and toolbar:btn Toolbar and Menu's Font Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:39+0000
Last-Translator: Robin, 2022
Language-Team: Polish (https://www.transifex.com/antix-linux-community-contributions/teams/120110/pl/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: pl
Plural-Forms: nplurals=4; plural=(n==1 ? 0 : (n%10>=2 && n%10<=4) && (n%100<12 || n%100>14) ? 1 : n!=1 && (n%10>=0 && n%10<=1) || (n%10>=5 && n%10<=9) || (n%100>=12 && n%100<=14) ? 2 : 3);
 Czcionka wskaźnika procesora i pamięci RAM Wskaźnik wykorzystania procesora i pamięci RAM:btn Czcionka zegara Zegar:btn Skonfiguruj czcionki FT10 Data w zegarze:btn Czcionka daty Menu i pasek narzędzi:btn Pasek narzędzi i czcionka menu 