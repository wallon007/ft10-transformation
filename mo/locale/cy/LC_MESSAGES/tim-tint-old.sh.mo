��    
      l      �       �      �   (     .   ,  0   [     �     �  )   �     �     �  �       �  -   �  9   �  B   .     q      �  A   �     �                              	                
    ADD ICON!add:FBTN Choose application to add to the Toolbar Double click any Application to move its icon: Double click any Application to remove its icon: Icon located! MOVE ICON!gtk-go-back-rtl:FBTN No icon located, using default Gears icon REMOVE ICON!remove:FBTN Tint2 icons Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:39+0000
Last-Translator: Robin, 2022
Language-Team: Welsh (https://www.transifex.com/antix-linux-community-contributions/teams/120110/cy/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: cy
Plural-Forms: nplurals=4; plural=(n==1) ? 0 : (n==2) ? 1 : (n != 8 && n != 11) ? 2 : 3;
 YCHWANEGU EICON!add:FBTN Dewiswch raglen i'w hychwanegu at y Bar Offer Cliciwch ddwywaith ar unrhyw Gymhwysiad i symud ei eicon: Cliciwch ddwywaith ar unrhyw Gymhwysiad i gael gwared ar ei eicon: Eicon wedi'i leoli! SYMUD EICON!gtk-go-back-rtl:FBTN Dim eicon wedi'i leoli, gan ddefnyddio'r eicon Gears rhagosodedig TYNNU'R EICON!remove:FBTN Eiconau Tint2 