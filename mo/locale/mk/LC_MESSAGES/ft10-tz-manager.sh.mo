��          T      �       �      �      �      �   
   �   k        p  �  �          3  &   O     v  �   �  ,   v                                        $add_tz_text!add:FBTN $delete_tz_text!remove:FBTN ADD Time zone FT10-clock Insert Time Zone to be added to the World Clock \n (Enter nothing to search trough all available timezones) REMOVE Time zone Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Macedonian (https://www.transifex.com/antix-linux-community-contributions/teams/120110/mk/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: mk
Plural-Forms: nplurals=2; plural=(n % 10 == 1 && n % 100 != 11) ? 0 : 1;
 $add_tz_text!add:FBTN $delete_tz_text!remove:FBTN ДОДАЈ временска зона FT10-часовник Вметнете временска зона за да се додаде во Светскиот часовник \n (Внесете ништо за пребарување низ сите достапни временски зони) ОТстрани временска зона 