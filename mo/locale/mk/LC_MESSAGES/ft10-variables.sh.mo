��    (      \  5   �      p     q     }     �     �  
   �     �     �     �     �     �  ,   �     �                      F        f  
   k     v     �     �     �     �     �     �     �     �     �     �     �                    &     ,     3     ;     D  �  P     �     �  
             (     =     N     n     {     �  k   �  $        +     <     E     T  �   e     8	     A	  %   X	  
   ~	     �	  
   �	     �	  9   �	     �	  .   
     <
     Z
  &   g
     �
  
   �
      �
  .   �
  
   �
               #     2                       
          	   $                                                     &   (       !          %          "                             #                    '        Accessories All Audio Browser Calculator Clock Control Centre Development E-mail Exit Favorite Apps: (click here to edit the list) File Manager Files Games Graphics Internet Left click- Add,move,remove Toolbar icons; Right click- Manage Toolbar Menu Multimedia Network manager - Connman News Office Pics Recent Search for programs and files Settings Show Desktop Stop App System Task switcher Terminal Text Text Editor Unplug USB drives Video Volume Weather Weather  Web Browser Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Macedonian (https://www.transifex.com/antix-linux-community-contributions/teams/120110/mk/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: mk
Plural-Forms: nplurals=2; plural=(n % 10 == 1 && n % 100 != 11) ? 0 : 1;
 Додатоци Сите Аудио Прелистувач Калкулатор Часовник Контролен центар Развој Е-пошта Излезете Омилени апликации: (кликнете овде за да ја уредите листата) Менаџер на датотеки Датотеки Игри Графика Интернет Лев клик - Додај, премести, отстранувај ги иконите на лентата со алатки; Десен клик - Управување со лентата со алатки Мени Мултимедија Мрежен менаџер - Connman Вести Канцеларија Слики Неодамнешни Пребарајте програми и датотеки Поставки Прикажи работна површина Стоп апликација Систем Преминувач на задачи Терминал Текст Уредувач на текст Исклучете ги USB-дисковите Видео Волумен Времето Времето Веб прелистувач 