��          �   %   �      P     Q  "   f     �  1   �     �     �     �  	   �  !        %     =     B  '   H  !   p  #   �  #   �     �     �     �                1     R     g  
   n  r  y  )   �  b     @   y  �   �     W     p  '   �     �  6   �  3   �           '  d   7  M   �  M   �  _   8	     �	  '   �	     �	     �	  O   
  O   U
  0   �
     �
     �
        	                                                                             
                                        Alarm sound filename Ask to begin each set (all timers) Ask to begin each timer Auto close progress bar display when all sets end Configuration Duration Each timer end Finished. Interface to Sysmonitor Indicator Multiple Timer settings Name Never Number of times to run set (all timers) Pop-up message when each set ends Pop-up message when each timer ends Progress Bar update every x seconds Ready to start SUSPEND COMPUTER Seconds Seconds!Minutes Sound alarm when each set ends Sound alarm when each timer ends Timer duration units Timers has ended. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Kyrgyz (https://www.transifex.com/antix-linux-community-contributions/teams/120110/ky/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: ky
Plural-Forms: nplurals=1; plural=0;
 Сигнал үнүнүн файл аты Ар бир топтомду баштоону сураныңыз (бардык таймерлер) Ар бир таймерди баштоону сураныңыз Бардык топтомдор аяктаганда автоматтык түрдө жабылган прогресс тилкесинин дисплейи Конфигурация Узактыгы Ар бир таймер аяктайт Бүттү. Sysmonitor индикаторуна интерфейс Бир нече таймер орнотуулары аты Эч качан Иштетүү үчүн канча жолу белгиленген (бардык таймерлер) Ар бир топтом аяктаганда калкыма билдирүү Ар бир таймер аяктаганда калкыма билдирүү Прогресс тилкеси ар бир x секунд сайын жаңырып турат Баштоого даяр КОМПЬЮТЕРДИ ТОКТОТУУ секунд секунд!Мүнөттөр Ар бир топтом аяктаганда үн сигнализациясы Ар бир таймер аяктаганда үн сигнализациясы Таймер узактык бирдиктери Таймерлер аяктады. 