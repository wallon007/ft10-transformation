��          �      l      �  (   �  '   
  7   2  '   j     �  G   �     �     �                    ,     5     >     G     S     Z  $   z     �     �  r  �  (   #  '   L  M   t  '   �     �  G   �     E     T     e     �  .   �     �     �     �     �       f   !  {   �               	                                                                                         
        \t<b>$alarm_set_to_text $hr:$mn</b>      \t<b>$alarm_text $hr:$mn...</b> \n    Are you sure to set the alarm for that time tomorrow? $alarm_set_to_text $hr:$mn
$cancel_text $are_you_sure_text $cancel_alarm_text !bash -c 'cancel_quit_tray $XPID $target'!gtk-cancel $confirm_title Alarm Alarm set to Are you sure??? Cancel Alarm Duration Hour::CB Interval Minutes::CB Repeat To cancel, right-click for menu To cancel, right-click icon for menu infinite seconds Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Kyrgyz (https://www.transifex.com/antix-linux-community-contributions/teams/120110/ky/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: ky
Plural-Forms: nplurals=1; plural=0;
     \t<b>$alarm_set_to_text $hr:$mn</b>      \t<b>$alarm_text $hr:$mn...</b> \n    Ойготкучту эртең ошол убакытка коесузбу? $alarm_set_to_text $hr:$mn
$cancel_text $are_you_sure_text $cancel_alarm_text !bash -c 'cancel_quit_tray $XPID $target'!gtk-cancel $confirm_title Ойготкуч Ойготкуч коюлган Ишенесиңби??? Ойготкучту жокко чыгаруу Узактыгы Саат::CB Интервал Мүнөттөр::CB Кайталоо Жокко чыгаруу үчүн, менюну оң баскыч менен чыкылдатыңыз Жокко чыгаруу үчүн, менюнун сөлөкөтүн оң баскыч менен чыкылдатыңыз чексиз секунд 