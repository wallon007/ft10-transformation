��          �      l      �  (   �  '   
  7   2  '   j     �  G   �     �     �                    ,     5     >     G     S     Z  $   z     �     �  {  �  (   ,  '   U  _   }  '   �       G        `     o  &   ~     �  !   �     �  
   �               *  M   7  `   �     �     �     	                                                                                         
        \t<b>$alarm_set_to_text $hr:$mn</b>      \t<b>$alarm_text $hr:$mn...</b> \n    Are you sure to set the alarm for that time tomorrow? $alarm_set_to_text $hr:$mn
$cancel_text $are_you_sure_text $cancel_alarm_text !bash -c 'cancel_quit_tray $XPID $target'!gtk-cancel $confirm_title Alarm Alarm set to Are you sure??? Cancel Alarm Duration Hour::CB Interval Minutes::CB Repeat To cancel, right-click for menu To cancel, right-click icon for menu infinite seconds Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Armenian (https://www.transifex.com/antix-linux-community-contributions/teams/120110/hy/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: hy
Plural-Forms: nplurals=2; plural=(n != 1);
     \t<b>$alarm_set_to_text $hr:$mn</b>      \t<b>$alarm_text $hr:$mn...</b> \n    Վստա՞հ եք, որ վաղն այդ ժամի զարթուցիչը կկարգավորեք: $alarm_set_to_text $hr:$mn
$cancel_text $are_you_sure_text $cancel_alarm_text !bash -c 'cancel_quit_tray $XPID $target'!gtk-cancel $confirm_title Ահազանգ Զարթուցիչը միացված է Համոզված ես??? Չեղարկել ահազանգը Տեւողությունը Ժամ::CB Ինտերվալ Րոպեներ::CB Կրկնել Չեղարկելու համար մենյուի համար սեղմեք աջը Չեղարկելու համար մենյուի համար աջ սեղմեք պատկերակին անսահման վայրկյան 