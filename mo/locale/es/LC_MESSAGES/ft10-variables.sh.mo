��    (      \  5   �      p     q     }     �     �  
   �     �     �     �     �     �  ,   �     �                      F        f  
   k     v     �     �     �     �     �     �     �     �     �     �     �                    &     ,     3     ;     D  z  P  
   �     �     �  	   �     �     �     �  
             .  >   5     t     �     �  	   �     �  |   �     .  
   4     ?     X     a     i     o     x     �     �     �     �     �     �     �     �     �     	     !	     )	     /	     5	                       
          	   $                                                     &   (       !          %          "                             #                    '        Accessories All Audio Browser Calculator Clock Control Centre Development E-mail Exit Favorite Apps: (click here to edit the list) File Manager Files Games Graphics Internet Left click- Add,move,remove Toolbar icons; Right click- Manage Toolbar Menu Multimedia Network manager - Connman News Office Pics Recent Search for programs and files Settings Show Desktop Stop App System Task switcher Terminal Text Text Editor Unplug USB drives Video Volume Weather Weather  Web Browser Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Spanish (https://www.transifex.com/antix-linux-community-contributions/teams/120110/es/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: es
Plural-Forms: nplurals=2; plural=(n != 1);
 Accesorios Todo Audio Navegador Calculadora Reloj Centro de control Desarrollo Correo electrónico Salida Aplicaciones favoritas: (haga clic aquí para editar la lista) Administrador de archivos archivos Juegos Gráficos Internet Clic izquierdo: agregar, mover, eliminar iconos de la barra de herramientas; Clic derecho- Administrar barra de herramientas Menú Multimedia Gerente de red - Connman Noticias Oficina Fotos Reciente Buscar programas y archivos Ajustes Mostrar escritorio Detener aplicación Sistema Conmutador de tareas Terminal Texto Editor de texto Desconecte las unidades USB Video Volumen Clima Clima Navegador web 