��          �   %   �      P     Q  "   f     �  1   �     �     �     �  	   �  !        %     =     B  '   H  !   p  #   �  #   �     �     �     �                1     R     g  
   n    y  H   ~  Q   �  C     �   ]     �  "   	  &   ,     S  5   k  2   �     �     �  J   �  h   5	  j   �	  J   	
     T
  %   l
     �
     �
  M   �
  ^     6   j     �     �        	                                                                             
                                        Alarm sound filename Ask to begin each set (all timers) Ask to begin each timer Auto close progress bar display when all sets end Configuration Duration Each timer end Finished. Interface to Sysmonitor Indicator Multiple Timer settings Name Never Number of times to run set (all timers) Pop-up message when each set ends Pop-up message when each timer ends Progress Bar update every x seconds Ready to start SUSPEND COMPUTER Seconds Seconds!Minutes Sound alarm when each set ends Sound alarm when each timer ends Timer duration units Timers has ended. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Russian (https://www.transifex.com/antix-linux-community-contributions/teams/120110/ru/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: ru
Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || (n%100>=11 && n%100<=14)? 2 : 3);
 Имя файла звукового сигнала будильника Попросите начать каждый подход (все таймеры) Спросите, чтобы начать каждый таймер Автоматическое закрытие индикатора выполнения, когда все наборы заканчиваются Конфигурация Продолжительность Каждый конец таймера Законченный. Интерфейс к индикатору Sysmonitor Несколько настроек таймера Имя Никогда Количество запусков набора (все таймеры) Всплывающее сообщение, когда заканчивается каждый набор Всплывающее сообщение, когда заканчивается каждый таймер Прогресс-бар обновляется каждые x секунд Готов начать ПОДВЕСИТЬ КОМПЬЮТЕР Секунды Секунды!Минуты Звуковой сигнал по окончании каждого сета Звуковой сигнал, когда каждый таймер заканчивается Единицы длительности таймера Таймеры закончился. 