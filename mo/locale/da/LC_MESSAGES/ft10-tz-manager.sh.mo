��          T      �       �      �      �      �   
   �   k        p  y  �     �          -     >  x   F     �                                        $add_tz_text!add:FBTN $delete_tz_text!remove:FBTN ADD Time zone FT10-clock Insert Time Zone to be added to the World Clock \n (Enter nothing to search trough all available timezones) REMOVE Time zone Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Danish (https://www.transifex.com/antix-linux-community-contributions/teams/120110/da/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: da
Plural-Forms: nplurals=2; plural=(n != 1);
 $add_tz_text!add:FBTN $delete_tz_text!remove:FBTN TILFØJ Tidszone FT10-ur Indsæt tidszone, der skal tilføjes til verdensuret \n (Indtast intet for at søge gennem alle tilgængelige tidszoner) FJERN Tidszone 