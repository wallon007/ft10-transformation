��    
      l      �       �      �   (     .   ,  0   [     �     �  )   �     �     �  y       �  :   �  3   �  3        :      I  -   j     �     �                         	                
    ADD ICON!add:FBTN Choose application to add to the Toolbar Double click any Application to move its icon: Double click any Application to remove its icon: Icon located! MOVE ICON!gtk-go-back-rtl:FBTN No icon located, using default Gears icon REMOVE ICON!remove:FBTN Tint2 icons Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:39+0000
Last-Translator: Robin, 2022
Language-Team: Danish (https://www.transifex.com/antix-linux-community-contributions/teams/120110/da/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: da
Plural-Forms: nplurals=2; plural=(n != 1);
 TILFØJ IKON!add:FBTN Vælg det program, der skal tilføjes til værktøjslinjen Dobbeltklik på et program for at flytte dets ikon: Dobbeltklik på et program for at fjerne dets ikon: Ikon placeret! FLYT IKONET!gtk-go-back-rtl:FBTN Intet ikon fundet, bruger standard Gears-ikon FJERN IKONET!remove:FBTN Tint2 ikoner 