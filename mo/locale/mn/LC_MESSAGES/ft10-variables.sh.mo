��    (      \  5   �      p     q     }     �     �  
   �     �     �     �     �     �  ,   �     �                      F        f  
   k     v     �     �     �     �     �     �     �     �     �     �     �                    &     ,     3     ;     D  |  P  !   �     �  
   �  
             #     *     B     O  
   [  b   f     �     �     �            �   #     �     �  +   	  
   ?	  
   J	  
   U	     `	  !   m	     �	  :   �	     �	     �	  '   
     0
  
   A
     L
  #   l
  
   �
     �
     �
     �
     �
                       
          	   $                                                     &   (       !          %          "                             #                    '        Accessories All Audio Browser Calculator Clock Control Centre Development E-mail Exit Favorite Apps: (click here to edit the list) File Manager Files Games Graphics Internet Left click- Add,move,remove Toolbar icons; Right click- Manage Toolbar Menu Multimedia Network manager - Connman News Office Pics Recent Search for programs and files Settings Show Desktop Stop App System Task switcher Terminal Text Text Editor Unplug USB drives Video Volume Weather Weather  Web Browser Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Mongolian (https://www.transifex.com/antix-linux-community-contributions/teams/120110/mn/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: mn
Plural-Forms: nplurals=2; plural=(n != 1);
 Дагалдах хэрэгсэл Бүгд Аудио Хөтөч Тооцоологч Цаг Хяналтын төв Хөгжил И-мэйл гарах Дуртай програмууд: (жагсаалтыг засах бол энд дарна уу) Файл менежер Файлууд Тоглоомууд График Интернет Зүүн товчлуур - Хэрэгслийн самбарын дүрс нэмэх, зөөх, устгах; Баруун товчийг дарна уу - Хэрэгслийн самбарыг удирдах Цэс Мультимедиа Сүлжээний менежер - Connman Мэдээ Оффис Зураг Саяхан Програм, файл хайх Тохиргоо Ширээний компьютерийг харуулах Програмыг зогсоо Систем Даалгавар шилжүүлэгч Терминал Текст Текст засварлагч USB хөтчүүдийг салга Видео Эзлэхүүн Цаг агаар Цаг агаар Вэб хөтөч 