��          �   %   �      P     Q  "   f     �  1   �     �     �     �  	   �  !        %     =     B  '   H  !   p  #   �  #   �     �     �     �                1     R     g  
   n  |  y  3   �  I   *  7   t  q   �       !   /  (   Q     z  4   �  &   �     �     �  F     B   K  O   �  G   �  $   &	  +   K	     w	     �	  =   �	  J   �	  ;   %
     a
     w
        	                                                                             
                                        Alarm sound filename Ask to begin each set (all timers) Ask to begin each timer Auto close progress bar display when all sets end Configuration Duration Each timer end Finished. Interface to Sysmonitor Indicator Multiple Timer settings Name Never Number of times to run set (all timers) Pop-up message when each set ends Pop-up message when each timer ends Progress Bar update every x seconds Ready to start SUSPEND COMPUTER Seconds Seconds!Minutes Sound alarm when each set ends Sound alarm when each timer ends Timer duration units Timers has ended. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Mongolian (https://www.transifex.com/antix-linux-community-contributions/teams/120110/mn/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: mn
Plural-Forms: nplurals=2; plural=(n != 1);
 Сэрүүлгийн дууны файлын нэр Багц бүрийг эхлүүлэхийг хүс (бүх таймер) Таймер бүрийг эхлүүлэхийг хүс Бүх багц дуусмагц автоматаар хаагдах явцын самбарыг харуулна Тохиргоо Үргэлжлэх хугацаа Таймер бүрийн төгсгөл Дууслаа. Sysmonitor үзүүлэлтийн интерфейс Олон таймер тохиргоо Нэр Хэзээ ч үгүй Тогтоосон ажиллах хугацаа (бүх таймер) Багц бүр дуусах үед гарч ирэх мессеж Цаг хэмжигч бүр дуусах үед гарч ирэх мессеж Явцын мөрийг х секунд тутамд шинэчилнэ Эхлэхэд бэлэн байна КОМПЬЮТЕРИЙГ ТОДОРХОЙЛ Секунд Секунд!Минут Багц бүр дуусах үед дуут дохиолол Цаг хэмжигч бүр дуусах үед дуут дохиолол Таймер үргэлжлэх хугацааны нэгж Цаг хэмжигч дууслаа. 