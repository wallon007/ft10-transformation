��          �      l      �  (   �  '   
  7   2  '   j     �  G   �     �     �                    ,     5     >     G     S     Z  $   z     �     �  |  �  (   -  '   V  i   ~  '   �       G   #     k     z  +   �  (   �  !   �  !     
   $     /     @     O  b   X  s   �     /     D     	                                                                                         
        \t<b>$alarm_set_to_text $hr:$mn</b>      \t<b>$alarm_text $hr:$mn...</b> \n    Are you sure to set the alarm for that time tomorrow? $alarm_set_to_text $hr:$mn
$cancel_text $are_you_sure_text $cancel_alarm_text !bash -c 'cancel_quit_tray $XPID $target'!gtk-cancel $confirm_title Alarm Alarm set to Are you sure??? Cancel Alarm Duration Hour::CB Interval Minutes::CB Repeat To cancel, right-click for menu To cancel, right-click icon for menu infinite seconds Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Mongolian (https://www.transifex.com/antix-linux-community-contributions/teams/120110/mn/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: mn
Plural-Forms: nplurals=2; plural=(n != 1);
     \t<b>$alarm_set_to_text $hr:$mn</b>      \t<b>$alarm_text $hr:$mn...</b> \n    Та маргааш тэр цагт сэрүүлэг тавихдаа итгэлтэй байна уу? $alarm_set_to_text $hr:$mn
$cancel_text $are_you_sure_text $cancel_alarm_text !bash -c 'cancel_quit_tray $XPID $target'!gtk-cancel $confirm_title Сэрүүлэг Сэрүүлгийг тохируулсан Чи итгэлтэй байна уу??? Сэрүүлгийг цуцлах Үргэлжлэх хугацаа Цаг::CB Интервал Минут::CB Давт Цуцлахын тулд цэс рүү хулганы баруун товчийг дарна уу Цуцлахын тулд цэсийн дүрс дээр хулганы баруун товчийг дарна уу хязгааргүй секунд 