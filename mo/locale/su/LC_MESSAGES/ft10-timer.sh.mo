��          �   %   �      P     Q  "   f     �  1   �     �     �     �  	   �  !        %     =     B  '   H  !   p  #   �  #   �     �     �     �                1     R     g  
   n  u  y     �  5   	  %   ?  ?   e     �     �     �  	   �  $   �     �            .   *  '   Y  ,   �  *   �     �     �     �       %     *   3     ^     r     x        	                                                                             
                                        Alarm sound filename Ask to begin each set (all timers) Ask to begin each timer Auto close progress bar display when all sets end Configuration Duration Each timer end Finished. Interface to Sysmonitor Indicator Multiple Timer settings Name Never Number of times to run set (all timers) Pop-up message when each set ends Pop-up message when each timer ends Progress Bar update every x seconds Ready to start SUSPEND COMPUTER Seconds Seconds!Minutes Sound alarm when each set ends Sound alarm when each timer ends Timer duration units Timers has ended. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Sundanese (https://www.transifex.com/antix-linux-community-contributions/teams/120110/su/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: su
Plural-Forms: nplurals=1; plural=0;
 Ngaran koropak sora alarm Tanya pikeun ngamimitian unggal set (sadayana timers) Tanya pikeun ngamimitian unggal timer Tutup otomatis tampilan bar kamajuan nalika sadaya set réngsé Konfigurasi Lilana Unggal tungtung timer Réngsé. Panganteur kana Sysmonitor Indikator Sababaraha setélan Timer Ngaran moal pernah Jumlah kali ngajalankeun set (sadayana timers) Pesen pop-up nalika unggal set réngsé Pesen pop-up sawaktos unggal waktos réngsé Ngamutahirkeun Progress Bar unggal x detik Siap ngamimitian TANGGUH KOMPUTER Detik Detik!Menit Sora alarm nalika unggal set réngsé Sora alarem sawaktos unggal timer réngsé Hijian durasi timer Timer geus réngsé. 