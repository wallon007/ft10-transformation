��          �   %   �      P     Q  "   f     �  1   �     �     �     �  	   �  !        %     =     B  '   H  !   p  #   �  #   �     �     �     �                1     R     g  
   n  y  y  9   �  �   -  e   �  �   #             5   3     i  A   �  C   �     	     	  �   2	  n   �	  q   C
  �   �
  V   @  R   �     �  .      y   /  |   �  ;   &     b     x        	                                                                             
                                        Alarm sound filename Ask to begin each set (all timers) Ask to begin each timer Auto close progress bar display when all sets end Configuration Duration Each timer end Finished. Interface to Sysmonitor Indicator Multiple Timer settings Name Never Number of times to run set (all timers) Pop-up message when each set ends Pop-up message when each timer ends Progress Bar update every x seconds Ready to start SUSPEND COMPUTER Seconds Seconds!Minutes Sound alarm when each set ends Sound alarm when each timer ends Timer duration units Timers has ended. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Telugu (https://www.transifex.com/antix-linux-community-contributions/teams/120110/te/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: te
Plural-Forms: nplurals=2; plural=(n != 1);
 అలారం సౌండ్ ఫైల్ పేరు ప్రతి సెట్u200cను ప్రారంభించమని అడగండి (అన్ని టైమర్u200cలు) ప్రతి టైమర్u200cను ప్రారంభించమని అడగండి అన్ని సెట్u200cలు ముగిసినప్పుడు ప్రోగ్రెస్ బార్ డిస్u200cప్లేను స్వయంచాలకంగా మూసివేయండి ఆకృతీకరణ వ్యవధి ప్రతి టైమర్ ముగింపు పూర్తయింది. Sysmonitor సూచికకు ఇంటర్u200cఫేస్ బహుళ టైమర్ సెట్టింగ్u200cలు పేరు ఎప్పుడూ అమలు చేయడానికి ఎన్ని సార్లు సెట్ చేయబడింది (అన్ని టైమర్u200cలు) ప్రతి సెట్ ముగిసినప్పుడు పాప్-అప్ సందేశం ప్రతి టైమర్ ముగిసినప్పుడు పాప్-అప్ సందేశం ప్రతి x సెకన్లకు ప్రోగ్రెస్ బార్ అప్u200cడేట్ అవుతుంది ప్రారంభించడానికి సిద్ధంగా ఉంది కంప్యూటర్u200cను సస్పెండ్ చేయండి సెకన్లు సెకన్లు!నిమిషాలు ప్రతి సెట్ ముగిసినప్పుడు అలారం ధ్వనిస్తుంది ప్రతి టైమర్ ముగిసినప్పుడు అలారం ధ్వనిస్తుంది టైమర్ వ్యవధి యూనిట్లు టైమర్లు ముగిసింది. 