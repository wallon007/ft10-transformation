��          �   %   �      P     Q  "   f     �  1   �     �     �     �  	   �  !        %     =     B  '   H  !   p  #   �  #   �     �     �     �                1     R     g  
   n  x  y     �  0        8  G   X     �     �     �     �  #   �     �            0   !  (   R  )   {  *   �     �     �     �        )     (   <     e     z     �        	                                                                             
                                        Alarm sound filename Ask to begin each set (all timers) Ask to begin each timer Auto close progress bar display when all sets end Configuration Duration Each timer end Finished. Interface to Sysmonitor Indicator Multiple Timer settings Name Never Number of times to run set (all timers) Pop-up message when each set ends Pop-up message when each timer ends Progress Bar update every x seconds Ready to start SUSPEND COMPUTER Seconds Seconds!Minutes Sound alarm when each set ends Sound alarm when each timer ends Timer duration units Timers has ended. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Shona (https://www.transifex.com/antix-linux-community-contributions/teams/120110/sn/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: sn
Plural-Forms: nplurals=2; plural=(n != 1);
 Alarm sound filename Kumbira kuti utange seti yega yega (ese madhiri) Kumbira kutanga nguva yega yega Otomatiki kuvhara kufambira mberi kwebhara kuratidza kana ese ese apera Configuration Duration Imwe neimwe nguva inopera Yapera. Interface kune Sysmonitor Indicator Multiple Timer marongero Zita Never Nhamba yenguva dzekumhanya seti (dzose dzenguva) Pop-up meseji kana seti yega yega yapera Pop-up meseji kana nguva yega yega yapera Progress Bar inogadziridza ese x masekonzi Wakagadzirira kutanga SIMIRIRA COMPUTER Sekondi Sekondi!Maminitsi Kurira alarm kana imwe neimwe seti yapera Kurira alarm kana nguva yega yega yapera Timer duration units Timers yapera. 