��    (      \  5   �      p     q     }     �     �  
   �     �     �     �     �     �  ,   �     �                      F        f  
   k     v     �     �     �     �     �     �     �     �     �     �     �                    &     ,     3     ;     D  y  P  %   �  	   �     �               7     D     a  %   q     �  �   �     ?     Y     m     �     �  �   �  	   `	      j	  #   �	     �	     �	     �	     �	  D   �	     /
  "   E
     h
     q
     �
     �
  	   �
  "   �
  K   �
     '  	   7     A     V  (   k                       
          	   $                                                     &   (       !          %          "                             #                    '        Accessories All Audio Browser Calculator Clock Control Centre Development E-mail Exit Favorite Apps: (click here to edit the list) File Manager Files Games Graphics Internet Left click- Add,move,remove Toolbar icons; Right click- Manage Toolbar Menu Multimedia Network manager - Connman News Office Pics Recent Search for programs and files Settings Show Desktop Stop App System Task switcher Terminal Text Text Editor Unplug USB drives Video Volume Weather Weather  Web Browser Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Persian (https://www.transifex.com/antix-linux-community-contributions/teams/120110/fa/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: fa
Plural-Forms: nplurals=2; plural=(n > 1);
 ﯽﺒﻧﺎﺟ ﺕﺍﺰﯿﻬﺠﺗ ﻪﻤﻫ ﯽﻌﻤﺳ ﺮﮔﺭﻭﺮﻣ ﺏﺎﺴﺣ ﻦﯿﺷﺎﻣ ﺖﻋﺎﺳ ﻝﺮﺘﻨﮐ ﺰﮐﺮﻣ ﻪﻌﺳﻮﺗ ﮏﯿﻧﻭﺮﺘﮑﻟﺍ ﺖﺴﭘ ﺝﻭﺮﺧ (ﺪﯿﻨﮐ ﮏﯿﻠﮐ ﺍﺭ ﺎﺠﻨﯾﺍ ﺖﺴﯿﻟ ﺶﯾﺍﺮﯾﻭ ﯼﺍﺮﺑ) :ﻪﻗﻼ﻿ﻋ ﺩﺭﻮﻣ ﯼﺎﻫ ﻪﻣﺎﻧﺮﺑ ﻞﯾﺎﻓ ﺮﯾﺪﻣ ﺎﻫ ﻞﯾﺎﻓ ﺎﻫ ﯼﺯﺎﺑ ﮏﯿﻓﺍﺮﮔ ﺖﻧﺮﺘﻨﯾﺍ Manage Toolbar - ﺪﯿﻨﮐ ﺖﺳﺍﺭ ﮏﯿﻠﮐ .ﺭﺍﺰﺑﺍ ﺭﺍﻮﻧ ﯼﺎﻫﺩﺎﻤﻧ ﻑﺬﺣ ،ﻝﺎﻘﺘﻧﺍ ،ﻥﺩﻭﺰﻓﺍ - ﭗﭼ ﮏﯿﻠﮐ ﻮﻨﻣ ﯼﺍ ﻪﻧﺎﺳﺭ ﺪﻨﭼ Connman - ﻪﮑﺒﺷ ﺮﯾﺪﻣ ﺭﺎﺒﺧﺍ ﺮﺘﻓﺩ ﺎﻫ ﺲﮑﻋ ﺮﯿﺧﺍ ﺎﻫ ﻞﯾﺎﻓ ﻭ ﺎﻫ ﻪﻣﺎﻧﺮﺑ ﯼﻮﺠﺘﺴﺟ ﺕﺎﻤﯿﻈﻨﺗ ﭖﺎﺘﮑﺳﺩ ﺶﯾﺎﻤﻧ Stop App ﻢﺘﺴﯿﺳ ﺭﺎﮐ ﺾﯾﻮﻌﺗ ﻪﻧﺎﯾﺎﭘ ﻦﺘﻣ ﻦﺘﻣ ﺮﮕﺸﯾﺍﺮﯾﻭ ﺪﯿﻨﮐ ﺍﺪﺟ ﻕﺮﺑ ﺯﺍ ﺍﺭ USB ﯼﺎﻫﻮﯾﺍﺭﺩ ﻮﯾﺪﯾﻭ ﺪﻠﺟ ﺍﻮﻫ ﻭ ﺏﺁ ﺍﻮﻫ ﻭ ﺏﺁ ﺖﻧﺮﺘﻨﯾﺍ ﺮﮔﺭﻭﺮﻣ 