��          �   %   �      P     Q  "   f     �  1   �     �     �     �  	   �  !        %     =     B  '   H  !   p  #   �  #   �     �     �     �                1     R     g  
   n  �  y  "   =  /   `     �  F   �     �               *  $   0     U     q     v  ;   {  '   �  )   �  )   	     3     C     V     ^  '   m  )   �     �     �     �        	                                                                             
                                        Alarm sound filename Ask to begin each set (all timers) Ask to begin each timer Auto close progress bar display when all sets end Configuration Duration Each timer end Finished. Interface to Sysmonitor Indicator Multiple Timer settings Name Never Number of times to run set (all timers) Pop-up message when each set ends Pop-up message when each timer ends Progress Bar update every x seconds Ready to start SUSPEND COMPUTER Seconds Seconds!Minutes Sound alarm when each set ends Sound alarm when each timer ends Timer duration units Timers has ended. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Maltese (https://www.transifex.com/antix-linux-community-contributions/teams/120110/mt/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: mt
Plural-Forms: nplurals=4; plural=(n==1 ? 0 : n==0 || ( n%100>1 && n%100<11) ? 1 : (n%100>10 && n%100<20 ) ? 2 : 3);
 Isem tal-fajl tal-ħoss tal-allarm Staqsi biex tibda kull sett (it-tajmers kollha) Staqsi biex tibda kull tajmer Wirja awtomatika tal-bar tal-progress meta jintemmu s-settijiet kollha Konfigurazzjoni Tul ta' żmien Kull tmiem tat-tajmer Lest. Interface għal Sysmonitor Indikatur Settings ta' Timer multipli Isem Qatt Numru ta' drabi biex taħdem issettjata (it-tajmers kollha) Messaġġ pop-up meta jintemm kull sett Messaġġ pop-up meta jintemm kull tajmer Progress Bar aġġornament kull x sekondi Lest biex tibda SOSPENDI KOMPJUTER Sekondi Sekondi!Minuti Allarm tal-ħoss meta jintemm kull sett Allarm tal-ħoss meta jintemm kull tajmer Unitajiet ta' tul ta' timer Timers intemm. 