��    (      \  5   �      p     q     }     �     �  
   �     �     �     �     �     �  ,   �     �                      F        f  
   k     v     �     �     �     �     �     �     �     �     �     �     �                    &     ,     3     ;     D  �  P          !     (     .  
   6     A     J     _     h     o  4   v     �     �     �     �     �  k   �     C  
   H     S     r  
   ~     �     �     �     �     �     �     �     �     �     �     �     	     	     $	     *	     /	     4	                       
          	   $                                                     &   (       !          %          "                             #                    '        Accessories All Audio Browser Calculator Clock Control Centre Development E-mail Exit Favorite Apps: (click here to edit the list) File Manager Files Games Graphics Internet Left click- Add,move,remove Toolbar icons; Right click- Manage Toolbar Menu Multimedia Network manager - Connman News Office Pics Recent Search for programs and files Settings Show Desktop Stop App System Task switcher Terminal Text Text Editor Unplug USB drives Video Volume Weather Weather  Web Browser Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Maltese (https://www.transifex.com/antix-linux-community-contributions/teams/120110/mt/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: mt
Plural-Forms: nplurals=4; plural=(n==1 ? 0 : n==0 || ( n%100>1 && n%100<11) ? 1 : (n%100>10 && n%100<20 ) ? 2 : 3);
 Aċċessorji Kollha Awdjo Browser Kalkulatur Arloġġ Ċentru ta' Kontroll Żvilupp E-mail Ħruġ Apps favoriti: (ikklikkja hawn biex teditja l-lista) File Manager Fajls Logħob Grafika Internet Ikklikkja tax-xellug - Żid, ċċaqlaq, neħħi l-ikoni tal-Toolbar; Ikklikkja dritt - Immaniġġja Toolbar Menu Multimedia Maniġer tan-netwerk - Connman Aħbarijiet Uffiċċju Pics Riċenti Fittex għal programmi u fajls Settings Uri Desktop Waqqaf l-App Sistema Task switcher Terminal Test Editur tat-test Unplug USB drives Video Volum Temp Temp Web Browser 