��    
      l      �       �      �   (     .   ,  0   [     �     �  )   �     �     �  x    .   �  `   �  �     �   �  +   .  >   Z  �   �  3        O                         	                
    ADD ICON!add:FBTN Choose application to add to the Toolbar Double click any Application to move its icon: Double click any Application to remove its icon: Icon located! MOVE ICON!gtk-go-back-rtl:FBTN No icon located, using default Gears icon REMOVE ICON!remove:FBTN Tint2 icons Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:39+0000
Last-Translator: Robin, 2022
Language-Team: Greek (https://www.transifex.com/antix-linux-community-contributions/teams/120110/el/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: el
Plural-Forms: nplurals=2; plural=(n != 1);
 ΠΡΟΣΘΗΚΗ ΕΙΚΟΝΙΔΙΟΥ!add:FBTN Επιλέξτε εφαρμογή για προσθήκη στη γραμμή εργαλείων Κάντε διπλό κλικ σε οποιαδήποτε εφαρμογή για να μετακινήσετε το εικονίδιό της: Κάντε διπλό κλικ σε οποιαδήποτε εφαρμογή για να αφαιρέσετε το εικονίδιό της: Το εικονίδιο βρίσκεται! ΕΙΚΟΝΙΔΙΟ ΜΕΤΑΚΙΝΗΣΗΣ!gtk-go-back-rtl:FBTN Δεν υπάρχει εικονίδιο, χρησιμοποιώντας το προεπιλεγμένο εικονίδιο Gears ΚΑΤΑΡΓΗΣΗ ΕΙΚΟΝΙΔΙΟΥ!remove:FBTN Tint2 εικονίδια 