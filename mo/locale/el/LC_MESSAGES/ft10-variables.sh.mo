��    (      \  5   �      p     q     }     �     �  
   �     �     �     �     �     �  ,   �     �                      F        f  
   k     v     �     �     �     �     �     �     �     �     �     �     �                    &     ,     3     ;     D  x  P     �     �     �  '   �       
   +     6     R  )   c     �  ~   �  #        =     J     ]     l  �     
   G	     R	  -   c	     �	     �	     �	     �	  A   �	     
  5   &
  !   \
     ~
  !   �
     �
     �
  )   �
  0   �
     ,     9     O     \     i                       
          	   $                                                     &   (       !          %          "                             #                    '        Accessories All Audio Browser Calculator Clock Control Centre Development E-mail Exit Favorite Apps: (click here to edit the list) File Manager Files Games Graphics Internet Left click- Add,move,remove Toolbar icons; Right click- Manage Toolbar Menu Multimedia Network manager - Connman News Office Pics Recent Search for programs and files Settings Show Desktop Stop App System Task switcher Terminal Text Text Editor Unplug USB drives Video Volume Weather Weather  Web Browser Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Greek (https://www.transifex.com/antix-linux-community-contributions/teams/120110/el/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: el
Plural-Forms: nplurals=2; plural=(n != 1);
 αξεσουάρ Ολα Ήχος Πρόγραμμα περιήγησης Αριθμομηχανή Ρολόι Κέντρο Ελέγχου Ανάπτυξη ΗΛΕΚΤΡΟΝΙΚΗ ΔΙΕΥΘΥΝΣΗ Εξοδος Αγαπημένες εφαρμογές: (κάντε κλικ εδώ για να επεξεργαστείτε τη λίστα) Διαχείριση αρχείων Αρχεία Παιχνίδια Γραφικά Διαδίκτυο Αριστερό κλικ- Προσθήκη, μετακίνηση, αφαίρεση εικονιδίων Toolbar. Κάντε δεξί κλικ - Διαχείριση γραμμής εργαλείων Μενού ΠΟΛΥΜΕΣΑ Διευθυντής δικτύου - Connman Νέα Γραφείο Φωτογραφίες Πρόσφατος Αναζήτηση προγραμμάτων και αρχείων Ρυθμίσεις Δείξε την επιφάνεια εργασίας Διακοπή εφαρμογής Σύστημα Εναλλαγή εργασιών Τερματικό Κείμενο Επεξεργαστής κειμένου Αποσυνδέστε τις μονάδες USB βίντεο Ενταση ΗΧΟΥ Καιρός Καιρός Φυλλομετρητής 