��    (      \  5   �      p     q     }     �     �  
   �     �     �     �     �     �  ,   �     �                      F        f  
   k     v     �     �     �     �     �     �     �     �     �     �     �                    &     ,     3     ;     D  �  P          +     4     9     F     R     Y     k     r     {  6   �     �     �     �     �     �  �   �     n     �     �     �  
   �     �  	   �     �  
   �     �     	     	     	  	   1	     ;	     @	     P	     b	     h	     n	     w	     �	                       
          	   $                                                     &   (       !          %          "                             #                    '        Accessories All Audio Browser Calculator Clock Control Centre Development E-mail Exit Favorite Apps: (click here to edit the list) File Manager Files Games Graphics Internet Left click- Add,move,remove Toolbar icons; Right click- Manage Toolbar Menu Multimedia Network manager - Connman News Office Pics Recent Search for programs and files Settings Show Desktop Stop App System Task switcher Terminal Text Text Editor Unplug USB drives Video Volume Weather Weather  Web Browser Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Czech (https://www.transifex.com/antix-linux-community-contributions/teams/120110/cs/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: cs
Plural-Forms: nplurals=4; plural=(n == 1 && n % 1 == 0) ? 0 : (n >= 2 && n <= 4 && n % 1 == 0) ? 1: (n % 1 != 0 ) ? 2 : 3;
 Příslušenství Všechno Zvuk Prohlížeč Kalkulačka Hodiny Řídicí centrum Rozvoj E-mailem Výstup Oblíbené aplikace: (kliknutím sem upravíte seznam) Správce souborů Soubory Hry Grafika Internet Levé kliknutí – Přidat, přesunout, odebrat ikony lišty nástrojů; Klikněte pravým tlačítkem - Spravovat panel nástrojů Jídelní lístek Multimédia Správce sítě - Connman Zprávy Kancelář obrázky Nedávné Vyhledejte programy a soubory Nastavení Zobrazit plochu Zastavit aplikaci Systém Přepínač úloh Terminál Text Textový editor Odpojte USB disky Video Objem Počasí Počasí Webový prohlížeč 