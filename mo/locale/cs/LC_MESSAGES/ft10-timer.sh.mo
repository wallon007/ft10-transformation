��          �   %   �      P     Q  "   f     �  1   �     �     �     �  	   �  !        %     =     B  '   H  !   p  #   �  #   �     �     �     �                1     R     g  
   n  �  y     B  9   b  .   �  Q   �          )     7     P  "   X  $   {     �     �  3   �  /   �  6     2   H     {     �     �     �  +   �  3   �     	  
   7	  	   B	        	                                                                             
                                        Alarm sound filename Ask to begin each set (all timers) Ask to begin each timer Auto close progress bar display when all sets end Configuration Duration Each timer end Finished. Interface to Sysmonitor Indicator Multiple Timer settings Name Never Number of times to run set (all timers) Pop-up message when each set ends Pop-up message when each timer ends Progress Bar update every x seconds Ready to start SUSPEND COMPUTER Seconds Seconds!Minutes Sound alarm when each set ends Sound alarm when each timer ends Timer duration units Timers has ended. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Czech (https://www.transifex.com/antix-linux-community-contributions/teams/120110/cs/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: cs
Plural-Forms: nplurals=4; plural=(n == 1 && n % 1 == 0) ? 0 : (n >= 2 && n <= 4 && n % 1 == 0) ? 1: (n % 1 != 0 ) ? 2 : 3;
 Název souboru se zvukem alarmu Požádat o zahájení každé sady (všechny časovače) Požádejte o spuštění každého časovače Automaticky zavřít zobrazení ukazatele průběhu, když skončí všechny sady Konfigurace Doba trvání Každý konec časovače Hotovo. Rozhraní k indikátoru Sysmonitor Vícenásobné nastavení časovače název Nikdy Nastavení počtu spuštění (všechny časovače) Vyskakovací zpráva po skončení každé sady Vyskakovací zpráva, když každý časovač skončí Aktualizace ukazatele průběhu každých x sekund Připraven začít POZASTAVIT POČÍTAČ Sekundy Sekundy!Minut Zvukový alarm při ukončení každé sady Zvukový alarm při ukončení každého časovače Jednotky trvání časovače Časovače skončil. 