#!/bin/bash
#GUI Script to  search for ip's of shared folders and then connect to the selected on, using zzzfm (may be adapted to any file manager)- By PPC, 8/3/2022, full GPL license
#Dependencies: smbclient
#PS: Fully GUI eay to create/manage Samba shared folders easily- the package "system-config-samba", available in the  Repository
#sudo service smbd restart ### makes sure that Samba is running on the server, so shared folders are available

#Clean up temporary files:
echo "" > /tmp/range_of_available_ips
echo "" > /tmp/ccc
echo "" >/tmp/result

#Connectivity check:
ip=$(hostname -I)
if [ -n "$ip" ]; then
echo "Connected to a network - this device's IP is $ip"
else
echo "Not connected to a network, exiting"
yad --center --window-icon="/usr/share/icons/gnome/48x48/places/gnome-mime-x-directory-smb-share.png" --title="antiX-shares" --text="Not connected to a network!" --button=" x " --width 250
exit
fi

#Draw pulsating "wait" window, while searching for shared folders:
while true; do
echo "#"
done | yad --center --window-icon="/usr/share/icons/gnome/48x48/places/gnome-mime-x-directory-smb-share.png" --title="antiX-shares" --text="" --width 250 --text-align center --on-top --pulsate --no-buttons --auto-close --progress &

##Main part of the script:
#Create a list of all available ips for network shares:
ips=$(ip -o addr | sed '/: lo /d'|sed '/::/d')
newString="${ips#*inet}"
ip_range=$(echo $newString|cut -d ' ' -f 1)
echo The detected available IP range is: $ip_range

###Check what ips are on:
nmap -v -sn $ip_range > /tmp/ips
cat /tmp/ips | sed '/host down/d'  |grep -E -o "([0-9]{1,3}[\.]){3}[0-9]{1,3}" > /tmp/range_of_available_ips
echo IPs that will be searched for shared folders:
cat /tmp/range_of_available_ips
echo
echo Scanning for shared folders on each IP...

#Remove empty lines from the result:
sed -i '/^[[:space:]]*$/d' /tmp/range_of_available_ips

#Test every possible network share ip, and log the valid addresses:
while read line; do
  smbclient -N -L $line | sed '/Printer Drivers/d' | grep "Disk" | cut -d' ' -f1  >/tmp/ccc
  sleep 0.2
  cat /tmp/ccc
  
  while read line2; do
 sleep 0.2
  echo //$line/$line2 >>/tmp/result
  done </tmp/ccc
  
done </tmp/range_of_available_ips

#Remove empty lines from the result:
sed -i '/^[[:space:]]*$/d' /tmp/result

#Close pulsating "wait" window:
wmctrl -c "antiX-shares"

#Exit if no network shares were detected:
check=$(cat /tmp/result)
if [ -n "$check" ]; then
 echo 
 echo "...Done scanning. Detected Network shares:"
 echo "$check"
else
 echo "No network shares detected, exiting"
 yad --center --window-icon="/usr/share/icons/gnome/48x48/places/gnome-mime-x-directory-smb-share.png" --title="antiX-shares" --text=$" No network shares detected! " --button=" x " --width 250
exit
fi

#Window that allows user to select the share to connect to:
selectedshare=$(yad --no-buttons --window-icon="/usr/share/icons/gnome/48x48/places/gnome-mime-x-directory-smb-share.png" --title="antiX-shares" --width=250 --height=250 --fixed --center --separator=" " --list  --column=""  < /tmp/result)

#If nothing was selected, exit:
 if [[ $selectedshare = "" ]]; then exit
 fi
 
share=$selectedshare
entry=$(yad --center --width=350 --window-icon="/usr/share/icons/gnome/48x48/places/gnome-mime-x-directory-smb-share.png" --title="$share" --form --field="<b><big><big> ☺ </big></big></b>" $USER --field=$"◦◦◦◦":H --button="OK")
username=$(echo $entry |cut -d\| -f1)
password=$(echo $entry |cut -d\| -f2)
echo user is $username
echo password is $password
udevil mount -t cifs -o username=$username,password=$password $share 


#Try to mount share in zzzfm:
zzzfm $share & sleep 1
