
# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-01-18 01:27+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: \n"

#: tint2_fonts.sh:11
msgid "Toolbar and Menu's Font"
msgstr ""

#: tint2_fonts.sh:26
msgid "CPU and RAM indicator's Font"
msgstr ""

#: tint2_fonts.sh:41
msgid "Clock's Font"
msgstr ""

#: tint2_fonts.sh:56
msgid "Date's Font"
msgstr ""

#: tint2_fonts.sh:69
msgid "Configure FT10 fonts"
msgstr ""

#: tint2_fonts.sh:69
msgid "Menu and toolbar:btn"
msgstr ""

#: tint2_fonts.sh:69
msgid "CPU and RAM usage indicator:btn"
msgstr ""

#: tint2_fonts.sh:69
msgid "Clock:btn"
msgstr ""

#: tint2_fonts.sh:69
msgid "Date in clock:btn"
msgstr ""

