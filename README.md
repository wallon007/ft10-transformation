# FT10-transformation

FT10 is an antiX Linux (extremely light and powerful Debian based distro) "Transformation Pack" that configures and uses a friendly and modern toolbar and menus (using tint2 panel and jgmenu).
If you install it from the package on antiX's repository (it's called ft10-transformation) OR install a .deb package available in this git page, to enable FT10 simply navigate to the "Preferences" antiX Menu entry and select the option to "Enable FT10".
You can return to your "default" desktop at any time, by selecting the menu entry to "Disable FT10".
 Currently FT10 is available in English, Portuguese, Brazillan Portuguese, French, Spanish, Italian and German (more localization files, probably machine translated) will eventually be added). 

Why the strange name? Hum... When I started developing this project, antiX Linux looked like something from the 00's. I was using Fluxbox Window Manager, that did not allow to create toolbar quick launchers, so my work around was using Tint2 toolbar. The "F" is from Fluxbox, the "T" is from "Tint2". It also has a double meaning, because it's meant to make antiX look "Finer Than 10".
  
IMPORTANT NOTE: If you have any problem with menu items (like reapeated entries), please reboot your computer. That may happen when you run jgmenu for the first time.

# What does it look like?

Because "a picture is worth a thousand words":

![](https://gitlab.com/antix-contribs/ft10-transformation/-/raw/main/FT10-tilled-menu-with-visual-effects.jpg) 
Here's FT10 toolbar and menu (in the older version of the "Tiled" layout), localized in Pt, with the older version of the Calendar open.


![](https://gitlab.com/antix-contribs/ft10-transformation/-/raw/main/screenshot-Categories_Menu2.jpg) 
Here's FT10 toolbar and menu (in the "Categories" layout), localized in Pt, with the latest version of Calendar open.  


![](https://gitlab.com/antix-contribs/ft10-transformation/-/raw/main/screenshot-Centered_Menu.jpg)
...And here's FT10 "Centered" menu, also localized in Pt. Please note the low RAM use, on the indicator on the right of the toolbar. That was not "photoshoped": you do get a modern looking OS that uses almost no system resources!

Here is video about how to install and enable FT10 in antiX 21 (also applies to any supported antiX version) (Many thanks for that BobC!!!): https://www.youtube.com/watch?v=ZOygVh6V17U

## FT10 is available in the official antiX repository 
 This means that if you are running antiX Linux (tested in "Full" versions 19.X - 23.X)
 you can install it from Synaptic package manager (search and then install "ft10-transformation") or via the terminal:
 ~~~
  sudo apt update && sudo apt install ft10-transformation
  ~~~
 The FT10 package and  all it's dependencies use about 3mb of disk space.
  Once you install FT10, go the antiX menu > Preferences > FT10- enable ... to instantly enable it.
  A window that allows you to select between the "Categories menu layout", the "Tiled menu layout", or (from v.2 on) the "Centered menu layout" will pop up and in a couple of seconds, you'll be using FT10

**Changes in v.2.beta1**
- Added the new "Centered" menu template
- Fixed some problems with the scripts that manage the tint2 toolbar
- Simplified the "Enable FT10" menu entry - now it no longer offers to install extra packages, just to manage some basic FT10 options and select the desired menu template
- Removed several scripts, that are now part of antiX 23 (so it makes no sense to have them replicated in FT10). The "Tiled menu" was adjusted accordingly
- Added the option (present on the previous 1.5beta) that allows to manage the way the time is displayed on the tint2 toolbar

**Changes in v.1.4.10**
– Added a script to configure zzzfm and add some extra features to it
– Added a GUI script that detects and mounts samba 2 shared drives in the default file manager
– Added a script to change Tiles from the Tiled Menu
– Fixed some bugs
– Improved localizations
– Fixed Deinstaller .desktop file
– Added kronometer and krename to the FT10 activation window (because the default stopwatch is extremely basic and antiX lacks a GUI application to bulk rename files)
– ...and many other changes

  **Changes in v.1.2**
– Fixed a few ft10-clock bugs
– Improved the look of the Tiled menu
– Included Cloud, Translate and Debinstaller (any dependencies that Cloud and Translate require will be installed, if the user so wishes at the first time these scripts are run).

## What does the current FT10 version include?

**Calendar** is a GUI calendar with basic agenda - it allows you to insert all day events and timed events (and set alarms for timed events)

**Clocks** includes a **GUI world clock**, **alarm**, **multi-timers** (that can suspend antiX after the defined time has elapsed) and a very basic **Stopwatch** (the "Enable FT10" window allows you to install a better looking GUI stopwatch called Kronometer )

**Translate** uses the trans command to display a localized version of any highlighted text.

**Weather** a simple script that displays the current weather and a 3 days weather forecast, for you current location, on your language, using wttr.in

**A GUI that allows easy configuration of the tint2 toolbar**

**A GUI that allows managing the Menu Favorite (or Pinned) entries and the "Tiles" (only available in the Tiled menu):**
FT10 menu (when using the Tiled menu) > Manage Favorite apps > Menu Tiles > “Change a Tile by selecting its number” > Select the tile number you want to replace (the first Tile is the one on the top left, the second one is to the right of the first, etc… there are 3 tiles per line, 7 lines) > Confirm the Tile you want to replace > Using the app-select window choose the application you want > Confirm it’s name (8-9 letters max).

**Maps** is a simple menu entry that launchs Openstreetmaps on your default browser, so you can easily access Maps, just like on any mobile device

[REMOVED in v.2, due to api restrictions]**News** displays news headlines without the need for a browser. You can select the source of the news from dozens of countries

[REMOVED in v.2]**A customized ZZZFM File Manager** - by default the "Enable FT10" window offers to install (if it's not already installed) and/or costumize zzzfm - adding a **Trash Can**, Recent Files, **My network** (to quickly access samba 2 shared folders), Cloud, and also the default bookmarks (Documents, Downloads, etc) and a more streamlined look and feel. It also adds buttons to switch betwen the detailed and compact views.

[REMOVED in v.2]**Cloud** mounts any cloud drive and uses your default file manager to access it.

[REMOVED in v.2]**Debinstaller** is meant to be a simple gdebi replacement, to allow for easy installation of .deb packages from a GUI file manager (associate .deb files with debinstaller in your file manager and you are set)

** **
** **

## Manually editing the "Tiles" in the "Tiled" menu layout (if you are into doing painfull stuff)**

– Open the config file tiles.csv, that’s located in your home folder, in the hidden ./config/jgmenu folder. The fastest way to edit it is: Menu > Terminal and enter this command:
geany ~/.config/jgmenu/tiles.csv
– You will see the config file’s content. There are 21 “Tiles”, divided by 7 lines, 3 Tiles per line. In the config file, each Tile is identified by a number. The first Tile is the one on the top left, the second one is the second Tile on the first line, etc…
There’s a comment line (starting with an # ) that identifies to which Tile the following lines refer to…
So the 2 lines after the line “#Tile1” refer to the very first icon.
The first line after that identifier, starting with “@icon” is the Tile itself, the second line, starting with “@text” is it’s subtitle.
On the line related to the “icon” the first field, that appears after the first comma, is the command associated to that Tile. The last filed, that appears after the last comma, it’s the Tile’s icon.
On the @text lines, the only interesting field is the last one, after the last comma- that’s the Tile subtitle. Do not use more than 9 characters here, or you risk “overwriting” the following Tile’s subtitle, or place it out of the menu.

– Where do you get the command and the icon and description of the app you want to place in a tile?
If you don’t already know all this info, there’s an easy way- using app-select… Menu > Terminal and run this command:
app-select --s
– Search for the app you want to place in a Tile and select it. The terminal window will display, divided by “|”, all the related app’s info:
desktop file (you can ignore this one) | Name of the app (place this in the Tile’s @text line last field, after the last comma)| command that executes the app (you’ll have to place this in the Tile’s @icon first field) then, the icon is the last field displayed on the terminal results (you’ll have to copy that to the Tile’s @icon last field)

Save the tiles.csv file and the changes will take place instantly, when you open FT10’s menu, no restart needed…

Warning 1: don’t forget to back up your tiles.csv file, before editing it!!! Even if you forget to back up the original file, it’s no big deal, see Warning 2, on how to restore it back to the original contents…
Warning 2: Also back up any edits you make to the tiles.csv file- if you run the FT10 configure script (that enables/toggles FT10’s menu layout) the tiles.csv file can be restored to it’s original state!!!

I know, this looks like a very complex procedure, but it really isn’t. Lets see an example.
Change the 7th icon (the “video” player Tile) to Celluloid:

– geany ~/.config/jgmenu/tiles.csv
– app-select –s > search for celluloid and then copy and paste the relevant field until you get this lines in the tiles.csv:

#Tile7

@icon,celluloid,630,141,52,52,2,left,top,auto,#000000 0,/usr/share/icons/papirus-antix/48×48/categories/io.github.celluloid_player.Celluloid.png

@text,,630,190,220,22,2,left,top,auto,#000000 0, Celluloid

Save the text file and you’r done! Check the Tiled menu- oops… For some reason, JGmenu can’t process the original icon…
So, let’s use the terminal and do a
search Celluloid.png

We have another icon available, let’s try using /usr/share/icons/papirus-antix/48×48/apps/io.github.Celluloid.png instead. Replace the icon on the Tile, and you’ll get this:

#Tile7

@icon,celluloid,630,141,52,52,2,left,top,auto,#000000 0,/usr/share/icons/papirus-antix/48×48/apps/io.github.Celluloid.png

@text,,630,190,220,22,2,left,top,auto,#000000 0, Celluloid

Check the menu… and it works!
Hum… probably not such an easy procedure after all, that’s why it took me so long to make a GUI for doing this… 🙂
Ok, ok, I took an extreme example, most app’s icons can be added directly to a Tile- probably JGmenu gets confused with the underscore on the original file icon’s name…

** **
** **

## Debian Packaging
This source has been modified for easy .deb package building.
Building has been tested on a debian buster distro (antiX 19.4 base).
Simple instructions below for personal use.

**0. Download source**

Download this source and unpack it (if not already) to a separate folder with easy access.

Example: unpack to `~/ft10-transformation/` folder.

**1. Install build dependencies**

ft10-transformation is mainly a collection of bash scripts. The needed dependencies are usually installed in most distros: `gettext, bash, coreutils, util-linux, sed`

Packaging prerequisites: `build-essential fakeroot devscripts debhelper`

One-liner:
~~~
sudo apt install build-essential fakeroot devscripts debhelper gettext bash coreutils util-linux sed
~~~

**2. Building .deb package**

The easy way is navigating to the folder that contains the source, open a terminal there, and run:
~~~
debuild -b -uc -us
~~~
It will build the .deb package for your architecture and save it "outside" the folder (one level above).

Example: it will create a `ft10-transformation_0.1.0_all.deb` file

As the program contains mainly bash scripts, it will work on all architectures that contain the dependencies needed for it to work.

**3. Install**

You can install using gdebi, your software-center program or from terminal. Example below:
~~~
sudo apt install /path/to/ft10-transformation_0.1.0_all.deb
~~~

**4. Dependencies**

ft10-transformations needs *yad* for the main dialogs, *skippy-xd* for the task/window selection view, *tint2* as the toolbar, and either *jgmenu* version 4.4.0 or greater (for searching inside folders contained in /usr/share/applications) or the already built and adapted to antiX linux [jgmenu-antix](https://gitlab.com/antix-contribs/jgmenu-antix).

Some launchers also point to antiX's unique desktop-session and desktop-defaults programs, which make it easier to set default browser, terminal, text editor, etc. But these are not required for this program to work. 

Once all dependencies are installed, you should be able to install ft10-transformation.

## How to use

Place the command `ft10-start` in your startup file, as a startup command
~~~
ft10-start &
~~~
and next time you start your linux session, it will configure jgmenu and tint2 with the custom layout brought by ft10-transformation.

You can restore the default configuration running the `ft10-create` command.

You can edit favorite applications in jgmenu launching the `jgmenu-editor.sh` command or from the menu itself.

The jgmenu layouts have been prepared to be able to adapt to the running gtk theme. Try it out running the jgmenu command `jgmenu_run gtktheme` (result not guaranteed)

## Language support

We have done as best as possible to support different languages, but we are limited in our knowledge. Contact the maintainer if you need support for your language.
