#!/bin/bash
#Yad script to switch between available workspaces (Virtual desktops) in a Window Manager, 13/4/2022 by PPC, originaly made for using with antiX Linux
#GPL license - use and modify as you want but please keep the above line 

available_options=$(wmctrl -d)
window_title="Workspaces"
window_icon="/usr/share/icons/papirus-antix/48x48/apps/workspace-switcher.png"

#Allow only one instance of the script running 
##(unused, but handy code adapted from https://stackoverflow.com/questions/1715137/what-is-the-best-way-to-ensure-only-one-instance-of-a-bash-script-is-running):
#[[ $(lsof -t $0| wc -l) > 1 ]] && exit
#If the application is already running, toggle it off:
scriptname=$(readlink -f $0)
for pid in $(pidof -x $scriptname); do
    if [ $pid != $$ ]; then
     wmctrl -c $window_title
        exit 1     
    fi 
done

#Show selection window:
selection=$( echo "$available_options" | yad --title="$window_title" --undecorated \
--text="" \
--borders=10 \
--listen \
--no-markup \
--width=500 \
--height=300 \
--center \
--window-icon="%window_icon" \
--list \
--column="":text \
--button="X":1 )

#Act upon selection:
workspace=$(echo $selection|cut -d' ' -f1) && wmctrl -s$workspace


