#!/bin/bash
# script to automaticly configure a trash can in zzzfm, by PPC

#Make really sure that zzzfm is not running
pkill -9 -e -f zzzfm
#back up original config file and move it out of the way, so we have a prestine configuration
 mv ~/.config/zzzfm  ~/.config/zzzfmBACKUP-FT10
 
#delete default zzzfm configurations
rm -rf ~/.config/zzzfm
 
 
##make sure zzzfm runs once and  creates a default config file
zzzfm & sleep 0.2 && pkill zzzfm


#Make sure that  trash-cli is installed or check if the latest version is installed)
#Exit if trash-cli is not installed
	if ! [ -x "$(command -v trash)" ]; then
  	 x-terminal-emulator -T $"FT10" -e /bin/bash -c "gksu 'apt install -y trash-cli' && yad --center --title=TrashCan-Installer --window-icon='/usr/share/icons/papirus-antix/32x32/apps/gnome-debian.png' --fixed --width=300 --height=100 --image=/usr/share/icons/numix-square-antix/32x32/actions/gtk-ok.png  --text='Package(s) installed! ' --button='x'|| yad --center --title=FT10 --window-icon='/usr/share/icons/papirus-antix/32x32/apps/gnome-debian.png'  --fixed --width=300 --height=100 --image=/usr/share/icons/papirus-antix/32x32/emblems/emblem-rabbitvcs-modified.png --text=' Error installing the package! \n  Please read the log on the terminal window below  ' --button='x'"
     exit 1
    fi
    
#Recheck- if trash-cli is not installed, exit    
    	if ! [ -x "$(command -v trash)" ]; then
  	     exit 1
       fi

#If trash-cli is found, proceed with configuration:
#Close zzzfm, so we can edit it's config file
 pkill zzzfm
#back up original config file
 cp ~/.config/zzzfm/session  ~/.config/zzzfm/sessionBACKUP-FT10
 
 # Create detailed_view_zzzfm.sh
cat > "$HOME/.config/zzzfm/detailed_view_zzzfm.sh" << 'DETAILED_TEMPLATE'
#!/bin/bash
#Detailed view
pid_zzzfm=$(ps -C zzzfm -o pid=)
pid_spacefm=$(ps -C spacefm -o pid=)

var=""

if [ -n "$pid_zzzfm" ]; then
    echo "zzzfm seems to be running"
    killall zzzfm 
    sleep 0.1
    killall spacefm
fi

if [ -n "$pid_spacefm" ]; then
    echo "spacefm seems to be running"
    pkill spacefm
fi


sed -i 's/panel1_list_detailed-b=.*/panel1_list_detailed-b=1/' ~/.config/zzzfm/session && sed -i 's/panel1_list_compact-b.*/panel1_list_compact-b=2/' ~/.config/zzzfm/session

zzzfm

DETAILED_TEMPLATE

# Create compact_view_zzzfm.sh
cat > "$HOME/.config/zzzfm/compact_view_zzzfm.sh" << 'COMPACT_TEMPLATE'
#!/bin/bash
#Compact view
pid_zzzfm=$(ps -C zzzfm -o pid=)
pid_spacefm=$(ps -C spacefm -o pid=)

var=""

if [ -n "$pid_zzzfm" ]; then
    echo "zzzfm seems to be running"
    killall zzzfm 
    sleep 0.1
    killall spacefm
fi

if [ -n "$pid_spacefm" ]; then
    echo "spacefm seems to be running"
    pkill spacefm
fi

sed -i 's/panel1_list_detailed-b=.*/panel1_list_detailed-b=2/' ~/.config/zzzfm/session && sed -i 's/panel1_list_compact-b.*/panel1_list_compact-b=1/' ~/.config/zzzfm/session

zzzfm

COMPACT_TEMPLATE

 # Create zzzfm-recent.sh
cat > "$HOME/.config/zzzfm/zzzfm-recent.sh" << 'RECENT_TEMPLATE'
#!/bin/bash
#obtain localization from a known source: geany
export TEXTDOMAIN=geany;  title=$(echo "$(gettext "Recent _Files")")
#remove any underscore
title=${title//_/}
#display window, using the localized title and also the correct window icon
eval "`zzzfm -g --title "$title" --window-icon chronometer --chooser --button close`" && xdg-open "$dialog_chooser1"

RECENT_TEMPLATE
 
 
 #Trash Label and Menu entry and localizations:
trash_label="Trash"
trash_menu_entry="Send to Trash" 
language=$(echo $LANG|cut -d. -f1)
if [ $language = "pt_PT" ]; then
 trash_label="Reciclagem"
 trash_menu_entry="Enviar para a Reciclagem" 
fi
if [ $language = "pt_BR" ]; then
 trash_label="Lixeira"
 trash_menu_entry="Mover para a Lixeira" 
fi 
  
#Make sure that the trash folder is created (or else users get an error if click the Trash folder before sending anything to trash):
if [ ! -d ~/.local/share/Trash/files ]; then
  mkdir -p ~/.local/share/Trash/files;
fi  
  
#make insertions in zzzfm config file:
 echo "cstm_25a291d5-y="  >>~/.config/zzzfm/session  
 echo cstm_25a291d5-key=65535 >>~/.config/zzzfm/session 
 echo "cstm_25a291d5-label=$trash_menu_entry" >>~/.config/zzzfm/session  
 echo cstm_25a291d5-icon=gtk-delete >>~/.config/zzzfm/session  
 echo cstm_25a291d5-cxt=3%%%%%0%%%%%2%%%%%2%%%%%.local/share/Trash/files >>~/.config/zzzfm/session
 #echo cstm_25a291d5-cxt=3%%%%%0%%%%%2%%%%%2%%%%%.local/share/Trash/files%%%%%2%%%%%11%%%%%aabbccddeeffgghhiijjkkllmmnnoopp  >>~/.config/zzzfm/session
 echo cstm_25a291d5-prev=edit_delete >>~/.config/zzzfm/session 
 echo 'cstm_25a291d5-line=folder=%d; if [[ ${folder} != *".local/share/Trash/files"* ]]; then trash %F; fi' >>~/.config/zzzfm/session  
 #echo cstm_25a291d5-task=1 >>~/.config/zzzfm/session  
 echo cstm_25a291d5-task_err=1 >>~/.config/zzzfm/session  
 echo cstm_25a291d5-task_out=1 >>~/.config/zzzfm/session  
 echo cstm_25a291d5-keep=1 >>~/.config/zzzfm/session  
 
#find out where the keybinding to Delete is, and then the line next to it:
 declare -i delete_key_line
 delete_key_line=$(grep -nr 'edit_delete-key=65535' ~/.config/zzzfm/session | cut -d: -f1)
 delete_key_line+=1
 #add this line in the line after the edit_delete_key:
 sed -i "`echo $delete_key_line`i\\edit_delete-keymod=1\\" ~/.config/zzzfm/session
 #find out what's initially next to the delete key:
  originally_next_to_delete=$(grep 'edit_delete-next=' ~/.config/zzzfm/session| cut -d= -f2)
 #now try make the contents of this line point to the Trash entry;
  sed -i 's/edit_delete-next=.*/edit_delete-next=cstm_25a291d5/' ~/.config/zzzfm/session 

#now tell that what comes next to Trash was what originally was after Delete 
 sed -i "s/cstm_25a291d5-next=.*/cstm_25a291d5-next='echo $originally_next_to_delete'/" ~/.config/zzzfm/session

#make sure that delete's icon is changed to an "X", to avoid confusion with Send to Trash
 sed -i "`echo $delete_key_line`i\\edit_delete-icn=gtk-close\\" ~/.config/zzzfm/session
 sed -i 's/edit_delete-icn=.*/edit_delete-icn=gtk-close/' ~/.config/zzzfm/session
 
# If the edit_delete-next line does not exist, insert it:
###TODO: check if the field exists in the config file: 
 echo edit_delete-next=cstm_25a291d5 >>~/.config/zzzfm/session
 
 ##########Insert bookmarks
previous_bookmark=$(grep -nr 'main_book-child=' ~/.config/zzzfm/session | cut -d= -f2)
 

 echo cstm_$previous_bookmark-next=cstm_2b7f3cb1  >>~/.config/zzzfm/session
 echo cstm_2b7f3cb1-next=cstm_0f7a0cea >>~/.config/zzzfm/session

#Recent files:
#Get label loclization, from geany's localization file
export TEXTDOMAIN=geany;  recents_label=$(echo "$(gettext "Recent _Files")")
#remove any underscore
recents_label=${recents_label//_/}

echo cstm_0f7a0cea-y= >>~/.config/zzzfm/session
echo cstm_0f7a0cea-label=$recents_label >>~/.config/zzzfm/session
echo cstm_0f7a0cea-icon=chronometer >>~/.config/zzzfm/session
echo cstm_0f7a0cea-next=cstm_6c99bdd2 >>~/.config/zzzfm/session
echo cstm_0f7a0cea-prev=cstm_2b7f3cb1 >>~/.config/zzzfm/session
echo cstm_0f7a0cea-line=bash $HOME/.config/zzzfm/zzzfm-recent.sh \n >>~/.config/zzzfm/session
echo cstm_0f7a0cea-task_err=1 >>~/.config/zzzfm/session
echo cstm_0f7a0cea-task_out=1 >>~/.config/zzzfm/session
echo cstm_0f7a0cea-keep=1 >>~/.config/zzzfm/session

 
#Downloads
downloads_label=$(echo $XDG_DOWNLOAD_DIR| cut -d/ -f4)
echo cstm_6c99bdd2-x=3 >>~/.config/zzzfm/session
echo "cstm_6c99bdd2-z=$XDG_DOWNLOAD_DIR" >>~/.config/zzzfm/session
echo "cstm_6c99bdd2-label=$downloads_label" >>~/.config/zzzfm/session
echo cstm_6c99bdd2-icon=/usr/share/icons/papirus-antix/48x48/actions/browser-download.png  >>~/.config/zzzfm/session
echo cstm_6c99bdd2-next=cstm_1f966a72  >>~/.config/zzzfm/session
echo cstm_6c99bdd2-prev=cstm_0f7a0cea >>~/.config/zzzfm/session

#Documents
documents_label=$(echo $XDG_DOCUMENTS_DIR| cut -d/ -f4)
echo cstm_1f966a72-x=3  >>~/.config/zzzfm/session
echo cstm_1f966a72-z=$XDG_DOCUMENTS_DIR >>~/.config/zzzfm/session
echo "cstm_1f966a72-label=$documents_label" >>~/.config/zzzfm/session
echo  cstm_1f966a72-icon=gtk-file >>~/.config/zzzfm/session
echo  cstm_1f966a72-next=cstm_23f8698b >>~/.config/zzzfm/session
echo cstm_1f966a72-prev=cstm_6c99bdd2 >>~/.config/zzzfm/session

#Pictures
documents_label=$(echo $XDG_PICTURES_DIR| cut -d/ -f4)
echo cstm_23f8698b-x=3 >>~/.config/zzzfm/session
echo cstm_23f8698b-z=$XDG_PICTURES_DIR >>~/.config/zzzfm/session
echo cstm_23f8698b-label=$documents_label >>~/.config/zzzfm/session
echo cstm_23f8698b-icon=viewimage >>~/.config/zzzfm/session
echo cstm_23f8698b-next=cstm_3be1d265 >>~/.config/zzzfm/session
echo cstm_23f8698b-prev=cstm_1f966a72 >>~/.config/zzzfm/session

#Videos
videos_label=$(echo $XDG_VIDEOS_DIR| cut -d/ -f4)
echo cstm_3be1d265-x=3 >>~/.config/zzzfm/session
echo cstm_3be1d265-z=$XDG_VIDEOS_DIR >>~/.config/zzzfm/session
echo cstm_3be1d265-label=$videos_label >>~/.config/zzzfm/session
echo cstm_3be1d265-icon=record >>~/.config/zzzfm/session
echo cstm_3be1d265-next=cstm_1fe75a69 >>~/.config/zzzfm/session
echo cstm_3be1d265-prev=cstm_23f8698b >>~/.config/zzzfm/session

#Music
music_label=$(echo $XDG_MUSIC_DIR| cut -d/ -f4)
echo cstm_1fe75a69-x=3 >>~/.config/zzzfm/session
echo cstm_1fe75a69-z=$XDG_MUSIC_DIR >>~/.config/zzzfm/session
echo cstm_1fe75a69-label=$music_label >>~/.config/zzzfm/session
echo cstm_1fe75a69-icon=/usr/share/icons/papirus-antix/48x48/actions/filename-filetype-amarok.png  >>~/.config/zzzfm/session
echo cstm_1fe75a69-prev=cstm_3be1d265 >>~/.config/zzzfm/session
echo cstm_1fe75a69-next=cstm_3ca29516 >>~/.config/zzzfm/session

#Trash 
 echo cstm_3ca29516-x=3 >>~/.config/zzzfm/session
 echo cstm_3ca29516-z=~/.local/share/Trash/files >>~/.config/zzzfm/session
 echo "cstm_3ca29516-label=$trash_label" >>~/.config/zzzfm/session
 echo cstm_3ca29516-icon=gtk-delete >>~/.config/zzzfm/session
 echo cstm_3ca29516-prev=cstm_1fe75a69 >>~/.config/zzzfm/session
 echo cstm_3ca29516-next=cstm_2e7ac554 >>~/.config/zzzfm/session

#Show access cloud drives icon:
echo cstm_2e7ac554-x=2 >>~/.config/zzzfm/session
echo cstm_2e7ac554-z=ft10-access-cloud.desktop >>~/.config/zzzfm/session
echo cstm_2e7ac554-prev=cstm_3ca29516 >>~/.config/zzzfm/session


#Don't show tabs if there's only one tab:
#find out where the entry [Interface] is and insert line right below it:
 declare -i interface_line
 interface_line=$(grep -nr '\[Interface\]' ~/.config/zzzfm/session | cut -d: -f1)
 interface_line+=1
 #add this line in the line after the interface_delete_key:
 sed -i "`echo $interface_line`i\\always_show_tabs=0\\" ~/.config/zzzfm/session


#show bookmarks:
echo panel1_show_book0-b=1 >>~/.config/zzzfm/session
sed -i 's/panel1_slider_positions0-y=.*/panel1_slider_positions0-y=178/' ~/.config/zzzfm/session
#Show devices:
echo panel1_show_devmon0-b=1 >>~/.config/zzzfm/session
#Hide hidden files:
echo panel1_show_hidden-b=2 >>~/.config/zzzfm/session
#Ctrl+H does not work????
echo panel1_show_hidden-key=104 >>~/.config/zzzfm/session
sed -i 's/panel1_show_hidden-key=.*/panel1_show_hidden-key=104/' ~/.config/zzzfm/session
#Show thumbnails???:
#####echo show_thumbnail=1  >>~/.config/zzzfm/session
sed -i 's/show_thumbnail=.*/show_thumbnail=1/' ~/.config/zzzfm/session

echo view_thumb-b=1  >>~/.config/zzzfm/session

#find out where the entry [General] is and insert line right below it:
 declare -i general_line
 general_line=$(grep -nr '\[General\]' ~/.config/zzzfm/session | cut -d: -f1)
 general_line+=1
 #add this line in the line after the edit_delete_key:
 sed -i "`echo $general_line`i\\show_thumbnail=1\\" ~/.config/zzzfm/session


#show big icons
echo  panel1_list_detailed-b=2  >>~/.config/zzzfm/session
echo  panel1_list_compact-b=1  >>~/.config/zzzfm/session
echo  panel1_list_large-b=1  >>~/.config/zzzfm/session
echo  panel1_list_large0-b=1  >>~/.config/zzzfm/session

#hide empty devices???
echo  dev_show_empty-b=1  >>~/.config/zzzfm/session
echo panel1_show_devmon-b=2  >>~/.config/zzzfm/session
echo panel1_show_dirtree-b=1  >>~/.config/zzzfm/session
echo panel1_show_book-b=2  >>~/.config/zzzfm/session

#hide Tree:
echo  main_dev-b=1 >>~/.config/zzzfm/session
sed -i 's/panel1_show_devmon-b=.*/panel1_show_devmon-b=2/' ~/.config/zzzfm/session
sed -i 's/panel1_show_dirtree-b=.*/panel1_show_dirtree-b=1/' ~/.config/zzzfm/session
sed -i 's/panel1_show_book-b=.*/panel1_show_book-b=2/' ~/.config/zzzfm/session
sed -i 's/panel1_show_dirtree0-b=.*/panel1_show_dirtree0-b=2/' ~/.config/zzzfm/session

#Use blue folder icon instead of zzzfm's default one:
echo main_icon-icn=folder-blue  >>~/.config/zzzfm/session

##Insert a Toogle Big icons button on the toolbar:
#find out where the  line that says: -tool=6 is, and then the line next to it:
tool6_line=$(grep -nr 'tool=6' ~/.config/zzzfm/session | cut -d: -f1)
tool6_id=$(sed -n -e "`echo $tool6_line`p" ~/.config/zzzfm/session| cut -d- -f1)
#Now just insert the needed lines, including the next and and prev, refering to the correct variables
echo $tool6_id-next=cstm_54d97f86 >>~/.config/zzzfm/session
echo cstm_54d97f86-tool=17 >>~/.config/zzzfm/session
echo cstm_54d97f86-prev=$tool6_id >>~/.config/zzzfm/session

#Also insert Details/Compact view buttons:
TEXTDOMAINDIR=/usr/share/locale
TEXTDOMAIN=zzzfm
detailed_text=$"_Detailed"
compact_text=$"_Compact"

echo cstm_54d97f86-next=cstm_248656b5 >>~/.config/zzzfm/session
echo cstm_248656b5-y= >>~/.config/zzzfm/session
echo cstm_248656b5-label=$detailed_text >>~/.config/zzzfm/session
echo cstm_248656b5-icon=/usr/share/icons/papirus-antix/48x48/actions/bookmark-new-list.png >>~/.config/zzzfm/session
echo cstm_248656b5-tool=1 >>~/.config/zzzfm/session
echo cstm_248656b5-prev=cstm_54d97f86 >>~/.config/zzzfm/session
echo cstm_248656b5-line=bash $HOME/.config/zzzfm/detailed_view_zzzfm.sh >>~/.config/zzzfm/session
echo cstm_248656b5-task=1 >>~/.config/zzzfm/session
echo cstm_248656b5-task_err=1 >>~/.config/zzzfm/session
echo cstm_248656b5-task_out=1 >>~/.config/zzzfm/session
echo cstm_248656b5-keep=1 >>~/.config/zzzfm/session
echo cstm_248656b5-next=cstm_6c5d99cd >>~/.config/zzzfm/session
echo cstm_6c5d99cd-y= >>~/.config/zzzfm/session
echo cstm_6c5d99cd-label=$compact_text >>~/.config/zzzfm/session
echo cstm_6c5d99cd-icon=/usr/share/icons/papirus-antix/48x48/actions/burst.png >>~/.config/zzzfm/session
echo cstm_6c5d99cd-tool=1 >>~/.config/zzzfm/session
echo cstm_6c5d99cd-prev=cstm_248656b5 >>~/.config/zzzfm/session
echo cstm_6c5d99cd-line=bash $HOME/.config/zzzfm/compact_view_zzzfm.sh >>~/.config/zzzfm/session
echo cstm_6c5d99cd-task=1 >>~/.config/zzzfm/session
echo cstm_6c5d99cd-task_err=1 >>~/.config/zzzfm/session
echo cstm_6c5d99cd-task_out=1 >>~/.config/zzzfm/session
echo cstm_6c5d99cd-keep=1 >>~/.config/zzzfm/session

#Hide empty devices:
pkill zzzfm
zzzfm & sleep 0.1
pkill zzzfm
sed -i 's/dev_show_empty-b=.*/dev_show_empty-b=2/' ~/.config/zzzfm/session


##Correct Home fodler to point to user's home:
#find out where the  line that says: -label=/home is and perform editions:
home_line=$(grep -nr '\-label=\/home' ~/.config/zzzfm/session | cut -d: -f1)
home_id=$(sed -n -e "`echo $home_line`p" ~/.config/zzzfm/session| cut -d- -f1)
sed -i "s/$home_id-z=.*/$home_id-z=~/g"  ~/.config/zzzfm/session
TEXTDOMAINDIR=/usr/share/locale
TEXTDOMAIN=zzzfm
homie=$"Home"
sed -i "s/$home_id-label=.*/$home_id-label=$homie/g"  ~/.config/zzzfm/session
echo $home_id-icon=gtk-home >>~/.config/zzzfm/session
sed -i "s/$home_id-icon=.*/$home_id-icon=gtk-home/g"  ~/.config/zzzfm/session

#Add zzz search:
TEXTDOMAINDIR=/usr/share/locale
TEXTDOMAIN=zzzfm
search_text=$"_File Search"
echo context_dlg-x=746 >>~/.config/zzzfm/session
echo context_dlg-y=600 >>~/.config/zzzfm/session
echo context_dlg-b=2 >>~/.config/zzzfm/session
echo cstm_11d43659-next=cstm_12757b81 >>~/.config/zzzfm/session
echo text_dlg-x=500 >>~/.config/zzzfm/session
echo text_dlg-y=304 >>~/.config/zzzfm/session
echo cstm_12757b81-y= >>~/.config/zzzfm/session
echo cstm_12757b81-label=$search_text  >>~/.config/zzzfm/session
echo cstm_12757b81-tool=1 >>~/.config/zzzfm/session
echo cstm_12757b81-prev=cstm_11d43659 >>~/.config/zzzfm/session
echo cstm_12757b81-line=zzzfm -f ~ >>~/.config/zzzfm/session
echo cstm_12757b81-task=1 >>~/.config/zzzfm/session
echo cstm_12757b81-task_err=1 >>~/.config/zzzfm/session
echo cstm_12757b81-task_out=1 >>~/.config/zzzfm/session
echo cstm_12757b81-keep=1 >>~/.config/zzzfm/session
echo cstm_12757b81-icon=search >>~/.config/zzzfm/session

#clean up toolbar from toogles:
#make insertions on the ID that referes to tool=14
toogles_line=$(grep -nr 'tool=14' ~/.config/zzzfm/session | cut -d: -f1)
toogles_id=$(sed -n -e "`echo $toogles_line`p" ~/.config/zzzfm/session| cut -d- -f1)
echo  panel1_tool_l-child=$toogles_id >>~/.config/zzzfm/session
echo  $toogles_id-parent=panel1_tool_l >>~/.config/zzzfm/session

#add terminal to menu:
#get localized version of the word "Terminal" (in zzzfm it's the T_erminal)
export TEXTDOMAIN=zzzfm;  terminal_label=$(echo "$(gettext "T_erminal")")
#remove any underscore
terminal_label=${terminal_label//_/}

echo sep_edit-next=cstm_1b8b4e5f >>~/.config/zzzfm/session
echo cstm_1b8b4e5f-y= >>~/.config/zzzfm/session
echo cstm_1b8b4e5f-label=$terminal_label >>~/.config/zzzfm/session
echo cstm_1b8b4e5f-prev=sep_edit >>~/.config/zzzfm/session
echo cstm_1b8b4e5f-line=roxterm %d >>~/.config/zzzfm/session
echo cstm_1b8b4e5f-task=1 >>~/.config/zzzfm/session
echo cstm_1b8b4e5f-task_err=1 >>~/.config/zzzfm/session
echo cstm_1b8b4e5f-task_out=1 >>~/.config/zzzfm/session
echo cstm_1b8b4e5f-keep=1 >>~/.config/zzzfm/session
echo cstm_1b8b4e5f-icon=terminal >>~/.config/zzzfm/session


#Start zzzfm:
zzzfm
 ###restaurar a configuração original
 ###cp ~/.config/zzzfm/sessionBACKUP-FT10 ~/.config/zzzfm/session   
 ######## pkill zzzfm &&  mv  ~/.config/zzzfm-2-2-22 ~/.config/zzzfm/
