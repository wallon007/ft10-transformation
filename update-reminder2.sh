#!/bin/bash
days_to_wait_before_warning=8
date_of_last_update=$(stat -c %y /var/lib/apt/periodic/update-success-stamp| cut -d" " -f1)
today=$(date +%Y-%m-%d)
let DIFF=(`date +%s -d $today`-`date +%s -d $date_of_last_update`)/86400
available_updates=$(apt-get dist-upgrade -s --quiet=2 | grep ^Inst | wc -l)

if [ "$available_updates" -gt "0" ]; then
 Warning=$"\nYou have at least, $available_updates update(s) available, last time you checked! ";
fi

if [ $DIFF -ge $days_to_wait_before_warning ]; then
#check if a network connection is available, if no network is detected, exit
	network_check=$(ping -c 1 -q google.com >&/dev/null; echo $?)
	if [ "$network_check" -eq "0" ]; then
	 echo "You are connect to a network";
		else echo "No network available"; exit;
	fi

yad --borders=10 --image="/usr/share/icons/papirus-antix/48x48/emblems/emblem-important.png" --undecorated --geometry=280x50-40-50  --window-icon="software-sources" --title="antix Updater" --text=$"It's been $DIFF day(s) since you last check for updates. \n You should check for updates now. $warning" --button="x" --button=$"Check for updates":"bash -c yad-updater" 

 else

 #Even if user ran "apt update" recently, always warn if there are upgrades available. 	
	if [ "$available_updates" -gt "0" ]; then
	yad  --borders=10 --image="/usr/share/icons/papirus-antix/48x48/emblems/emblem-important.png" --undecorated --geometry=280x50-40-50 --window-icon="software-sources" --title="antix Updater" --text=$"$Warning" --button=$"Update package(s)":"bash -c 'gksudo yad-updater'" --button="x"
    fi 
 exit
fi
