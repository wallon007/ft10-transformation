#!/bin/bash

main(){
    SCRIPT_NAME="${1}"
    PO_FOLDER="${2}"
    
    PO_FOLDER="$(readlink -f "$PO_FOLDER")"
    
    if [ -z "$SCRIPT_NAME" ] || [ -z "$PO_FOLDER" ]; then exit 1; fi
    
    LOCALE_FOLDER="$(dirname "$PO_FOLDER")/locale"
    
    # Before starting, remove the old locale folder
    clean_locale
    
    # Process .po files for different folders
    if $(ls -d "${PO_FOLDER}"/*/ 1>/dev/null 2>/dev/null); then
        for folder in "${PO_FOLDER}"/*/; do
            [ ! -d "$folder" ] && continue
            FILE_NAME="$(echo "$folder" | rev | cut -d"/" -f2 | rev)"
            echo "Processing translations for $FILE_NAME..."
            create_locales "${PO_FOLDER}/$FILE_NAME" "$LOCALE_FOLDER" "$FILE_NAME"
        done
    fi
    
    # If folder contains .po files, process them
    if $(ls "${PO_FOLDER}"/*.po 1>/dev/null 2>/dev/null); then
        create_locales "${PO_FOLDER}" "$LOCALE_FOLDER" "$SCRIPT_NAME"
    fi
}

clean_locale(){
    if [ -d "$LOCALE_FOLDER" ]; then
        rm -r "$LOCALE_FOLDER"
    fi
}

create_locales(){
    local FILE_NAME FOLDER_IN FOLDER_OUT PESKY_NAME MO_NAME MO_FOLDER
    FOLDER_IN="${1}"
    FOLDER_OUT="${2}"
    FILE_NAME="${3}"
    
    MO_NAME="$FILE_NAME"
    
    # if the .po files also contain a name before the language code
    PESKY_NAME="$(pesky_name "$FOLDER_IN" "$MO_NAME")"
    [ ! -z "$PESKY_NAME" ] && echo "found pesky name $PESKY_NAME"
    
    for file in "${FOLDER_IN}"/*.po ; do
        LANGUAGE_CODE="$(echo "${file##*/}" | cut -d"." -f1)"
        if [ ! -z "$PESKY_NAME" ]; then
            LANGUAGE_CODE="$(echo "$LANGUAGE_CODE" | sed "s/${PESKY_NAME}_//")"
        fi
        
        # Make language locale folder
        MO_FOLDER="${FOLDER_OUT}/${LANGUAGE_CODE}/LC_MESSAGES"
        if [ ! -d "$MO_FOLDER" ]; then
            mkdir -p "$MO_FOLDER"
        fi
        # Convert .po to .mo
        msgfmt "$file" -o "${MO_FOLDER}/${MO_NAME}.mo"
    done
}

pesky_name(){
    local CHECK_FOLDER NAME_CANDIDATE TOTAL_PO FILE_LIST
    local STRIP_NAME TOTAL_STRIP EXAMPLE_STRING UN_TIMES v
    CHECK_FOLDER="${1}"
    NAME_CANDIDATE="${2}"
    
    FILE_LIST="$(ls -1 "$CHECK_FOLDER"/*.po | sed "s#${CHECK_FOLDER}/##")"
    TOTAL_PO=$(echo "$FILE_LIST" | grep -c ".po$")
    
    if [ ! -z "$NAME_CANDIDATE" ] && \
    [ $(echo "$FILE_LIST" | grep -c "^${NAME_CANDIDATE}_") -eq $TOTAL_PO ]; then
        echo "$NAME_CANDIDATE"
    else
        NAME_CANDIDATE=""
        
        EXAMPLE_STRING="$(echo "$FILE_LIST" | head -n 1)"
        
        UN_TIMES="$(echo "$EXAMPLE_STRING" | grep -o "_" | wc -l)"
        
        [ "$UN_TIMES" -lt 1 ] && return 1
        
        v=1
        
        while [ $v -le $UN_TIMES ]; do
            STRIP_NAME="$(echo "$EXAMPLE_STRING" | cut -d"_" -f1-$v)"
            TOTAL_STRIP=$(echo "$FILE_LIST" | grep -c "$STRIP_NAME")
            
            # Consider NAME_CANDIDATE or exit loop
            [ $TOTAL_STRIP -eq $TOTAL_PO ] && \
            NAME_CANDIDATE="$STRIP_NAME" || break
            v=$((++v))
        done
        
        if [ $TOTAL_PO -lt 3 ] || [ -z "$NAME_CANDIDATE" ]; then return 1; fi
        
        # This is the pesky name
        echo "$NAME_CANDIDATE"
    fi
}

main "$@"
