# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
# Translators:
# Robin, 2022
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-01-18 01:27+0100\n"
"PO-Revision-Date: 2022-01-18 00:38+0000\n"
"Last-Translator: Robin, 2022\n"
"Language-Team: Estonian (https://www.transifex.com/antix-linux-community-contributions/teams/120110/et/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: et\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: FT10-yalarm2.sh:79 FT10-yalarm2.sh:87 FT10-yalarm2.sh:132
msgid "Alarm set to"
msgstr "Äratus on seatud"

#: FT10-yalarm2.sh:80 FT10-yalarm2.sh:86
msgid "Alarm"
msgstr "Alarm"

#: FT10-yalarm2.sh:81
msgid "    \\t<b>$alarm_set_to_text $hr:$mn</b> "
msgstr "    \\t<b>$alarm_set_to_text $hr:$mn</b> "

#: FT10-yalarm2.sh:88
msgid "To cancel, right-click icon for menu"
msgstr "Tühistamiseks paremklõpsake menüü ikoonil"

#: FT10-yalarm2.sh:89
msgid "    \\t<b>$alarm_text $hr:$mn...</b> \\n "
msgstr "    \\t<b>$alarm_text $hr:$mn...</b> \\n "

#: FT10-yalarm2.sh:133
msgid "To cancel, right-click for menu"
msgstr "Tühistamiseks paremklõpsake menüü avamiseks"

#: FT10-yalarm2.sh:134
msgid "Cancel Alarm"
msgstr "Tühista äratus"

#: FT10-yalarm2.sh:141
msgid ""
"$alarm_set_to_text $hr:$mn\n"
"$cancel_text"
msgstr ""
"$alarm_set_to_text $hr:$mn\n"
"$cancel_text"

#: FT10-yalarm2.sh:145
msgid ""
"$cancel_alarm_text !bash -c 'cancel_quit_tray $XPID $target'!gtk-cancel"
msgstr ""
"$cancel_alarm_text !bash -c 'cancel_quit_tray $XPID $target'!gtk-cancel"

#: FT10-yalarm2.sh:155
msgid "Are you sure???"
msgstr "Oled sa kindel???"

#: FT10-yalarm2.sh:156
msgid "  Are you sure to set the alarm for that time tomorrow?"
msgstr "  Kas paned homme kindlasti äratuse selleks ajaks?"

#: FT10-yalarm2.sh:157
msgid "$confirm_title"
msgstr "$confirm_title"

#: FT10-yalarm2.sh:157
msgid "$are_you_sure_text"
msgstr "$are_you_sure_text"

#: FT10-yalarm2.sh:273
msgid "Duration"
msgstr "Kestus"

#: FT10-yalarm2.sh:274
msgid "Interval"
msgstr "Intervall"

#: FT10-yalarm2.sh:275
msgid "Repeat"
msgstr "Korda"

#: FT10-yalarm2.sh:276
msgid "seconds"
msgstr "sekundit"

#: FT10-yalarm2.sh:277
msgid "infinite"
msgstr "lõpmatu"

#: FT10-yalarm2.sh:280
msgid "Hour::CB"
msgstr "Tund::CB"

#: FT10-yalarm2.sh:281
msgid "Minutes::CB"
msgstr "Minutid::CB"
