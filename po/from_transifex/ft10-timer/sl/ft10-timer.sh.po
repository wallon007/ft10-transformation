# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
# Translators:
# Robin, 2022
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-01-18 01:27+0100\n"
"PO-Revision-Date: 2022-01-18 00:38+0000\n"
"Last-Translator: Robin, 2022\n"
"Language-Team: Slovenian (https://www.transifex.com/antix-linux-community-contributions/teams/120110/sl/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: sl\n"
"Plural-Forms: nplurals=4; plural=(n%100==1 ? 0 : n%100==2 ? 1 : n%100==3 || n%100==4 ? 2 : 3);\n"

#: FT10-timer.sh:67
msgid "Seconds!Minutes"
msgstr "Sekund!Minute"

#: FT10-timer.sh:103 FT10-timer.sh:228
msgid "Seconds"
msgstr "Sekund"

#: FT10-timer.sh:107
msgid "Never"
msgstr "Nikoli"

#: FT10-timer.sh:130
msgid "Name"
msgstr "ime"

#: FT10-timer.sh:136
msgid "Duration"
msgstr "Trajanje"

#: FT10-timer.sh:151
msgid "Timer duration units"
msgstr "Enote trajanja časovnika"

#: FT10-timer.sh:152
msgid "Number of times to run set (all timers)"
msgstr "Nastavljeno število zagonov (vsi merilniki časa)"

#: FT10-timer.sh:153
msgid "Progress Bar update every x seconds"
msgstr "Vrstica napredka se posodablja vsakih x sekund"

#: FT10-timer.sh:154
msgid "Alarm sound filename"
msgstr "Ime datoteke zvoka alarma"

#: FT10-timer.sh:155
msgid "SUSPEND COMPUTER"
msgstr "USTAVITE RAČUNALNIK"

#: FT10-timer.sh:157
msgid "Ask to begin each timer"
msgstr "Prosite za začetek vsakega časovnika"

#: FT10-timer.sh:158
msgid "Pop-up message when each timer ends"
msgstr "Pojavno sporočilo, ko se vsak časovnik konča"

#: FT10-timer.sh:159
msgid "Sound alarm when each timer ends"
msgstr "Zvočni alarm, ko se vsak časovnik konča"

#: FT10-timer.sh:160
msgid "Ask to begin each set (all timers)"
msgstr "Prosite za začetek vsakega niza (vsi merilniki časa)"

#: FT10-timer.sh:161
msgid "Pop-up message when each set ends"
msgstr "Pojavno sporočilo, ko se vsak sklop konča"

#: FT10-timer.sh:162
msgid "Sound alarm when each set ends"
msgstr "Zvočni alarm, ko se vsak sklop konča"

#: FT10-timer.sh:163
msgid "Interface to Sysmonitor Indicator"
msgstr "Vmesnik za indikator Sysmonitor"

#: FT10-timer.sh:164
msgid "Auto close progress bar display when all sets end"
msgstr "Samodejno zapiranje prikaza vrstice napredka, ko se končajo vsi nizi"

#: FT10-timer.sh:193
msgid "Timers"
msgstr "Časovniki"

#: FT10-timer.sh:193
msgid "Configuration"
msgstr "Konfiguracija"

#: FT10-timer.sh:196
msgid "Multiple Timer settings"
msgstr "Več nastavitev časovnika"

#: FT10-timer.sh:302
msgid "Ready to start"
msgstr "Pripravljen za začetek"

#: FT10-timer.sh:331
msgid "has ended."
msgstr "se je končalo."

#: FT10-timer.sh:461
msgid "Finished."
msgstr "Dokončano."

#: FT10-timer.sh:530
msgid "Each timer end"
msgstr "Konec vsakega časovnika"
