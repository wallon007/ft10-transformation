# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
# Translators:
# Robin, 2022
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-01-18 01:27+0100\n"
"PO-Revision-Date: 2022-01-18 00:39+0000\n"
"Last-Translator: Robin, 2022\n"
"Language-Team: Malagasy (https://www.transifex.com/antix-linux-community-contributions/teams/120110/mg/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: mg\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"

#: tim-tint-old.sh:93
msgid "No icon located, using default Gears icon"
msgstr "Tsy misy kisary hita, mampiasa kisary Gears default"

#: tim-tint-old.sh:95
msgid "Icon located!"
msgstr "Kisary hita!"

#: tim-tint-old.sh:156
msgid "Tint2 icons"
msgstr "Kisary Tint2"

#: tim-tint-old.sh:156
msgid "Double click any Application to remove its icon:"
msgstr "Kitiho indroa izay Application hanesorana ny kisary:"

#: tim-tint-old.sh:216
msgid "Double click any Application to move its icon:"
msgstr "Kitiho indroa ny Application mba hamindra ny kisary:"

#: tim-tint-old.sh:305
msgid "Choose application to add to the Toolbar"
msgstr "Safidio ny fampiharana ampidirina ao amin'ny Toolbar"

#: tim-tint-old.sh:327
msgid "ADD ICON!add:FBTN"
msgstr "ADD ICON!add:FBTN"

#: tim-tint-old.sh:327
msgid "MOVE ICON!gtk-go-back-rtl:FBTN"
msgstr "MOVE ICON!gtk-go-back-rtl:FBTN"

#: tim-tint-old.sh:327
msgid "REMOVE ICON!remove:FBTN"
msgstr "ESOTRA ICON!remove:FBTN"
