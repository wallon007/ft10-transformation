# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
# Translators:
# Robin, 2022
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-01-18 01:27+0100\n"
"PO-Revision-Date: 2022-01-18 00:38+0000\n"
"Last-Translator: Robin, 2022\n"
"Language-Team: Khmer (https://www.transifex.com/antix-linux-community-contributions/teams/120110/km/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: km\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#: ft10-tz-manager.sh:6
msgid "ADD Time zone"
msgstr "បន្ថែមតំបន់ពេលវេលា"

#: ft10-tz-manager.sh:7
msgid "REMOVE Time zone"
msgstr "លុបតំបន់ពេលវេលា"

#: ft10-tz-manager.sh:18 ft10-tz-manager.sh:40 ft10-tz-manager.sh:55
#: ft10-tz-manager.sh:75
msgid "FT10-clock"
msgstr "FT10-នាឡិកា"

#: ft10-tz-manager.sh:39
msgid ""
"Insert Time Zone to be added to the World Clock \\n (Enter nothing to search"
" trough all available timezones)"
msgstr ""
"បញ្ចូល Time Zone ដើម្បីបញ្ចូលទៅក្នុងនាឡិកាពិភពលោក \\n "
"(បញ្ចូលអ្វីទាំងអស់ដើម្បីស្វែងរកតាមតំបន់ពេលវេលាដែលមានទាំងអស់។)"

#: ft10-tz-manager.sh:75
msgid "$add_tz_text!add:FBTN"
msgstr "$add_tz_text!add:FBTN"

#: ft10-tz-manager.sh:75
msgid "$delete_tz_text!remove:FBTN"
msgstr "$delete_tz_text!remove:FBTN"
