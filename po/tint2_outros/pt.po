# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# marcelocripe <marcelocripe@gmail.com>, 2021.
#
msgid ""
msgstr ""
"Project-Id-Version: tint2_outross\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-11-14 09:22+0100\n"
"PO-Revision-Date: 2021-11-14 09:22+0100\n"
"Last-Translator: Automatically generated\n"
"Language-Team: none\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: tint2_outros.sh:10
msgid "Toolbar size"
msgstr "Tamanho da Barra de Ferramentas"

#: tint2_outros.sh:11
msgid "Toolbar width"
msgstr "Largura da Barra de Ferramentas"

#: tint2_outros.sh:12
msgid "Toolbar height"
msgstr "Altura da Barra de Ferramentas"

#: tint2_outros.sh:13
msgid "Toolbar border"
msgstr "Borda da Barra de Ferramentas"

#: tint2_outros.sh:14
msgid "Height when hidden"
msgstr "Altura da Barra Quando Estiver Oculta"
