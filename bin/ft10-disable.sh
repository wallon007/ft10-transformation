#!/bin/bash
#Automatic disable antiX FT10 Transformation Pack
#Free GPL license, by PCC, 25-11-2021

#Get What window manager is running: 
window_manager=$(wmctrl -m| sed -n '1p' | cut -d ' ' -f2)

#Security prompt:
yad --center --width=300 --title="FT10 Transformation Pack" --text="FT10 $window_manager -->  $window_manager ???" --button=" x ":0 --button "OK"
foo=$?

if [ $foo = 1 ] ; then
#if USER CLICKS "OK"- DISABLE FT10...

if [ $window_manager = "IceWM" ] ; then
\cp ~/.icewm/preferences-ft10-backup ~/.icewm/preferences ; \cp  ~/.icewm/startup-ft10-backup ~/.icewm/startup
pkill tint2
/usr/local/lib/desktop-session/desktop-session-restart
fi

if [ $window_manager = "Fluxbox" ] ; then
\cp  ~/.fluxbox/init-ft10-backup ~/.fluxbox/init ; \cp  ~/.fluxbox/startup-ft10-backup ~/.fluxbox/startup
pkill tint2
/usr/local/lib/desktop-session/desktop-session-restart
fi

if [ $window_manager = "JWM" ] ; then
\cp  ~/.jwmrc-ft10-backup ~/.jwmrc ; \cp  ~/.jwm/startup-ft10-backup ~/.jwm/startup
pkill tint2
/usr/local/lib/desktop-session/desktop-session-restart
fi

#end initial if-fi loop, from the yad confirmation window
fi
