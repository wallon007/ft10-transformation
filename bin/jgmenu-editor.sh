#!/bin/bash
# Very basic managment of jgmenu's contents (adding "favorites" is done using the info from the app's .desktop file) 
# Version Beta3, By PPC, 8/8/2021 adapted from many, many on-line examples and Tollbar Icon Manager for IceWM
# GPL licence - feel free to improve/adapt this script - but keep the lines about the license and author
###Create list of availables apps, with localized names- this takes a few seconds on low powered devices:

TEXTDOMAINDIR=/usr/share/locale
TEXTDOMAIN=icewm-toolbar-icon-manager.sh

delete_icon()
{
		###Function to delete  icon
		### Select any application whose icon you want to remove from the toolbar:
		#display only application names
sed "/jgmenu-editor/d" ~/.config/jgmenu/append.csv| sed '/^#/d' | sed '/^\^/d'| awk -F"," '{print $1}'  > /tmp/toolbar-test-edit.txt
#remove empty lines from file:
sed -i '/^[[:space:]]*$/d' /tmp/toolbar-test-edit.txt
#select icon to delete
EXEC=$(yad --window-icon="/usr/share/icons/papirus-antix/24x24/actions/application-menu.png" --title=$"Ft10 Menu" --width=450 --height=480 --center --separator=" " --list  --column=$" "  < /tmp/toolbar-test-edit.txt --button=$" - ":4)
#get line number(s) where the choosen application is
x=$(echo $EXEC)
Line=$(sed -n "/$x/=" ~/.config/jgmenu/append.csv)
## NOTE: to be on the safe side, in order to use $Line to delete the line(s) that match the selection, first extract it's first number, to avoid errors in case more than one line matchs the selection (there's more that one icon for the same app on the toolbar), TIM should only delete the first occorrence!!! Also: changed the sed command so it directly deletes line number $Line (solves the bug of not deleting paterns with spaces)...
firstx=$(echo $Line | grep -o -E '[0-9]+' | head -1 | sed -e 's/^0\+//')
# remove the first line that matchs the user selection, only if something was selected
if test -z "$x" 
then
      echo $"nothing was selected"
else
sed -i ${firstx}d ~/.config/jgmenu/append.csv
fi
}		

move_icon()
{
#display only application names	
sed "/jgmenu-editor/d" ~/.config/jgmenu/append.csv| sed '/^#/d' | sed '/^\^/d'| awk -F"," '{print $1}'  > /tmp/toolbar-test-edit.txt
#remove empty lines from file:
sed -i '/^[[:space:]]*$/d' /tmp/toolbar-test-edit.txt
#select icon to move
EXEC=$(yad --window-icon="/usr/share/icons/papirus-antix/24x24/actions/application-menu.png" --title=$"Ft10 Menu" --width=450 --height=480 --center --separator=" " --list  --column=$" "  < /tmp/toolbar-test-edit.txt --button=$"Move":4)
		#get line number(s) where the choosen application is
		x=$(echo $EXEC)
		 Line=$(sed -n "/$x/=" ~/.config/jgmenu/append.csv)
		 #get number of lines in file
		 number_of_lines=$(wc -l < $file)

#only do something if a icon was selected :
if test -z "$x" 
then
      echo $"nothing was selected"
else

file_name=~/.config/jgmenu/append.csv
a=$Line

#this performs an infinite loop, so the Move window is ALWAYS open unless the user clicks "Cancel"
	while :
	do

yad --center --title=$"Ft10 Menu" --text=$"Move $EXEC" \
--button=" x ":1 \
--button=$"↑":2 \
--button=$"↓":3 

foo=$?
Line_to_the_left=line_number=$((line_number-1))
line_number=$a

if [[ $foo -eq 1 ]]; then exit
fi

#move icon to the left:
if [[ $foo -eq 2 ]]; then
b=$(($a-1))
	if [ $b -gt 0 ]; then
sed -i -n "$b{h; :a; n; $a{p;x;bb}; H; ba}; :b; p" ${file_name} 
a=$(($a-1))   # update selected icon's position, just in case the user wants to move it again
	fi
fi

#move icon to the right
if [[ $foo -eq 3 ]]; then
a=$(($a+1))
number_of_lines=$(wc -l < ~/.config/jgmenu/append.csv)
b=$(($a-1))
    if [[ $line_number -ge  $number_of_lines ]]; then 
  exit 
  else
  sed -i -n "$b{h; :a; n; $a{p;x;bb}; H; ba}; :b; p" ${file_name} > test2.txt
  fi
fi

	done

fi ### ends if cicle that checks if user selected icon to move in the main Move icon window

	}	

add_icon()
{
#use app-select to get application to be added to the menu
add_icon=$(app-select --s)

#if no choice was made, exit
[ -z "$add_icon" ] && exit

#if a choice was made, process app-select's output
NOME=$(echo $add_icon| cut -d '|' -f2)
EXECperc=$(echo $add_icon| cut -d '|' -f3)
ICONE=$(echo $add_icon| cut -d '|' -f6)

#add entry to menu, inserting it in the second to last line- the last line is the separtor
xx=$(grep -c ".*" ~/.config/jgmenu/append.csv)
line=$((xx-1))
i="i"
text=$(echo sed -i \'$line$i\\\ ${NOME}, ${EXECperc}, ${ICONE}\' ~/.config/jgmenu/append.csv)
eval $text
		###END of Function to add a new icon
}

advanced()
{
		###Function to manually manage icons (ADVANCED management)
   geany ~/.config/jgmenu/append.csv
		###END of Function to manually arrange icons
}	

toggle_tiles()
{
			###Function to toggle the prepend.csv file on and off (that's the file that shows the "Tiles")
POSICAO_ATUAL=$(egrep "^menu_width          =" ~/.config/jgmenu/jgmenurc | cut -d'=' -f2)
echo $POSICAO_ATUAL
if [[ POSICAO_ATUAL -eq 620 ]]
then
echo tiles are enabled, disabling
#disable tiles
sed -i "/^menu_width          = /s/620/430/g" ~/.config/jgmenu/jgmenurc
sed -i "/^menu_padding_right  =/s/200/2/g" ~/.config/jgmenu/jgmenurc
exit 0
fi

if [[ POSICAO_ATUAL -eq 430 ]]  
then
echo tiles are disabled, enabling
#enable tiles
sed -i "/^menu_width          = /s/430/620/g" ~/.config/jgmenu/jgmenurc
sed -i "/^menu_padding_right  =/s/2/200/g" ~/.config/jgmenu/jgmenurc
exit 0
fi
 		###END of Function to toggle the prepend.csv file on and off 
}	

export -f delete_icon advanced add_icon move_icon toggle_tiles

#Text if prepend.csv has content that only exists in the "tiled" menu, if it does, show the "Toogle tiles" button, if not, don't show it
#content="@rect,,44,4,392,36,2,left,top,#000000 0,#656565 50,"
content="@rect,,46,4,380,36,2,left,top,auto,auto,"
if grep -Fxq "$content" ~/.config/jgmenu/prepend.csv
then

	DADOS=$(yad --length=200 --width=280 --center --title="Ft10 Menu" --window-icon="/usr/share/icons/papirus-antix/24x24/actions/application-menu.png" \
--form  \
--button=" x ":1 \
--field=$"ADVANCED!help-hint:FBTN" "bash -c advanced" \
--field=$"ADD ICON!add:FBTN" "bash -c add_icon" \
--field=$"MOVE ICON!gtk-go-back-rtl:FBTN" "bash -c move_icon" \
--field=$"REMOVE ICON!remove:FBTN" "bash -c delete_icon" \
--field=$"Tiles on/off!settings:FBTN" "bash -c toggle_tiles" \
--wrap --text=$"")

else

	DADOS=$(yad --length=200 --width=280 --center --title="Ft10 Menu" --window-icon="/usr/share/icons/papirus-antix/24x24/actions/application-menu.png" \
--form  \
--button=" x ":1 \
--field=$"ADVANCED!help-hint:FBTN" "bash -c advanced" \
--field=$"ADD ICON!add:FBTN" "bash -c add_icon" \
--field=$"MOVE ICON!gtk-go-back-rtl:FBTN" "bash -c move_icon" \
--field=$"REMOVE ICON!remove:FBTN" "bash -c delete_icon" \
--wrap --text=$"")

fi

### wait for a button to be pressed then perform the selected function
foo=$?

[[ $foo -eq 1 ]] && exit 0
