#!/bin/bash
#Detailed view
pid_zzzfm=$(ps -C zzzfm -o pid=)
pid_spacefm=$(ps -C spacefm -o pid=)

var=""

if [ -n "$pid_zzzfm" ]; then
    echo "zzzfm seems to be running"
    killall zzzfm 
    sleep 0.1
    killall spacefm
fi

if [ -n "$pid_spacefm" ]; then
    echo "spacefm seems to be running"
    pkill spacefm
fi


sed -i 's/panel1_list_detailed-b=.*/panel1_list_detailed-b=1/' ~/.config/zzzfm/session && sed -i 's/panel1_list_compact-b.*/panel1_list_compact-b=2/' ~/.config/zzzfm/session

zzzfm
