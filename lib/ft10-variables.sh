#!/bin/bash

### Localization ###
TEXTDOMAINDIR=/usr/share/locale
TEXTDOMAIN=ft10-templates

# TINT2 VALUES
MENU_BUTTON=$"Menu" # Menu Iniciar
ROFI_BUTTON=$"Search for programs and files" # Procurar aplicativos / Procurar arquivos
TASK_SWITCHER_BUTTON=$"Task switcher" # Alternar Janelas / Exibir fundo do ambiente de trabalho
FT10_Icons=$"Manage Toolbar icons" # Gerir ícones da barra (adicionar/remover/mover ícones)
FILE_MANAGER_BUTTON=$"File Manager" # Gerenciador de Arquivos
WEB_BROWSER_BUTON=$"Web Browser"
NETWORK_BUTTON=$"Network manager - Connman" # Gerenciador de Redes - Connman
VOLUME_BUTTON=$"Volume" # Volume
WEATHER_BUTTON=$"Weather" # Previsão do Tempo
UNPLUGDRIVE=$"Unplug USB drives" # Ejetar dispostivos USB
EXIT_BUTTON=$"Exit" # Sair
SHOW_DESKTOP=$"Show Desktop" #Exibir ambiente de trabalho

#JGMENU VALUES
ACCESORIES_CATEGORY=$"Accessories"
DEVELOPMENT_CATEGORY=$"Development"
GAMES_CATEGORY=$"Games"
GRAPHICS_CATEGORY=$"Graphics"
INTERNET_CATEGORY=$"Internet"
MULTIMEDIA_CATEGORY=$"Multimedia"
OFFICE_CATEGORY=$"Office"
SETTINGS_CATEGORY=$"Settings"
SYSTEM_CATEGORY=$"System"
ALL_CATEGORY=$"All"

RECENT_BUTTON=$"Recent" #
STOP_APP_BUTTON=$"Stop App" # Fechar Janela
CONTROL_CENTRE_BUTTON=$"Control Centre" # Centro de Controlo
FAVORITES_ENTRY=$"Favorite Apps: (click here to edit the list)"
TERMINAL_ENTRY=$"Terminal"
TEXT_EDITOR_ENTRY=$"Text Editor"
EMAIL_ENTRY=$"E-mail"
