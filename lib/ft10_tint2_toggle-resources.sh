#!/bin/bash
# Tint2 toolbar quick settings- toggle system resources gauge, by PPC
FILE="$HOME/.config/tint2/tint2rc"
STRING="execp_command = /usr/local/lib/ft10/executors/cpu"
if  grep -q "$STRING" "$FILE" ; then
         sed -i 's*execp_command = /usr/local/lib/ft10/executors/cpu*execp_command = /usr/local/lib/ft10/executors/no-cpu*' $FILE 
 else
         sed -i 's*execp_command = /usr/local/lib/ft10/executors/no-cpu*execp_command = /usr/local/lib/ft10/executors/cpu*' $FILE
fi
# restart tint2
killall -9 tint2
nohup tint2 &
