#!/bin/bash
# Adds/Removes icons to the Tint2 toolbar(adding icons is done using the info from the app's .desktop file) 
# By PPC, 6/01/2021 adapted from many, many on-line examples, and from toolbar icon manager for icewm
# GPL licence - feel free to improve/adapt this script - but keep the lines about the license and author
###Strings to translate:

TEXTDOMAINDIR=/usr/share/locale
TEXTDOMAIN=icewm-toolbar-icon-manager.sh

window_title="Tint2 icons"
text_loading="Tint2 icons..."
#add_icons_text="Select icon to be added to the toolbar"
#remove_icons_text="Select icon to be removed from the toolbar"
move_icons_text="Select icon to be moved in the toolbar"
moving_text="Moving "
add_text="Add"
remove_text="Remove"
move_text="Move"
###
window_icon="/usr/share/icons/papirus-antix/22x22/apps/tint2.png"

#Export files, so then can be used in functions
export window_title add_icons_text remove_icons_text move_icons_text moving


delete_icon()
{

for (( ; ; ))
do

###Function to delete  icon
#criar lista de ícones:
grep -r "launcher_item_app" ~/.config/tint2/tint2rc > /tmp/launchers_list.txt
#remover o texto "launcher_item_app = ", 21 letters
(cat /tmp/launchers_list.txt | cut -c21-) > /tmp/launchers.txt
#apagar ficheiro temporiario
echo > /tmp/lista_apelativa.txt
#processar, linha a linha, a lista dos launchers existentes na barra
cat /tmp/launchers.txt | while read line; do
ico=$(grep -o -m 1 '^Icon=.*' $line | cut -d\= -f2) 
	if [ -z "$ico" ]
		then
		echo "This icon - $ico - was not found, probably the corresponding application was uninstalled, ignoring it"
		#just ignore it
		#delete that non existing application icon from toolbar
		#grep -v $ico ~/.config/tint2/tint2rc > temp && mv temp ~/.config/tint2/tint2rc		 
	else
      #icon really exists, was not just left behind from a unistalled app, procced adding ico, name and .desktop file to list:
		echo $ico >> /tmp/lista_apelativa.txt
		nam=$(grep -o -m 1 '^Name=.*' $line | cut -d\= -f2) 
		echo $nam >> /tmp/lista_apelativa.txt
		echo $line >> /tmp/lista_apelativa.txt
	fi
done
# remove empty lines:
sed -i '/^[[:space:]]*$/d' /tmp/lista_apelativa.txt
cat /tmp/lista_apelativa.txt
###Get selected: icon (displayed), app name, .desktop file (hidden- this is really what we want, to remove it from tint2rc file)
selection=$(yad --window-icon="" --center --height=600 --width=450 --title=$"Tint2 icons" --list --column=:IMG --column=$"Double click any Application to remove its icon:" --column=Desktop:HD --button="-" < /tmp/lista_apelativa.txt)	
selection_to_delete=$(echo $selection|cut -d\| -f3)	

# if no application selected- avoids creating empty icon: (also allows to exit the infinit loop tha keeps respawning the selection window)
if [ -z "$selection" ]; then exit
fi

#delete application icon from toolbar
grep -v $selection_to_delete ~/.config/tint2/tint2rc > temp && mv temp ~/.config/tint2/tint2rc		 

#instantly restart toolbar to show changes
old_tint2_pid=$(pgrep tint2)
nohup  tint2 && sleep 0.1 &&
kill -9  $old_tint2_pid &

#refresh list of launchers
echo > /tmp/lista_apelativa.txt
cat /tmp/launchers.txt | while read line; do
app_with_descri=$(grep -E $line ~/.apps.txt)
[ -z "$app_with_descri" ] && app_with_descri=$line
echo $app_with_descri >> /tmp/lista_apelativa.txt
done

#end initial infine loop (so window is always open after selection)
done
}		

move_icon()
{
	#criar lista de ícones:
grep -r "launcher_item_app" ~/.config/tint2/tint2rc > /tmp/launchers_list.txt
#remover o texto "launcher_item_app = ", 21 leters
(cat /tmp/launchers_list.txt | cut -c21-) > /tmp/launchers.txt

# estabelecer linha que é o limite minimo esquerdo até onde um ícone pode ser movido
limite_esq_zona=$(grep -n -m 1 "# Launcher" ~/.config/tint2/tint2rc |sed  's/\([0-9]*\).*/\1/')
limite_esq_launchers=$(grep -n -m 1 "launcher_item_app =" ~/.config/tint2/tint2rc |sed  's/\([0-9]*\).*/\1/')
if [ $limite_esq_launchers -ge $limite_esq_zona ]; then limite_esq=$limite_esq_launchers
 else
 limite_esq=$limite_esq_zona
fi
 limite_esq_final=$(($limite_esq -1 ))
 echo $limite_esq_final

# estabelecer linha que é o limite máximo direito até onde um ícone pode ser movido 
#number_of_lines=$(wc -l < ~/.config/tint2/tint2rc)
limite_dir_launchers=$(grep -n launcher_item_app ~/.config/tint2/tint2rc |cut -d':' -f1 |tail -n 1)
 
#apagar ficheiro temporiario
echo > /tmp/lista_apelativa.txt
#processar linha alinha a lista dos launchers existentes na barra
cat /tmp/launchers.txt | while read line; do
### ver se o ficheiros apps.txt, que tem todos os programas, com descrição, tem uma linha que corresponde à aplicação que está a ser analisada, a "line" da lista launchers.txt
app_with_descri=$(grep -E $line ~/.apps.txt)
#se não houver correspondencia, manter o nome do .desktop
[ -z "$app_with_descri" ] && app_with_descri=$line
echo $app_with_descri >> /tmp/lista_apelativa.txt
done

#choose application to move
		EXEC=$(yad --window-icon=$window_icon --title="$window_title" --width=450 --height=480 --center --separator=" " --list  --column=$"Double click any Application to move its icon:"  < /tmp/lista_apelativa.txt --button="↔":4)

		#get line number(s) where the choosen application is
		x=$(echo $EXEC)
		#get last filed of the selected line in practice it's the path and file name of the .desktop file)
		x=$(echo $EXEC | awk '{print $NF}')
		Line=$(grep -n -m 1 $x ~/.config/tint2/tint2rc |cut -f1 -d: )

		 #get number of lines in file
		 number_of_lines=$(wc -l < $file)
		 
 #####Act on selection:
#only do something if a icon was selected :
if test -z "$x" 
then
      echo "nothing was selected"
else

file_name=~/.config/tint2/tint2rc
a=$Line

#this performs an infinite loop, so the Move window is ALWAYS open unless the user clicks "Cancel"
	while :
	do

nome=$(echo $EXEC |cut -d'-' -f1)
yad --window-icon=/usr/share/pixmaps/icewm_editor.png --center --undecorated --title="$window_title" --text="$moving $nome" \
--button=" !/usr/share/icons/papirus-antix/22x22/actions/gtk-quit.png":1 \
--button="←":2 \
--button="→":3 

foo=$?
Line_to_the_left=line_number=$((line_number-1))
line_number=$a

if [[ $foo -eq 1 ]]; then exit
fi

#move icon to the left:
limite_esquerdo=0
if [[ $foo -eq 2 ]]; then
b=$(($a-1))
	if [ $b -gt $limite_esq_final ]; then
sed -n "$b{h; :a; n; $a{p;x;bb}; H; ba}; :b; p" ${file_name} > test2.txt
#create backup file before changes
cp ~/.config/tint2/tint2rc ~/.config/tint2/tint2rc.bak
 cp test2.txt ~/.config/tint2/tint2rc
 sleep .3

old_tint2_pid=$(pgrep tint2)
nohup  tint2 && sleep 0.1 &&
kill -9  $old_tint2_pid &

a=$(($a-1))   # update selected icon's position, just in case the user wants to move it again
	fi
fi

#move icon to the right:
if [[ $foo -eq 3 ]]; then
a=$(($a+1))
b=$(($a-1))
    if [ $b -ge  $limite_dir_launchers ]; then 
      exit 
  else
  sed -n "$b{h; :a; n; $a{p;x;bb}; H; ba}; :b; p" ${file_name} > test2.txt
#create backup file before changes 
cp ~/.config/tint2/tint2rc ~/.config/tint2/tint2rc.bak
    cp test2.txt  ~/.config/tint2/tint2rc
    sleep .3

old_tint2_pid=$(pgrep tint2)
nohup  tint2 && sleep 0.1 &&
kill -9  $old_tint2_pid &
# There's no need to update selected icon's position, just in case the user wants to move it again, because moving right just moves the icon to the right of the select icon to the left, so, it updates instantly the selected icon's position
  fi
fi

	done

fi ### ends if cicle that checks if user selected icon to move in the main Move icon window

	}	

add_icon()
{
#check if window with the title "tint2-wait" is showing, if so, close that window:
wmctrl -lp | awk '/tint2-wait/{print $3}' | xargs kill

#use app-select to get application to be added to the menu
add_icon=$(app-select --s)

#if no choice was made, exit
[ -z "$add_icon" ] && exit

#if a choice was made, process app-select's output
NOME=$(echo $add_icon| cut -d '|' -f2)
EXECperc=$(echo $add_icon| cut -d '|' -f3)
ICONE=$(echo $add_icon| cut -d '|' -f6)
add=$(echo $add_icon| cut -d '|' -f1)

#IF no selection was made, exit
[ -z "$add" ] && exit

#add line to toolbar
echo "launcher_item_app =" $add  >> ~/.config/tint2/tint2rc
#instantly restart toolbar to show changes
old_tint2_pid=$(pgrep tint2)
nohup  tint2 && sleep 0.1 &&
kill -9  $old_tint2_pid &

###END of Function to add a new icon
}

#display main window
export -f delete_icon add_icon move_icon
DADOS=$(yad --window-icon=$window_icon --paned --center --splitter="200" --title="$window_title" \
--form  \
--center \
--field=$"ADD ICON!add:FBTN" "bash -c add_icon" \
--field=$"MOVE ICON!gtk-go-back-rtl:FBTN" "bash -c move_icon" \
--field=$"REMOVE ICON!remove:FBTN" "bash -c delete_icon" \
--wrap --no-buttons)

### wait for a button to be pressed then perform the selected function
foo=$?

[[ $foo -eq 1 ]] && exit 0
