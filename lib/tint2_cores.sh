#!/bin/bash
# cores

_troca_cor() {
	COR_ATUAL=$(egrep "^$1*" ~/.config/tint2/tint2rc | egrep -o '#[A-Fa-f0-9]{3,6}')
	while NOVA_COR=$( \
				yad --title="Select color $1" --color --init-color="$COR_ATUAL" \
				--button='Sair':1 --button='Alterar':0
				)
	do
		sed -i "/^$1/s/$COR_ATUAL/ $NOVA_COR/g" ~/.config/tint2/tint2rc
		# reiniciar o tint2
		killall -9 tint2
		tint2 &
		COR_ATUAL=$(egrep "^$1*" ~/.config/tint2/tint2rc | egrep -o '#[A-Fa-f0-9]{3,6}')
	done
}

while ITEM=$( yad --title='Configure Tint2' --list --width=220 --height=150 --hide-column=1 --no-headers \
			--column='ID' --column='Propriedade' --print-column=1 --separator='' \
			1 'Background color' \
			2 'Active color' )
do
	case "$ITEM" in
	1) _troca_cor background_color ;;
	2) _troca_cor task_active_font_color ;;
	esac
done
