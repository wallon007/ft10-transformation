#!/bin/bash
# configurações diversas do tint2
###
# The script was created by PPC
# Modified by marcelocripe 12/02/2021 - script prepared for translation

        TEXTDOMAINDIR=/usr/share/locale 
        TEXTDOMAIN=tint2_outros

#Strings to translate:
window_title=$"Toolbar size"
width=$"Toolbar width"
height=$"Toolbar height"
border=$"Toolbar border"
hidden_height=$"Height when hidden"
###
window_icon="/usr/share/icons/papirus-antix/22x22/apps/tint2.png"

function _get_configs() {
	LARGURA_ATUAL=$(egrep "^panel_size =" ~/.config/tint2/tint2rc | cut -d'=' -f2 | cut -d'%' -f1)
	ALTURA_ATUAL=$(egrep "^panel_size =" ~/.config/tint2/tint2rc | cut -d'=' -f2 | cut -d'%' -f2)
	TAMANHO_BORDA=$(egrep "^border_width =" ~/.config/tint2/tint2rc | cut -d'=' -f2)
	ALTURA_ESCONDER=$(egrep "^autohide_height =" ~/.config/tint2/tint2rc | cut -d'=' -f2)
	}

_get_configs

while CONFIGS=$(yad --center --form --window-icon=$window_icon --title="$window_title" --width=400 \
	--field="$width":NUM $LARGURA_ATUAL!30..100 --field="$height":NUM $ALTURA_ATUAL!20..100 \
	--field="$border":NUM $TAMANHO_BORDA!0..10 \
	--field="$hidden_height":NUM $ALTURA_ESCONDER!0..10 --button='X':1 --button='OK':0)
do
	NOVA_LARGURA=$(echo $CONFIGS | cut -d'|' -f1 | cut -d'.' -f1)
	NOVA_ALTURA=$(echo $CONFIGS | cut -d'|' -f2 | cut -d'.' -f1)
	NOVA_TAMANHO_BORDA=$(echo $CONFIGS | cut -d'|' -f3 | cut -d'.' -f1)
	NOVO_AUTO_ESCONDER=$(echo $CONFIGS | cut -d'|' -f4)
	NOVA_ALTURA_ESCONDER=$(echo $CONFIGS | cut -d'|' -f5 | cut -d'.' -f1)
	sed -i "/^panel_size =/s/$LARGURA_ATUAL%/ $NOVA_LARGURA%/g" ~/.config/tint2/tint2rc
	sed -i "/^panel_size =/s/$ALTURA_ATUAL/ $NOVA_ALTURA/g" ~/.config/tint2/tint2rc
	sed -i "/^border_width =/s/$TAMANHO_BORDA/ $NOVA_TAMANHO_BORDA/g" ~/.config/tint2/tint2rc
	sed -i "/^autohide_height =/s/$ALTURA_ESCONDER/ $NOVA_ALTURA_ESCONDER/g" ~/.config/tint2/tint2rc
	# restart tint2
	killall -9 tint2
	nohup tint2 &
	_get_configs
done
