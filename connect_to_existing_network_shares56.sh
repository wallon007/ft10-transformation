#!/bin/bash
#GUI Script to  search for ip's of shared folders and then connect to the selected on, using zzzfm (may be adapted to any file manager)- By PPC, 8/3/2022, full GPL license
#Dependencies: smbclient
#PS: Fully GUI eay to create/manage Samba shared folders easily- the package "system-config-samba", available in the  Repository
#sudo service smbd restart ### makes sure that Samba is running on the server, so shared folders are available

#Clean up temporary files:
echo "" > /tmp/range_of_available_ips
echo "" > /tmp/ccc
echo "" >/tmp/result

#Connectivity check:
ip=$(hostname -I)
if [ -n "$ip" ]; then
echo "Connect to a network - your ip is $ip"
else
echo "Not connected to a network, exiting"
yad --center --window-icon="/usr/share/icons/gnome/48x48/places/gnome-mime-x-directory-smb-share.png" --title="antiX-shares" --text="Not connected to a network!" --button=" x " --width 250
exit
fi


#Draw pulsating "wait" window, while searching for shared folders:
while true; do
echo "#"
done | yad --center --window-icon="/usr/share/icons/gnome/48x48/places/gnome-mime-x-directory-smb-share.png" --title="antiX-shares" --text="" --width 250 --text-align center --on-top --pulsate --no-buttons --auto-close --progress &

##Main part of the script:
#Create a list of all available ips for network shares:
ips=$(ip -o addr | sed '/: lo /d'|sed '/::/d')
newString="${ips#*inet}"
ip_range=$(echo $newString|cut -d ' ' -f 1)
variable_part=$(echo $ip_range|cut -d. -f4)
one=$(echo $ip_range|cut -d. -f1)
two=$(echo $ip_range|cut -d. -f2)
three=$(echo $ip_range|cut -d. -f3)
main_part=$(echo $one.$two.$three)
first=$(echo $ip_range|cut -d. -f4| cut -d\/ -f1)
last=$(echo $ip_range|cut -d. -f4| cut -d\/ -f2)
seq $first $last > /tmp/seq
while read sequence; do
   echo $main_part.$sequence >> /tmp/range_of_available_ips
done </tmp/seq
cat /tmp/range_of_available_ips ###This shows all possible netwrok share ips on the terminal

#Test every possible network share ip, and log the valid addresses:
while read line; do
  smbclient -L $line -N  | sed '/Printer Drivers/d' | grep "Disk" | cut -d' ' -f1 >/tmp/ccc
  
  while read line2; do
  echo //$line/$line2 >>/tmp/result
  done </tmp/ccc
  
done </tmp/range_of_available_ips

#Remove empty lines from the result:
sed -i '/^[[:space:]]*$/d' /tmp/result

#Close pulsating "wait" window:
wmctrl -c "antiX-shares"

#Count lines in /tmp/result
##lines_with_ips=$(wc -l < /tmp/result)

#Exit if no network shares were detected:
check=$(cat /tmp/result)
if [ -n "$check" ]; then
echo "Network shares detected: $check"
else
echo "No network shares detected, exiting"
yad --center --window-icon="/usr/share/icons/gnome/48x48/places/gnome-mime-x-directory-smb-share.png" --title="antiX-shares" --text="No network shares detected!" --button=" x " --width 250
exit
fi

#Window that allows user to select the share to connect to:
selectedshare=$(yad --no-buttons --window-icon="/usr/share/icons/gnome/48x48/places/gnome-mime-x-directory-smb-share.png" --title="antiX-shares" --width=250 --height=250 --fixed --center --separator=" " --list  --column=""  < /tmp/result)

#If nothing was selected, exit:
 if [[ $selectedshare = "" ]]; then exit
 fi

#Try to mount share in zzzfm:
zzzfm $selectedshare & sleep 1
