#!/bin/bash
# script to automaticly configure a trash can in zzzfm, by PPC

#Make really sure that zzzfm is not running
pkill -9 -e -f zzzfm
#back up original config file and move it out of the way, so we have a prestine configuration
 mv ~/.config/zzzfm  ~/.config/zzzfmBACKUP-FT10
 
#delete default zzzfm configurations
rm -rf ~/.config/zzzfm
 
 
##make sure zzzfm runs once and  creates a default config file
zzzfm & sleep 0.2 && pkill zzzfm


#Make sure that  trash-cli is installed or check if the latest version is installed)
#Exit if trash-cli is not installed
	if ! [ -x "$(command -v trash)" ]; then
  	 x-terminal-emulator -T $"FT10" -e /bin/bash -c "gksu 'apt install -y trash-cli' && yad --center --title=TrashCan-Installer --window-icon='/usr/share/icons/papirus-antix/32x32/apps/gnome-debian.png' --fixed --width=300 --height=100 --image=/usr/share/icons/numix-square-antix/32x32/actions/gtk-ok.png  --text='Package(s) installed! ' --button='x'|| yad --center --title=FT10 --window-icon='/usr/share/icons/papirus-antix/32x32/apps/gnome-debian.png'  --fixed --width=300 --height=100 --image=/usr/share/icons/papirus-antix/32x32/emblems/emblem-rabbitvcs-modified.png --text=' Error installing the package! \n  Please read the log on the terminal window below  ' --button='x'"
     exit 1
    fi
    
#Recheck- if trash-cli is not installed, exit    
    	if ! [ -x "$(command -v trash)" ]; then
  	     exit 1
       fi

#If trash-cli is found, proceed with configuration:
#Close zzzfm, so we can edit it's config file
 pkill zzzfm
#back up original config file
 cp ~/.config/zzzfm/session  ~/.config/zzzfm/sessionBACKUP-FT10
 
 #Trash Label and Menu entry and localizations:
trash_label="Trash"
trash_menu_entry="Send to Trash" 
language=$(echo $LANG|cut -d. -f1)
if [ $language = "pt_PT" ]; then
 trash_label="Reciclagem"
 trash_menu_entry="Enviar para a Reciclagem" 
fi
if [ $language = "pt_BR" ]; then
 trash_label="Lixeira"
 trash_menu_entry="Mover para a Lixeira" 
fi 
  
  
#make insertions in zzzfm config file:
 echo "cstm_25a291d5-y="  >>~/.config/zzzfm/session  
 echo cstm_25a291d5-key=65535 >>~/.config/zzzfm/session 
 echo "cstm_25a291d5-label=$trash_menu_entry" >>~/.config/zzzfm/session  
 echo cstm_25a291d5-icon=gtk-delete >>~/.config/zzzfm/session  
 echo cstm_25a291d5-cxt=3%%%%%0%%%%%2%%%%%2%%%%%.local/share/Trash/files >>~/.config/zzzfm/session
 echo cstm_25a291d5-prev=edit_delete >>~/.config/zzzfm/session 
 echo 'cstm_25a291d5-line=folder=%d; if [[ ${folder} != *".local/share/Trash/files"* ]]; then trash %F; fi' >>~/.config/zzzfm/session  
 echo cstm_25a291d5-task=1 >>~/.config/zzzfm/session  
 echo cstm_25a291d5-task_err=1 >>~/.config/zzzfm/session  
 echo cstm_25a291d5-task_out=1 >>~/.config/zzzfm/session  
 echo cstm_25a291d5-keep=1 >>~/.config/zzzfm/session  
 
#find out where the keybinding to Delete is, and then the line next to it:
 declare -i delete_key_line
 delete_key_line=$(grep -nr 'edit_delete-key=65535' ~/.config/zzzfm/session | cut -d: -f1)
 delete_key_line+=1
 #add this line in the line after the edit_delete_key:
 sed -i "`echo $delete_key_line`i\\edit_delete-keymod=1\\" ~/.config/zzzfm/session
 #find out what's initially next to the delete key:
  originally_next_to_delete=$(grep 'edit_delete-next=' ~/.config/zzzfm/session| cut -d= -f2)
 #now try make the contents of this line point to the Trash entry;
  sed -i 's/edit_delete-next=.*/edit_delete-next=cstm_25a291d5/' ~/.config/zzzfm/session 

#now tell that what comes next to Trash was what originally was after Delete 
 sed -i "s/cstm_25a291d5-next=.*/cstm_25a291d5-next='echo $originally_next_to_delete'/" ~/.config/zzzfm/session

#make sure that delete's icon is changed to an "X", to avoid confusion with Send to Trash
 sed -i "`echo $delete_key_line`i\\edit_delete-icn=gtk-close\\" ~/.config/zzzfm/session
 sed -i 's/edit_delete-icn=.*/edit_delete-icn=gtk-close/' ~/.config/zzzfm/session
 
# If the edit_delete-next line does not exist, insert it:
###TODO: check if the field exists in the config file: 
 echo edit_delete-next=cstm_25a291d5 >>~/.config/zzzfm/session
 
 ##########Insert bookmarks
previous_bookmark=$(grep -nr 'main_book-child=' ~/.config/zzzfm/session | cut -d= -f2)
 
#Trash
 echo cstm_$previous_bookmark-next=cstm_2b7f3cb1  >>~/.config/zzzfm/session
 echo cstm_2b7f3cb1-next=cstm_3ca29516 >>~/.config/zzzfm/session
 echo cstm_3ca29516-x=3 >>~/.config/zzzfm/session
 echo cstm_3ca29516-z=~/.local/share/Trash/files >>~/.config/zzzfm/session
 echo "cstm_3ca29516-label=$trash_label" >>~/.config/zzzfm/session
 echo cstm_3ca29516-icon=gtk-delete >>~/.config/zzzfm/session
 echo cstm_3ca29516-prev=$previous_bookmark>>~/.config/zzzfm/session
 
#Downloads
downloads_label=$(echo $XDG_DOWNLOAD_DIR| cut -d/ -f4)
echo cstm_3ca29516-next=cstm_6c99bdd2 >>~/.config/zzzfm/session
echo cstm_6c99bdd2-x=3 >>~/.config/zzzfm/session
echo "cstm_6c99bdd2-z=$XDG_DOWNLOAD_DIR" >>~/.config/zzzfm/session
echo "cstm_6c99bdd2-label=$downloads_label" >>~/.config/zzzfm/session
echo cstm_6c99bdd2-icon=download  >>~/.config/zzzfm/session
echo cstm_6c99bdd2-next=cstm_1f966a72  >>~/.config/zzzfm/session
echo cstm_6c99bdd2-prev=cstm_3ca29516 >>~/.config/zzzfm/session

#Documents
documents_label=$(echo $XDG_DOCUMENTS_DIR| cut -d/ -f4)
echo cstm_1f966a72-x=3  >>~/.config/zzzfm/session
echo cstm_1f966a72-z=$XDG_DOCUMENTS_DIR >>~/.config/zzzfm/session
echo "cstm_1f966a72-label=$documents_label" >>~/.config/zzzfm/session
echo  cstm_1f966a72-icon=gtk-file >>~/.config/zzzfm/session
echo  cstm_1f966a72-next=cstm_23f8698b >>~/.config/zzzfm/session
echo cstm_1f966a72-prev=cstm_6c99bdd2 >>~/.config/zzzfm/session

#Pictures
documents_label=$(echo $XDG_PICTURES_DIR| cut -d/ -f4)
echo cstm_23f8698b-x=3 >>~/.config/zzzfm/session
echo cstm_23f8698b-z=$XDG_PICTURES_DIR >>~/.config/zzzfm/session
echo cstm_23f8698b-label=$documents_label >>~/.config/zzzfm/session
echo cstm_23f8698b-icon=viewimage >>~/.config/zzzfm/session
echo cstm_23f8698b-next=cstm_3be1d265 >>~/.config/zzzfm/session
echo cstm_23f8698b-prev=cstm_1f966a72 >>~/.config/zzzfm/session

#Videos
videos_label=$(echo $XDG_VIDEOS_DIR| cut -d/ -f4)
echo cstm_3be1d265-x=3 >>~/.config/zzzfm/session
echo cstm_3be1d265-z=$XDG_VIDEOS_DIR >>~/.config/zzzfm/session
echo cstm_3be1d265-label=$videos_label >>~/.config/zzzfm/session
echo cstm_3be1d265-icon=/usr/share/icons/papirus-antix/48x48/actions/filmgrain.png >>~/.config/zzzfm/session
echo cstm_3be1d265-next=cstm_1fe75a69 >>~/.config/zzzfm/session
echo cstm_3be1d265-prev=cstm_23f8698b >>~/.config/zzzfm/session

#Music
music_label=$(echo $XDG_MUSIC_DIR| cut -d/ -f4)
echo cstm_1fe75a69-x=3 >>~/.config/zzzfm/session
echo cstm_1fe75a69-z=$XDG_MUSIC_DIR >>~/.config/zzzfm/session
echo cstm_1fe75a69-label=$music_label >>~/.config/zzzfm/session
echo cstm_1fe75a69-icon=/usr/share/icons/papirus-antix/48x48/actions/filename-filetype-amarok.png  >>~/.config/zzzfm/session
echo cstm_1fe75a69-prev=cstm_3be1d265 >>~/.config/zzzfm/session
echo cstm_1fe75a69-next=cstm_2e7ac554 >>~/.config/zzzfm/session

#Show access cloud drives + Seachmonkey icons:
echo cstm_2e7ac554-x=2 >>~/.config/zzzfm/session
echo cstm_2e7ac554-z=ft10-access-cloud.desktop >>~/.config/zzzfm/session
echo cstm_2e7ac554-prev=cstm_1fe75a69 >>~/.config/zzzfm/session
echo cstm_2e7ac554-next=cstm_5cb750ae >>~/.config/zzzfm/session
echo cstm_5cb750ae-x=2 >>~/.config/zzzfm/session
echo cstm_5cb750ae-z=searchmonkey.desktop >>~/.config/zzzfm/session
echo cstm_5cb750ae-prev=cstm_2e7ac554 >>~/.config/zzzfm/session


#Don't show tabs if there's only one tab:
#find out where the entry [Interface] is and insert line right below it:
 declare -i interface_line
 interface_line=$(grep -nr '\[Interface\]' ~/.config/zzzfm/session | cut -d: -f1)
 interface_line+=1
 #add this line in the line after the interface_delete_key:
 sed -i "`echo $interface_line`i\\always_show_tabs=0\\" ~/.config/zzzfm/session


#show bookmarks:
echo panel1_show_book0-b=1 >>~/.config/zzzfm/session
sed -i 's/panel1_slider_positions0-y=.*/panel1_slider_positions0-y=178/' ~/.config/zzzfm/session
#Show devices:
echo panel1_show_devmon0-b=1 >>~/.config/zzzfm/session
#Hide hidden files:
echo panel1_show_hidden-b=2 >>~/.config/zzzfm/session
#Ctrl+H does not work????
echo panel1_show_hidden-key=104 >>~/.config/zzzfm/session
sed -i 's/panel1_show_hidden-key=.*/panel1_show_hidden-key=104/' ~/.config/zzzfm/session
#Show thumbnails???:
#####echo show_thumbnail=1  >>~/.config/zzzfm/session
sed -i 's/show_thumbnail=.*/show_thumbnail=1/' ~/.config/zzzfm/session

echo view_thumb-b=1  >>~/.config/zzzfm/session

#find out where the entry [General] is and insert line right below it:
 declare -i general_line
 general_line=$(grep -nr '\[General\]' ~/.config/zzzfm/session | cut -d: -f1)
 general_line+=1
 #add this line in the line after the edit_delete_key:
 sed -i "`echo $general_line`i\\show_thumbnail=1\\" ~/.config/zzzfm/session


#show big icons
echo  panel1_list_detailed-b=2  >>~/.config/zzzfm/session
echo  panel1_list_compact-b=1  >>~/.config/zzzfm/session
echo  panel1_list_large-b=1  >>~/.config/zzzfm/session
echo  panel1_list_large0-b=1  >>~/.config/zzzfm/session

#hide empty devices???
echo  dev_show_empty-b=1  >>~/.config/zzzfm/session
echo panel1_show_devmon-b=2  >>~/.config/zzzfm/session
echo panel1_show_dirtree-b=1  >>~/.config/zzzfm/session
echo panel1_show_book-b=2  >>~/.config/zzzfm/session

#hide Tree:
echo  main_dev-b=1 >>~/.config/zzzfm/session
sed -i 's/panel1_show_devmon-b=.*/panel1_show_devmon-b=2/' ~/.config/zzzfm/session
sed -i 's/panel1_show_dirtree-b=.*/panel1_show_dirtree-b=1/' ~/.config/zzzfm/session
sed -i 's/panel1_show_book-b=.*/panel1_show_book-b=2/' ~/.config/zzzfm/session
sed -i 's/panel1_show_dirtree0-b=.*/panel1_show_dirtree0-b=2/' ~/.config/zzzfm/session

#Use blue folder icon instead of zzzfm's default one:
echo main_icon-icn=zzzfm-48-folder-blue  >>~/.config/zzzfm/session

##Insert a Toogle Big icons button on the toolbar:
#find out where the  line that says: -tool=6 is, and then the line next to it:
tool6_line=$(grep -nr 'tool=6' ~/.config/zzzfm/session | cut -d: -f1)
tool6_id=$(sed -n -e "`echo $tool6_line`p" ~/.config/zzzfm/session| cut -d- -f1)
#Now just insert the needed lines, including the next and and prev, refering to the correct variables
echo $tool6_id-next=cstm_54d97f86 >>~/.config/zzzfm/session
echo cstm_54d97f86-tool=17 >>~/.config/zzzfm/session
echo cstm_54d97f86-prev=$tool6_id >>~/.config/zzzfm/session

#Hide empty devices:
pkill zzzfm
zzzfm & sleep 0.1
pkill zzzfm
sed -i 's/dev_show_empty-b=.*/dev_show_empty-b=2/' ~/.config/zzzfm/session

#Start zzzfm:
zzzfm
 ###restaurar a configuração original
 ###cp ~/.config/zzzfm/sessionBACKUP-FT10 ~/.config/zzzfm/session   
 ######## pkill zzzfm &&  mv  ~/.config/zzzfm-2-2-22 ~/.config/zzzfm/
